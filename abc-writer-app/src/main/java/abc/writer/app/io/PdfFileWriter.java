package abc.writer.app.io;

import abc.writer.app.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hl
 */
public class PdfFileWriter extends AbstractProcessRunner<File> {

    private File tempFile;
    private final File psFile;

    public PdfFileWriter(File psFile) {
        this.psFile = psFile;
    }

    @Override
    public File apply() {
        createTemporaryPdfFile(psFile);
        try {
            runCommand();
            return createResultFile();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(PdfFileWriter.class.getName()).log(Level.SEVERE, "Could not create PDF File from " + psFile.getAbsolutePath(), ex);
            return null;
        }
    }

    private void createTemporaryPdfFile(File file) {
        String name = file.getName().substring(0, file.getName().lastIndexOf("."));
        tempFile = new File(Constants.WORKING_DIRECTORY_PREVIEW + "/" + name + ".pdf");
        tempFile.getParentFile().mkdirs();
    }

    private void runCommand() throws IOException, InterruptedException {
        String cmdString = "/usr/bin/ps2pdf " + psFile.getAbsolutePath() + " " + tempFile.getAbsolutePath();
        Process process = Runtime.getRuntime().exec(cmdString);
        logProcess(process);
        process.waitFor();
        if (psFile != null) {
            psFile.delete();
        }
    }

    private File createResultFile() throws FileNotFoundException {
        File result = new File(Constants.PUBLIC_PDF_DIRECTORY + "/" + tempFile.getName().replaceAll("_", " "));
        byte[] bytes = FileUtil.inputStreamToByteArray(new FileInputStream(tempFile));
        FileUtil.write(bytes, result.getAbsolutePath());
        tempFile.delete();
        return result;
    }
}
