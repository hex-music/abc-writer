package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import java.io.File;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author hl
 */
public class AddFolderAction extends AbcWriterAction<File> {

    private final File parent;

    public AddFolderAction(AbcWriter app, File parent) {
        super(app);
        this.parent = parent;
    }

    @Override
    protected File performAction() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add Folder");
        dialog.setHeaderText("Create new folder in " + parent.getName() + ".");
        String folderName = dialog.showAndWait().orElse("");
        if (folderName.isEmpty()) {
            return null;
        }
        File result = new File(parent.getAbsolutePath() + "/" + folderName);
        result.mkdir();
        return result;
    }

}
