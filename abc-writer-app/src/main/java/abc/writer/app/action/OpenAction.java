package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.FileType;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.Editor;
import abc.writer.app.gui.TextEditor;
import java.io.File;
import java.util.Optional;

/**
 *
 * @author hl
 */
public class OpenAction extends AbcWriterAction<Optional<Editor>> {

    private final File file;

    public OpenAction(AbcWriter app, File file) {
        super(app);
        this.file = file;
    }

    @Override
    protected Optional<Editor> performAction() {
        if (file == null) {
            return Optional.empty();
        }
        Editor editor;
        switch (FileType.of(file)) {
            case ABC:
            case FMT:
                editor = new AbcEditor(app, file);
                break;
            case SWT:
            case TXT:
            case EMPTY:
                editor = new TextEditor(app, file);
                break;
            default:
                new OpenInSystemAction(app, file).actionPerformed();
                return Optional.empty();
        }
        app.setCurrentEditor(editor);
        return Optional.of(editor);
    }
}
