package abc.writer.app.gui.navigator;

import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Tooltip;

/**
 *
 * @author hl
 */
interface FileBrowserItem {

    String getText();

    Node getGraphic();

    Tooltip getTooltip();

    void performDefaultAction();

    void performDeleteAction();

    ContextMenu getContextMenu();

}
