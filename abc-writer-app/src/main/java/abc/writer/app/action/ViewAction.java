package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.Editor;
import java.io.File;

/**
 *
 * @author hl
 */
public class ViewAction extends AbstractPostScriptAction {

    public ViewAction(AbcWriter app, Editor editor) {
        super(app, editor, Action.PREVIEW);
    }

    public ViewAction(AbcWriter app) {
        this(app, app.getCurrentEditor());
    }

    @Override
    protected File performAction() {
        return apply();
    }
}
