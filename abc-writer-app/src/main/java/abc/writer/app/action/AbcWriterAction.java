package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.FileType;
import abc.writer.app.gui.StatusBar;
import java.util.List;
import javafx.stage.FileChooser;

/**
 * Created 2017-okt-24
 *
 * @author hl
 * @param <T>
 */
public abstract class AbcWriterAction<T> {

    protected final AbcWriter app;
    private final StatusBar statusBar = StatusBar.instance();

    public AbcWriterAction(AbcWriter app) {
        this.app = app;
    }

    public final T actionPerformed() {
        if (!isEnabled()) {
            return null;
        }
        return performAction();
    }

    public boolean isEnabled() {
        return true;
    }

    protected List<FileChooser.ExtensionFilter> getExtensionFilters() {
        return FileType.getExtensionFilters();
    }

    protected abstract T performAction();

    protected void setLeftStatus(String message) {
        statusBar.leftMessage(message);
    }

    protected void setLeftStatus(String message, double timeout) {
        statusBar.leftMessage(message, timeout);
    }

    protected void clearLeftStatus() {
        statusBar.clearLeft();
    }

    protected void setCenterStatus(String message) {
        statusBar.centerMessage(message);
    }

    protected void setCenterStatus(String message, double timeout) {
        statusBar.centerMessage(message, timeout);
    }

    protected void clearCenterStatus() {
        statusBar.clearCenter();
    }

    protected void setRightStatus(String message) {
        statusBar.rightMessage(message);
    }

    protected void setRightStatus(String message, double timeout) {
        statusBar.rightMessage(message, timeout);
    }

    protected void clearRightStatus() {
        statusBar.clearRight();
    }
}
