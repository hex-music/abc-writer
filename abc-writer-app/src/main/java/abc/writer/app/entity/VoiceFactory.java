package abc.writer.app.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author hl
 */
public class VoiceFactory {

    private static final Pattern HEADER_FIELD_PATTERN = Pattern.compile("([A-UX-Z]:)|([%\\\\].)");

    private VoiceFactory() {
    }

    public static List<Voice> from(Tune tune) {
        return collectVoices(tune);
    }

    private static List<Voice> collectVoices(Tune tune) {
        String abcContent = tune.getAsString();
        if (abcContent == null || !abcContent.contains("\n")) {
            return Collections.emptyList();
        }
        int start = 0;
        for (String line : abcContent.split("\n")) {
            if (line.length() < 2 || HEADER_FIELD_PATTERN.matcher(line.substring(0, 2)).matches()) {
                start += line.length() + 1;
            } else {
                break;
            }
        }
        if (!abcContent.contains("\nV:")) {
            return Collections.singletonList(Voice.of(tune, tune.getAsString()));
        } else if (!abcContent.contains("\n[V:")) {
            return Stream.of(abcContent.substring(start).split("\nV:"))
                    .map(v -> v.startsWith("V:") ? v : "V:" + v)
                    .map(voiceContent -> Voice.of(tune, voiceContent))
                    .collect(Collectors.toList());
        } else {
            Map<String, List<String>> voiceMap = new HashMap<>();
            Stream.of(abcContent.split("\n")).filter(line -> line.startsWith("V:"))
                    .map(line -> line.substring(2).trim())
                    .map(line -> line.substring(0, line.indexOf(" ")).trim())
                    .forEach(id -> voiceMap.put(id, new ArrayList<>()));
            Stream.of(abcContent.substring(start).split("\n\\[?V:"))
                    .map(VoiceFactory::ensureLineStartIsCorrect)
                    .forEach(line -> voiceMap.get(trimId(line)).add(trimContent(line)));
            return voiceMap.keySet().stream()
                    .map(id -> voiceMap.get(id).stream()
                    .collect(Collectors.joining("\n")))
                    .map(voiceContent -> Voice.of(tune, voiceContent))
                    .collect(Collectors.toList());
        }
    }

    private static String ensureLineStartIsCorrect(String line) {
        String result = line.startsWith("V:")
                ? line
                : line.startsWith("[V:")
                ? line.substring(line.indexOf("]") + 1)
                : "V:" + line;
        return result;
    }

    private static String trimContent(String line) {
        return line.substring(0, line.indexOf(" ")).contains("]")
                ? line.substring(line.indexOf("]") + 1)
                : line;
    }

    private static String trimId(String line) {
        String result = line.startsWith("V:")
                ? line.substring(2)
                : line.startsWith("[V:")
                ? line.substring(3)
                : line;
        return result.substring(0, result.indexOf(" ")).replaceAll("]", "");
    }
}
