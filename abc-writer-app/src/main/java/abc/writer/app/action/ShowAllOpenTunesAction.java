package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.AllOpenTunesPane;

/**
 *
 * @author hl
 */
public class ShowAllOpenTunesAction extends AbcWriterAction<Void> {

    public ShowAllOpenTunesAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        app.setRight(new AllOpenTunesPane(app));
        return null;
    }

}
