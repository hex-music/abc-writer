package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hl
 */
public class OpenInBrowserAction extends AbcWriterAction<Void> {

    private final String address;

    public OpenInBrowserAction(AbcWriter app, String address) {
        super(app);
        this.address = address;
    }

    @Override
    protected Void performAction() {
        new Thread(() -> {
            try {
                Desktop.getDesktop().browse(new URI(address));
            } catch (URISyntaxException | IOException ex) {
                Logger.getLogger(OpenInBrowserAction.class.getName()).log(Level.SEVERE, "Could not open \"" + address + "\" in the default browser", ex);
            }
        }).start();
        return null;
    }

}
