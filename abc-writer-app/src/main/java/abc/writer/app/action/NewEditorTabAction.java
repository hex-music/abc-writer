package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.AbcEditor;

/**
 *
 * @author hl
 */
public class NewEditorTabAction extends AbcWriterAction<AbcEditor> {

    public NewEditorTabAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected AbcEditor performAction() {
        return app.newEditor();
    }

}
