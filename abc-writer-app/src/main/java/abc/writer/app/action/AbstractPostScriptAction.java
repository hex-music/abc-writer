package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.entity.Tune;
import abc.writer.app.io.FileUtil;
import abc.writer.app.io.PdfFileWriter;
import abc.writer.app.io.PsFileWriter;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.Editor;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author hl
 */
public abstract class AbstractPostScriptAction extends AbcWriterAction<File> {

    private final AbcEditor editor;
    private final Action action;

    public AbstractPostScriptAction(AbcWriter app, Editor editor, Action action) {
        super(app);
        this.editor = getAsAbcEditor(editor);
        this.action = action;
    }

    protected final File apply() {
        if (editor == null) {
            return null;
        }
        AbcEditor.AbcFile abcFile;
        if (action.equals(Action.PREVIEW_SINGLE) && editor.getCurrentTune() != null) {
            Tune currentTune = editor.getCurrentTune();
            File currentTempFile = new File(Constants.PUBLIC_ABC_DIRECTORY, currentTune.getLabel().replaceAll(" ", "_") + "-temp" + FileType.ABC.suffix());
            currentTempFile.deleteOnExit();
            abcFile = AbcEditor.AbcFile.of(currentTempFile, currentTune.getAsString());
        } else {
            abcFile = editor.get();
        }
        String abcPart = abcFile.file().getAbsolutePath();
        abcPart = abcPart.substring(abcFile.file().getParent().length());
        String abcFilePath = Constants.WORKING_DIRECTORY_ABC_TEMP + abcPart;
        File tempFile = new File(abcFilePath.replaceAll(" ", "_"));
        tempFile.getParentFile().mkdirs();
        FileUtil.write(abcFile.text().getBytes(StandardCharsets.UTF_8), tempFile.getAbsolutePath());
        File file;
        switch (action) {
            case CREATE_PS:
                File psResult = new PsFileWriter(tempFile, createResultFile(abcPart)).apply();
                setLeftStatus("Exported to " + psResult.getAbsolutePath(), 5);
                return psResult;
            case PREVIEW_SINGLE:
            case PREVIEW:
                file = new PsFileWriter(tempFile).apply();
                file.deleteOnExit();
                openPreview(file);
                return file;
            case CREATE_PDF:
                file = new PsFileWriter(tempFile).charset(Charset.forName("ISO-8859-1")).apply();
                File pdfResult = new PdfFileWriter(file).apply();
                setLeftStatus("Exported to " + pdfResult.getAbsolutePath(), 5);
                return pdfResult;
            default:
                return null;
        }
    }

    private File createResultFile(String abcPart) {
        String psPart = abcPart.substring(0, abcPart.lastIndexOf(".")) + ".ps";
        String psFilePath = Constants.PUBLIC_PS_DIRECTORY + psPart;
        File result = new File(psFilePath);
        result.getParentFile().mkdirs();
        return result;
    }

    private void openPreview(File file) {
        new Thread(() -> {
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException e) {

            }
        }).start();
    }

    private static AbcEditor getAsAbcEditor(Editor editor) {
        if (editor instanceof AbcEditor) {
            return (AbcEditor) editor;
        }
        return null;
    }

    protected enum Action {
        PREVIEW, PREVIEW_SINGLE, CREATE_PS, CREATE_PDF
    }
}
