package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.FileEntity;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;

/**
 *
 * @author hl
 */
public class FileBrowser extends TreeView<Entity> {

    private final AbcWriter app;
    private final FolderItem rootItem;

    public FileBrowser(AbcWriter app) {
        this.app = app;
        rootItem = new FolderItem(app, FileEntity.from(Constants.PUBLIC_DIRECTORY));
        rootItem.setExpanded(true);
        setRoot(rootItem);
        init();
    }

    private void init() {
        setOnKeyPressed((e) -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                if (getSelectedItem() instanceof FileItem || getSelectedItem() instanceof TuneItem || getSelectedItem() instanceof VoiceItem) {
                    getSelectedItem().performDefaultAction();
                    e.consume();
                }
            }
            if (e.getCode().equals(KeyCode.DELETE)) {
                if (getSelectedItem() instanceof FileItem || getSelectedItem() instanceof FolderItem) {
                    getSelectedItem().performDeleteAction();
                    e.consume();
                }
            }
        });
        setCellFactory((TreeView<Entity> t) -> {
            return new AbcTreeCell<>();
        });
    }

    private FileBrowserItem getSelectedItem() {
        return (FileBrowserItem) getSelectionModel().getSelectedItem();
    }

}
