package abc.writer.app.io;

import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 *
 * @author hl
 */
public class MidiFileWriter extends AbstractProcessRunner<File> {

    private final String abcContent;

    public MidiFileWriter(String abcContent) {
        this.abcContent = abcContent;
    }

    @Override
    public File apply() {
        try {
            File abcFile = setupAbcFile();
            File midiFile = setupMidiFile();
            FileUtil.write(abcContent.getBytes(StandardCharsets.UTF_8), abcFile.getAbsolutePath());
            String cmdString = "/usr/bin/abc2midi " + abcFile.getAbsolutePath() + " -o " + midiFile.getAbsolutePath();
            Process process = Runtime.getRuntime().exec(cmdString);
            logProcess(process);
            process.waitFor();
            return midiFile;
        } catch (IOException | InterruptedException ex) {
            throw new AbcWriterException(ex);
        }
    }

    private File setupMidiFile() throws IOException {
        Constants.WORKING_DIRECTORY_MIDI_TEMP.mkdirs();
        File midiFile = File.createTempFile("temp-file", FileType.MID.suffix(), Constants.WORKING_DIRECTORY_MIDI_TEMP);
        midiFile.deleteOnExit();
        return midiFile;
    }

    private File setupAbcFile() throws IOException {
        Constants.WORKING_DIRECTORY_ABC_TEMP.mkdirs();
        File abcFile = File.createTempFile("temp-file", FileType.ABC.suffix(), Constants.WORKING_DIRECTORY_ABC_TEMP);
        abcFile.deleteOnExit();
        return abcFile;
    }

    @Override
    public List<String> getProcessInfo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getProcessError() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
