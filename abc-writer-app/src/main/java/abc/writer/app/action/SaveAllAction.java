package abc.writer.app.action;

import abc.writer.app.AbcWriter;

/**
 *
 * @author hl
 */
public class SaveAllAction extends AbcWriterAction<Void> {

    public SaveAllAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        long count = app.getOpenEditors().stream()
                .filter(e -> e.isChanged())
                .map(e -> new SaveEditorContentAction(app, e).performAction())
                .filter(of -> of.isPresent())
                .count();
        String message = count == 0
                ? "No files saved"
                : count == 1
                        ? "Saved one file"
                        : "Saved " + count + " files";
        setLeftStatus(message, 4);
        return null;
    }

}
