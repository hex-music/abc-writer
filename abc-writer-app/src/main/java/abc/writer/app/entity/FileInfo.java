package abc.writer.app.entity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author hl
 */
public class FileInfo implements Entity<List<Tune>> {

    private final String label;
    private final Preamble preamble;
    private final List<Tune> tunes;

    private FileInfo(String label, Preamble preamble, List<Tune> tunes) {
        this.label = label;
        this.preamble = preamble;
        this.tunes = tunes;
    }

    public static FileInfo of(String label, Preamble preamble, List<Tune> tunes) {
        return new FileInfo(label, preamble, tunes);
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public List<Tune> getContent() {
        return tunes.stream().sorted((a, b) -> a.getIndex().compareTo(b.getIndex())).collect(Collectors.toList());
    }

    public Optional<Preamble> getPreamble() {
        return Optional.ofNullable(preamble);
    }

    public Integer getNumberOfTunes() {
        return tunes.size();
    }
}
