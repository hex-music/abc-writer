package abc.writer.app.gui;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.action.CreatePdfAction;
import abc.writer.app.action.CreatePostScriptAction;
import abc.writer.app.action.EditAwFileAction;
import abc.writer.app.action.ViewAwFileAction;
import abc.writer.app.action.ExitAction;
import abc.writer.app.action.NewEditorTabAction;
import abc.writer.app.action.OpenAction;
import abc.writer.app.action.SelectFileToOpenAction;
import abc.writer.app.action.OpenInBrowserAction;
import abc.writer.app.action.SaveAction;
import abc.writer.app.action.SaveAllAction;
import abc.writer.app.action.SaveFileAsAction;
import abc.writer.app.action.ShowAllOpenTunesAction;
import abc.writer.app.action.ShowFileInfoAction;
import abc.writer.app.action.ShowMidiChannelCheatSheetAction;
import abc.writer.app.action.ShowPostscriptFontCheatSheetAction;
import abc.writer.app.action.ShowShortcutCheatSheetAction;
import abc.writer.app.action.SplitIntoSeparateTunesAction;
import abc.writer.app.action.ViewLogFileAction;
import abc.writer.app.io.FileUtil;
import abc.writer.app.system.WriterLogger;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.geometry.Pos;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;

/**
 *
 * @author hl
 */
public class AbcWriterMenuBar extends MenuBar {

    private final AbcWriter app;

    private AbcWriterMenuBar(AbcWriter app) {
        this.app = app;
    }

    public static AbcWriterMenuBar from(AbcWriter writer) {
        AbcWriterMenuBar menuBar = new AbcWriterMenuBar(writer);
        menuBar.init();
        return menuBar;
    }

    private void init() {
        Menu fileMenu = new Menu("File");

        MenuItem openItem = new MenuItem("Open ABC Music File");
        openItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        openItem.setOnAction((e) -> new SelectFileToOpenAction(app).actionPerformed());

        Menu openRecentFilesMenu = getOpenRecentFilesMenu();

        MenuItem saveItem = new MenuItem("Save");
        saveItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        saveItem.setOnAction((e) -> new SaveAction(app).actionPerformed());
        MenuItem saveAllItem = new MenuItem("Save All");
        saveAllItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveAllItem.setOnAction((e) -> new SaveAllAction(app).actionPerformed());
        MenuItem saveAsItem = new MenuItem("Save As...");
        saveAsItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        saveAsItem.setOnAction((e) -> new SaveFileAsAction(app).actionPerformed());

        MenuItem exportPostScriptItem = new MenuItem("Export as PostScript");
        exportPostScriptItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        exportPostScriptItem.setOnAction((e) -> new CreatePostScriptAction(app).actionPerformed());
        MenuItem exportPdfItem = new MenuItem("Export as PDF");
        exportPdfItem.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        exportPdfItem.setOnAction((e) -> new CreatePdfAction(app).actionPerformed());

        MenuItem exitItem = new MenuItem("Quit");
        exitItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
        exitItem.setOnAction((e) -> new ExitAction(app).actionPerformed());

        fileMenu.getItems().addAll(openItem, openRecentFilesMenu, separator(),
                saveItem, saveAllItem, saveAsItem, separator(),
                exportPostScriptItem, exportPdfItem, separator(),
                exitItem);

        Menu tuneMenu = new Menu("Tune");

        MenuItem newTabItem = new MenuItem("New tab");
        newTabItem.setOnAction((e) -> new NewEditorTabAction(app).actionPerformed());
        newTabItem.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
        MenuItem showFileInfoItem = new MenuItem("Show file information");
        showFileInfoItem.setOnAction((e) -> new ShowFileInfoAction(app).actionPerformed());
        MenuItem splitToTunesItem = new MenuItem("Split ABC-tunes into separate files");
        splitToTunesItem.setOnAction((e) -> new SplitIntoSeparateTunesAction(app).actionPerformed());
        MenuItem showAllOpenTunesItem = new MenuItem("Show all open tunes");
        showAllOpenTunesItem.setOnAction((e) -> new ShowAllOpenTunesAction(app).actionPerformed());

        tuneMenu.getItems().addAll(newTabItem, separator(), showFileInfoItem, splitToTunesItem, separator(),
                showAllOpenTunesItem);

        Menu toolMenu = new Menu("Tools");
        Menu templateMenu = createTemplatesMenu();

        MenuItem editAwFileItem = new MenuItem("Edit AW File");
        editAwFileItem.setOnAction((e) -> new EditAwFileAction(app, new File(Constants.AW_PATH)).actionPerformed());
        MenuItem viewAwFileItem = new MenuItem("View AW File");
        viewAwFileItem.setOnAction((e) -> new ViewAwFileAction(app).actionPerformed());
        MenuItem viewLogFileItem = new MenuItem("View Log File");
        viewLogFileItem.setOnAction((e) -> new ViewLogFileAction(app).actionPerformed());

        toolMenu.getItems().addAll(templateMenu, separator(), editAwFileItem, viewAwFileItem, viewLogFileItem);

        Menu helpMenu = new Menu("Help");
        // TODO: Fix help pages
//        MenuItem helpItem = new MenuItem("Help");
//        helpItem.setAccelerator(new KeyCodeCombination(KeyCode.F1));
//        helpItem.setOnAction((e) -> new ShowHelpDialogAction(app).actionPerformed());
        MenuItem shortcutsItem = new MenuItem("Shortcut Cheat Sheet");
        shortcutsItem.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.CONTROL_DOWN));
        shortcutsItem.setOnAction((e) -> new ShowShortcutCheatSheetAction(app).actionPerformed());
        MenuItem psFontsItem = new MenuItem("Postscript Font Cheat Sheet");
        psFontsItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN));
        psFontsItem.setOnAction((e) -> new ShowPostscriptFontCheatSheetAction(app).actionPerformed());
        MenuItem midiChannelsItem = new MenuItem("Midi Channel Cheat Sheet");
        midiChannelsItem.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN));
        midiChannelsItem.setOnAction((e) -> new ShowMidiChannelCheatSheetAction(app).actionPerformed());
        MenuItem abc2xmlItem = new MenuItem("ABC 2 XML");
        abc2xmlItem.setOnAction((e) -> new OpenInBrowserAction(app, "http://localhost:88/abc-writer/abc2xml/abc2xml.html").actionPerformed());
        MenuItem xml2abcItem = new MenuItem("XML 2 ABC");
        xml2abcItem.setOnAction((e) -> new OpenInBrowserAction(app, "http://localhost:88/abc-writer/abc2xml/xml2abc.html").actionPerformed());

        MenuItem abcDraftItem = new MenuItem("Latest draft for ABC - v.2.2");
        abcDraftItem.setOnAction((e) -> new OpenInBrowserAction(app, "http://abcnotation.com/wiki/abc:standard:v2.2").actionPerformed());
        MenuItem abcm2psDeviationsItem = new MenuItem("Deviation from standard in abcm2ps - Jef Moine");
        abcm2psDeviationsItem.setOnAction((e) -> new OpenInBrowserAction(app, "http://moinejf.free.fr/abcm2ps-doc/features.xhtml").actionPerformed());
        MenuItem abcm2psParamsItem = new MenuItem("Parameters for abcm2ps - Jef Moine");
        abcm2psParamsItem.setOnAction((e) -> new OpenInBrowserAction(app, "http://moinejf.free.fr/abcm2ps-doc/").actionPerformed());
        MenuItem abcMidiItem = new MenuItem("About ABC and MIDI");
        abcMidiItem.setOnAction((e) -> new OpenInBrowserAction(app, "https://ifdo.ca/~seymour/runabc/abcguide/abc2midi_guide.html").actionPerformed());

//        helpMenu.getItems().addAll(helpItem, separator(), shortcutsItem, psFontsItem, midiChannelsItem, separator(),
//                abcDraftItem, abcm2psDeviationsItem, abcm2psParamsItem, abcMidiItem, abc2xmlItem, xml2abcItem);
        helpMenu.getItems().addAll(shortcutsItem, psFontsItem, midiChannelsItem, separator(),
                abcDraftItem, abcm2psDeviationsItem, abcm2psParamsItem, abcMidiItem, abc2xmlItem, xml2abcItem);

        getMenus().addAll(fileMenu, tuneMenu, toolMenu, helpMenu);
    }

    private Menu createTemplatesMenu() {
        Menu templatesMenu = new Menu("Templates");
        setupTemplates();
        templatesMenu.getItems().addAll(Stream.of(Constants.TEMPLATES_DIRECTORY.listFiles())
                .sorted((a, b) -> a.getName().compareTo(b.getName()))
                .map(this::createTemplateItem)
                .collect(Collectors.toList()));
        return templatesMenu;
    }

    private MenuItem createTemplateItem(File file) {
        MenuItem item = new MenuItem(FileType.of(file).getLabel());
        item.setOnAction(e -> new OpenAction(app, file).actionPerformed());
        return item;
    }

    private Menu getOpenRecentFilesMenu() {
        Menu openRecent = new Menu("Open Recent ABC Music File");
        MenuItem noRecent = new MenuItem("No Recent Files Available");
        openRecent.getItems().add(noRecent);
        openRecent.setOnShowing((e) -> {
            List<String> alreadyOpened = app.getOpenEditors().stream().map(editor -> editor.getKey()).collect(Collectors.toList());
            List<MenuItem> recentlyOpened = WriterLogger.getDistinctList(WriterLogger.LAST_OPENED_FILE).stream()
                    .filter(last -> !alreadyOpened.contains(last.getValue()))
                    .limit(10)
                    .map(this::creatOpenFileItem)
                    .collect(Collectors.toList());
            if (!recentlyOpened.isEmpty()) {
                openRecent.getItems().clear();
                openRecent.getItems().addAll(recentlyOpened);
            }
        });
        openRecent.setOnHiding((e) -> {
            openRecent.getItems().clear();
            openRecent.getItems().add(noRecent);
        });
        return openRecent;
    }

    private MenuItem creatOpenFileItem(WriterLogger.Item logItem) {
        File file = new File(logItem.getValue());
        Label nameLabel = new Label(file.getName());
        nameLabel.setMinWidth(200);
        if (file.exists()) {
            nameLabel.getStyleClass().add("abc-latest-file-name");
        } else {
            nameLabel.getStyleClass().add("deleted-latest-file-name");
        }
        Label pathLabel = new Label(file.getAbsolutePath());
        pathLabel.getStyleClass().add("abc-latest-file-path");
        pathLabel.setAlignment(Pos.BASELINE_RIGHT);
        pathLabel.setMinWidth(500);
        HBox node = new HBox(20, nameLabel, pathLabel);
        CustomMenuItem menuItem = new CustomMenuItem(node);
        menuItem.setOnAction((e) -> new OpenAction(app, file).actionPerformed());
        return menuItem;
    }

    private static SeparatorMenuItem separator() {
        return new SeparatorMenuItem();
    }

    private void setupTemplates() {
        if (!Constants.TEMPLATES_DIRECTORY.exists()) {
            Constants.TEMPLATES_DIRECTORY.mkdirs();
        }
        Stream.of(FileType.values()).filter(ft -> ft.hasTemplate()).forEach(this::assertTemplateExists);
    }

    private void assertTemplateExists(FileType ft) {
        File file = new File(Constants.TEMPLATES_DIRECTORY.getAbsolutePath() + "/template" + ft.suffix());
        if (!file.exists()) {
            byte[] template = FileUtil.inputStreamToByteArray(getClass().getResourceAsStream("/template/template" + ft.suffix()));
            if (template == null) {
                String missingTemplate = "%%%%%% " + ft.getLabel() + " %%%%%%\n% Template missing for this type.\n\n";
                template = missingTemplate.getBytes(StandardCharsets.UTF_8);
            }
            FileUtil.write(template, file.getAbsolutePath());
        }
    }

}
