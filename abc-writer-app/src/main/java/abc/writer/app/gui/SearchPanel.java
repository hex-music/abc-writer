package abc.writer.app.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author hl
 */
public class SearchPanel extends VBox {

    private static final double LABEL_WIDTH = 100;
    private static final String BAR_REPLACEMENT = "§";
    private final FindPanel findPanel;
    private final ReplacePanel replacePanel;
    private final AbcEditor editor;
    private final List<IndexRange> matches;
    private int currentSelected;

    public SearchPanel(AbcEditor editor) {
        matches = new ArrayList<>();
        this.editor = editor;
        findPanel = new FindPanel();
        replacePanel = new ReplacePanel();
        currentSelected = -1;
        init();
        getSelectionFromArea();
    }

    private void getSelectionFromArea() {
        if (!editor.getAbcArea().getSelectedText().isEmpty()) {
            findPanel.setText(editor.getAbcArea().getSelectedText());
            find(editor.getAbcArea().getSelectedText());
            select(currentSelected);
        }
    }

    private void init() {
        getChildren().add(findPanel);
        findPanel.findField.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                currentSelected = -1;
                findPanel.getText().ifPresent(text -> {
                    find(text);
                    select(currentSelected);
                });
            }
            if (e.getCode().equals(KeyCode.ESCAPE)) {
                hide();
            }
            if (e.getCode().equals(KeyCode.H) && e.isControlDown()) {
                showReplacePanel();
                e.consume();
            }
            if (e.getCode().equals(KeyCode.F) && e.isControlDown()) {
                getSelectionFromArea();
                findPanel.findField.selectAll();
            }
            if (e.getCode().equals(KeyCode.TAB) && isShowingReplace()) {
                replacePanel.replaceField.requestFocus();
                e.consume();
            }
        });
        findPanel.next.setOnMouseClicked(e -> {
            goToNext();
        });
        findPanel.previous.setOnMouseClicked(e -> {
            goToPrevious();
        });
        replacePanel.replaceField.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.ESCAPE)) {
                hide();
            }
            if (e.getCode().equals(KeyCode.TAB)) {
                findPanel.findField.requestFocus();
                e.consume();
            }
        });
        replacePanel.replace.setOnMouseClicked(e -> {
            replace();
        });
        replacePanel.replaceAll.setOnMouseClicked(e -> {
            replaceAll();
        });
    }

    public void setFocus() {
        Platform.runLater(() -> {
            getSelectionFromArea();
            findPanel.findField.requestFocus();
            findPanel.findField.selectAll();
        });
    }

    public void hide() {
        editor.clearBottom();
        editor.getAbcArea().requestFocus();
    }

    public void showReplacePanel() {
        if (!isShowingReplace()) {
            getChildren().add(replacePanel);
        }
    }

    public void hideReplacePanel() {
        if (isShowingReplace()) {
            getChildren().remove(replacePanel);
        }
    }

    private boolean isShowingReplace() {
        return getChildren().contains(replacePanel);
    }

    private void find(String text) {
        matches.clear();
        Matcher matcher = Pattern.compile(text.replaceAll("\\|", BAR_REPLACEMENT)).matcher(editor.getAbcArea().getText().replaceAll("\\|", BAR_REPLACEMENT));
        while (matcher.find()) {
            matches.add(new IndexRange(matcher.start(), matcher.end()));
        }
    }

    private void setResultLabel() {
        String result;
        if (matches.isEmpty()) {
            result = "No matches";
        } else {
            result = matches.size() + " match";
            if (matches.size() > 1) {
                result += "es";
            }
        }
        if (currentSelected < 0) {
            findPanel.setResult(result);
        } else {
            int displayIndex = currentSelected + 1;
            findPanel.setResult(displayIndex + " of " + result);
        }
    }

    private void goToNext() {
        find(findPanel.findField.getText());
        if (matches.isEmpty()) {
            return;
        }
        if (currentSelected < 0 || currentSelected == matches.size() - 1) {
            currentSelected = 0;
        } else {
            currentSelected = currentSelected + 1;
        }
        select(currentSelected);
    }

    private void goToPrevious() {
        find(findPanel.findField.getText());
        if (matches.isEmpty()) {
            return;
        }
        if (currentSelected <= 0) {
            currentSelected = matches.size() - 1;
        } else {
            currentSelected = currentSelected - 1;
        }
        select(currentSelected);
    }

    private void replace() {
        String replacement = replacePanel.replaceField.getText();
        IndexRange selection = editor.getAbcArea().getSelection();
        String result = editor.getAbcArea().getText().substring(0, selection.getStart()) + replacement + editor.getAbcArea().getText().substring(selection.getEnd());
        editor.getAbcArea().setText(result);
        editor.getAbcArea().positionCaret(selection.getStart() + replacement.length());
        currentSelected--;
        goToNext();
    }

    private void replaceAll() {
        int caretPosition = editor.getAbcArea().getCaretPosition();
        String replacement = replacePanel.replaceField.getText();
        String textToReplace = findPanel.findField.getText();
        String result = editor.getAbcArea().getText().replaceAll(textToReplace, replacement);
        editor.getAbcArea().setText(result);
        editor.getAbcArea().positionCaret(caretPosition);
        editor.getAbcArea().requestFocus();
    }

    private void select(int match) {
        if (matches.isEmpty()) {
            currentSelected = -1;
        } else if (match < 0) {
            setSelection(0);
        } else if (match >= matches.size()) {
            setSelection(matches.size() - 1);
        } else {
            setSelection(match);
        }
    }

    private void setSelection(int match) {
        currentSelected = match;
        IndexRange found = matches.get(currentSelected);
        editor.getAbcArea().selectRange(found.getStart(), found.getEnd());
        setResultLabel();
        editor.getAbcArea().requestFocus();
    }

//    private static class Match {
//
//        private final int start;
//        private final int end;
//
//        private Match(int start, int end) {
//            this.start = start;
//            this.end = end;
//        }
//
//        public static Match of(int start, int end) {
//            return new Match(start, end);
//        }
//
//        public int getStart() {
//            return start;
//        }
//
//        public int getEnd() {
//            return end;
//        }
//
//        @Override
//        public String toString() {
//            return start + " - " + end;
//        }
//    }
    private static class FindPanel extends BorderPane {

        private final HBox findBox;
        private final Label findLabel;
        private final TextField findField;
        private final Label resultLabel;
        private final Button previous;
        private final Button next;

        public FindPanel() {
            super.setPadding(new Insets(5));
            findBox = new HBox(10);
            findBox.setAlignment(Pos.CENTER_LEFT);
            resultLabel = new Label();
            findLabel = new Label("Find:");
            findLabel.setPrefWidth(LABEL_WIDTH);
            findField = new TextField();
            previous = new Button("Previous", new ImageView("/layout/image/icon/select_blue_previous.png"));
            previous.setPrefHeight(24);
            next = new Button("Next", new ImageView("/layout/image/icon/select_blue_next.png"));
            next.setPrefHeight(24);
            setCenter(findBox);
            setRight(resultLabel);
            findBox.getChildren().addAll(findLabel, findField, previous, next);
        }

        void setResult(String result) {
            resultLabel.setText(result);
        }

        Optional<String> getText() {
            return Optional.ofNullable(findField.getText().isEmpty() ? null : findField.getText());
        }

        void setText(String text) {
            findField.setText(text.replaceAll("\n", "\\\\n"));
        }
    }

    private static class ReplacePanel extends BorderPane {

        private final HBox replaceBox;
        private final Label replaceLabel;
        private final TextField replaceField;
        private final Button replace;
        private final Button replaceAll;

        public ReplacePanel() {
            super.setPadding(new Insets(5));
            replaceBox = new HBox(10);
            replaceLabel = new Label("Replace with:");
            replaceLabel.setPrefWidth(LABEL_WIDTH);
            replaceField = new TextField();
            replace = new Button("Replace", new ImageView("/layout/image/icon/replace_blue_orange.png"));
            replaceAll = new Button("Replace all", new ImageView("/layout/image/icon/replace_all_blue_orange.png"));
            replaceBox.getChildren().addAll(replaceLabel, replaceField, replace, replaceAll);
            setCenter(replaceBox);
        }

    }

}
