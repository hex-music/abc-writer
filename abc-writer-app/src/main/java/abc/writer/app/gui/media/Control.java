package abc.writer.app.gui.media;

/**
 *
 * @author hl
 */
public enum Control {
    START(true),
    REWIND(true),
    PLAY(false),
    PAUSE(true),
    STOP(true),
    FASTFORWARD(true),
    END(true),
    REPEAT(true),
    POWER(false);
    private final boolean initiallyDisabled;

    private Control(boolean initiallyActive) {
        this.initiallyDisabled = initiallyActive;
    }

    public boolean isInitiallyDisabled() {
        return initiallyDisabled;
    }

}
