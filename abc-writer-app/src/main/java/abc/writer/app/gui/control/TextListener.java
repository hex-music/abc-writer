package abc.writer.app.gui.control;

/**
 *
 * @author hl
 */
@FunctionalInterface
public interface TextListener {

    void update(String text);

}
