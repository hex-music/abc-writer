package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.FileType;
import abc.writer.app.action.DeleteAction;
import abc.writer.app.action.OpenAction;
import abc.writer.app.action.OpenInSystemAction;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.FileEntity;
import abc.writer.app.io.FileUtil;
import java.io.File;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 *
 * @author hl
 */
public class FileItem extends TreeItem<Entity> implements FileBrowserItem {

    private static final double ICON_SIZE = 20;
    protected final File file;
    protected final AbcWriter app;
    protected final FileType fileType;

    public FileItem(AbcWriter app, FileEntity fileEntity) {
        super(fileEntity, getImageView(fileEntity));
        this.file = fileEntity.getContent();
        this.fileType = FileType.of(file);
        this.app = app;
        createTooltip();
    }

    @Override
    public Tooltip getTooltip() {
        return new Tooltip(file.getName() + "\n\n"
                + fileType.getLabel() + "\n"
                + "Directory: " + file.getParent() + "/\n"
                + "Mediatype: " + fileType.getMediatype() + "\n"
                + "Size: " + FileUtil.getFileSize(file));
    }

    private void createTooltip() {
        String tooltip = file.getAbsolutePath() + "\n"
                + fileType.getLabel() + "\n"
                + fileType.getMediatype();
        Tooltip.install(getGraphic(), new Tooltip(tooltip));
    }

    public File getFile() {
        return file;
    }

    public FileType getFileType() {
        return fileType;
    }

    @Override
    public String getText() {
        return file.getName();
    }

    @Override
    public void performDefaultAction() {
        new OpenAction(app, file).actionPerformed();
    }

    @Override
    public void performDeleteAction() {
        new DeleteAction(app, file).actionPerformed();
    }

    @Override
    public ContextMenu getContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem openItem = new MenuItem("Open file");
        openItem.setOnAction(e -> performDefaultAction());
        MenuItem deleteItem = new MenuItem("Delete file");
        deleteItem.setOnAction(e -> performDeleteAction());
        menu.getItems().addAll(openItem, deleteItem);
        if (FileType.of(file).isEditable()) {
            MenuItem openInSystemItem = new MenuItem("Open in system");
            openInSystemItem.setOnAction(e -> new OpenInSystemAction(app, file).actionPerformed());
            menu.getItems().add(1, openInSystemItem);
        }
        return menu;
    }

    public String getSuffix() {
        return fileType.suffix();
    }

    private static ImageView getImageView(FileEntity fileEntity) {
        String url;
        if (fileEntity.suffix().isPresent()) {
            url = "layout/image/icon/file_extension_" + fileEntity.suffix().get() + ".png";
        } else {
            url = "layout/image/icon/file_extension_none.png";
        }
        if (FileItem.class.getResource("/" + url) == null) {
            url = "layout/image/icon/file_extension_unrecognized.png";
        }
        ImageView result = new ImageView(url);
        result.setFitHeight(ICON_SIZE);
        result.setFitWidth(ICON_SIZE);
        return result;
    }

}
