package abc.writer.app.gui.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import javafx.geometry.Point2D;
import javafx.scene.control.IndexRange;
import javafx.scene.control.TextInputControl;

/**
 *
 * @author hl
 */
public class TextInputControlUtil {

    private static final Pattern NEW_LINE_MATCH = Pattern.compile("(\r\n)|(\r)|(\n)");
    public static final Pattern TUNE_TITLE_FIELD_MATCH = Pattern.compile("((\r\n)|(\r)|(\n))*T:(.+)((\r\n)|(\r)|(\n)*)");
    private final TextInputControl area;

    public TextInputControlUtil(TextInputControl area) {
        this.area = area;
    }

    private List<String> getLines() {
        return Arrays.asList(area.getText().split("\n"));
    }

    public Integer getNumberOfLines() {
        return getLines().size();
    }

    public Integer getCurrentLineIndex() {
        String string = area.getText().substring(0, getCurrentLineStartPosition());
        Matcher m = NEW_LINE_MATCH.matcher(string);
        int lines = 1;
        while (m.find()) {
            lines++;
        }
        return lines;
    }

    public void goToLine(Integer lineNumber) {
        List<String> lines = getLines();
        if (lineNumber > lines.size()) {
            lineNumber = lines.size();
        }
        int pos = IntStream.range(0, lineNumber).map((index) -> {
            return lines.get(index).length() + 1;
        }).sum() - 1;
        area.positionCaret(getLineStartPosition(pos));
    }

    public String getCurrentLineText() {
        return getLineText(area.getCaretPosition());
    }

    public List<Integer> getSelectedLineStarts() {
        List<Integer> lineStarts = new ArrayList<>();
        IndexRange selection = area.getSelection();
        int pos = selection.getStart();
        while (pos < selection.getEnd()) {
            int lineStart = getLineStartPosition(pos);
            lineStarts.add(lineStart);
            pos += getLineText(pos).length() + 1;
        }
        return lineStarts;
    }
    
    public List<String> getSelectedLines() {
        List<String> lines = new ArrayList<>();
        IndexRange selection = area.getSelection();
        int pos = selection.getStart();
        while (pos < selection.getEnd()) {
            String line = getLineText(pos);
            lines.add(line);
            pos += line.length() + 1;
        }
        return lines;
    }

    public String getLineText(int pos) {
        int start = getLineStartPosition(pos);
        int end = getLineEndPosition(pos);
        return area.getText().substring(start, end);
    }

    public int getCurrentLineStartPosition() {
        return getLineStartPosition(area.getCaretPosition());
    }

    public int getLineStartPosition(int pos) {
        String text = area.getText();
        int start = 0;
        if (text.substring(0, pos).contains("\n")) {
            start = text.substring(0, pos).lastIndexOf("\n") + 1;
        }
        return start;
    }

    public int getPositionOnCurrentLine() {
        return area.getCaretPosition() - getCurrentLineStartPosition();
    }

    public int getCurrentLineEndPosition() {
        return getLineEndPosition(area.getCaretPosition());
    }

    public int getLineEndPosition(int pos) {
        String text = area.getText();
        int end = area.getLength();
        if (text.substring(pos).contains("\n")) {
            end = text.substring(pos).indexOf("\n") + pos;
        }
        return end;
    }
    
    public Point2D getCoordinates() {
        return area.getInputMethodRequests().getTextLocation(0);
    }
}
