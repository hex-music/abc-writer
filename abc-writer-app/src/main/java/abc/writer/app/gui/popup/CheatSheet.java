package abc.writer.app.gui.popup;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.control.CloseLabel;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;

/**
 *
 * @author hl
 */
public class CheatSheet extends Popup {

    private final double width;
    private final double height;
    private final Node content;
    private final String labelText;

    private final double x;
    private final double y;

    private CheatSheet(AbcWriter app, double width, double height, Node content, String labelText) {
        this.width = width;
        this.height = height;
        this.content = content;
        this.labelText = labelText;
        x = app.getStage().getX() + app.getStage().getWidth();
        y = app.getStage().getY();
        init();
    }

    public static CheatSheet of(AbcWriter app, double width, double height, Node content, String labelText) {
        return new CheatSheet(app, width, height, content, labelText);
    }

    private void init() {
        setAutoHide(true);
        Label headerLabel = new Label(labelText);
        headerLabel.getStyleClass().add("abc-popup-header");
        BorderPane contentPane = new BorderPane(content);
        content.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case ESCAPE:
                    super.hide();
            }
        });
        HBox header = new HBox(headerLabel, CloseLabel.of(() -> super.hide(), "Close"));
        header.setAlignment(Pos.TOP_RIGHT);
        header.setPrefSize(width, 24);
        contentPane.setTop(header);
        getContent().add(contentPane);
        contentPane.setPrefSize(width, height);
        setX(x - width - 2);
        setY(y + 33);
    }
}
