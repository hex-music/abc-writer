package abc.writer.app.gui.control;

import javafx.scene.control.IndexRange;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

/**
 *
 * @author hl
 */
public class ClipboardHelper {

    private final TextInputControl area;

    public ClipboardHelper(TextInputControl area) {
        this.area = area;
    }

    public void copy(String text) {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(text);
        clipboard.setContent(content);
    }

    public void cut() {
        IndexRange sel = area.getSelection();
        copy(area.getSelectedText());
        String text = area.getText();
        area.setText(text.substring(0, sel.getStart()) + text.substring(sel.getEnd()));
        area.positionCaret(sel.getStart());
    }

    public void paste(String text) {
        copy(text);
        area.paste();
    }
}
