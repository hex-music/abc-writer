package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.io.FileUtil;
import abc.writer.app.system.WriterProperty;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author hl
 */
public class CreateFileFromTemplateAction extends AbcWriterAction<File> {

    private static final Pattern REPLACEMENT_PATTERN = Pattern.compile("€([-a-zA-Z]+)");
    private final File parent;
    private final FileType fileType;
    private String filename;
    private File file;

    public CreateFileFromTemplateAction(AbcWriter app, File parent, FileType fileType, String filename) {
        super(app);
        this.parent = parent;
        this.filename = filename;
        this.fileType = fileType;
    }

    @Override
    protected File performAction() {
        if (!filename.isEmpty()) {
            if (!filename.endsWith(fileType.suffix())) {
                filename += fileType.suffix();
            }
        } else {
            if (fileType.equals(FileType.EMPTY)) {
                filename = "New " + fileType.getLabel();
            } else {
                filename = "New " + fileType.getLabel()
                        + " File" + fileType.suffix();
            }
        }
        file = new File(parent.getAbsolutePath() + "/" + filename);
        if (file.exists()) {
            file = FileUtil.getIncrementallyNamedFile(file);
        }
        FileUtil.write(getTemplate().getBytes(), file.getAbsolutePath());
        return file;
    }

    private String getTemplate() {
        if (fileType.hasTemplate()) {
            try {
                String template = FileUtil.inputStreamToString(new FileInputStream(Constants.TEMPLATES_DIRECTORY.getAbsolutePath() + "/template" + fileType.suffix()));
                Matcher matcher = REPLACEMENT_PATTERN.matcher(template);
                while (matcher.find()) {
                    template = template.replaceAll("€" + matcher.group(1), getReplacement(matcher.group(1)));
                }
                return template;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(CreateFileFromTemplateAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }

    private String getReplacement(String toReplace) {
        switch (toReplace) {
            case "date":
                return LocalDate.now().format(DateTimeFormatter.ISO_DATE);
            case "file":
                return file.getName().substring(0, file.getName().lastIndexOf("."));
            case "folder":
                return file.getParentFile().getName();
            case "timestamp":
                return LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
            case "year":
                return LocalDate.now().getYear() + "";
            default:
                return WriterProperty.get(toReplace).orElse("");
        }
    }

}
