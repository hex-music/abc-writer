package abc.writer.app.io;

import java.util.List;

/**
 *
 * @author hl
 * @param <T>
 */
public interface Writer<T> {

    T apply();

    List<String> getProcessInfo();

    List<String> getProcessError();
}
