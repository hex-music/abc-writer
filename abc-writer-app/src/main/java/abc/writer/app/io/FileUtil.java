package abc.writer.app.io;

import abc.writer.app.AbcWriterException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hl
 */
public class FileUtil {

    private FileUtil() {
    }

    public static String inputStreamToString(InputStream is) {
        java.util.Scanner scanner = new java.util.Scanner(is).useDelimiter("\\A");
        String result = scanner.hasNext() ? scanner.next() : "";
        return result;
    }

    public static byte[] inputStreamToByteArray(InputStream input) {
        if (input == null) {
            return null;
        }
        byte[] result = null;
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] data = new byte[16384];
            int read;
            while ((read = input.read(data, 0, data.length)) != -1) {
                output.write(data, 0, read);
            }
            result = output.toByteArray();
        } catch (IOException ex) {
            throw new AbcWriterException("Could not read stream", ex);
        }
        return result;
    }

    public static File write(byte[] content, String path) {
        File result = new File(path);
        result.getParentFile().mkdirs();
        try (FileOutputStream output = new FileOutputStream(path)) {
            try {
                output.write(content);
            } catch (IOException ex) {
                Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static String getFileSize(File file) {
        long length = file.length();
        int unit = 1024;
        if (length < unit) {
            return length + " bytes";
        }
        int exp = (int) (Math.log(length) / Math.log(unit));
        char pre = "kMGTPE".charAt(exp - 1);
        return String.format("%.1f %sB", length / Math.pow(unit, exp), pre);
    }

    public static int sortFiles(File a, File b) {
        if (a.isDirectory() && !b.isDirectory()) {
            return -1;
        } else if (b.isDirectory() && !a.isDirectory()) {
            return 1;
        }
        return a.getAbsolutePath().compareTo(b.getAbsolutePath());
    }

    public static File getIncrementallyNamedFile(File original) {
        return getIncrementallyNamedFile(original, 0);
    }

    public static File getIncrementallyNamedFile(File original, int index) {
        index++;
        String fileName = original.getName();
        if (fileName.contains(".")) {
            fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "-" + index + fileName.substring(fileName.lastIndexOf("."));
        } else {
            fileName += "-" + index;
        }
        File result = new File(original.getParent() + "/" + fileName);
        if (result.exists()) {
            return getIncrementallyNamedFile(original, index);
        }
        return result;
    }
}
