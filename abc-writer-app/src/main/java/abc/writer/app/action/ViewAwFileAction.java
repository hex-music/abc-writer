package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.system.WriterProperty;

/**
 *
 * @author hl
 */
public class ViewAwFileAction extends AbcWriterAction<Void> {

    public ViewAwFileAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        new OpenInSystemAction(app, WriterProperty.getAwFile()).actionPerformed();
        return null;
    }

}
