package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.action.ViewVoiceAction;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.Tune;
import abc.writer.app.entity.Voice;
import java.io.File;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;

/**
 *
 * @author hl
 */
public class VoiceItem extends TreeItem<Entity> implements FileBrowserItem {

    private final AbcWriter app;
    private final File parent;
    private final Tune tune;
    private final Voice voice;

    public VoiceItem(AbcWriter app, File parent, Tune tune, Voice voice) {
        this.app = app;
        this.parent = parent;
        this.tune = tune;
        this.voice = voice;
    }

    @Override
    public String getText() {
        return voice.getLabel();
    }

    @Override
    public Tooltip getTooltip() {
        return null;
    }

    @Override
    public void performDefaultAction() {
        new ViewVoiceAction(app, voice).actionPerformed();
    }

    @Override
    public void performDeleteAction() {
    }

    @Override
    public ContextMenu getContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem viewVoiceItem = new MenuItem("View Voice");
        viewVoiceItem.setOnAction(e -> performDefaultAction());
        menu.getItems().addAll(viewVoiceItem);
        return menu;
    }

}
