package abc.writer.app.gui.control;

import java.io.File;

/**
 *
 * @author hl
 */
@FunctionalInterface
public interface FileChangeListener {

    public void fileChanged(File file);
}
