package abc.writer.app.gui.popup;

import abc.writer.app.xml.XmlUtil;
import java.util.List;
import java.util.stream.Collectors;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import xmlight.DocumentToXmlNodeParser;
import xmlight.XmlNode;

/**
 *
 * @author hl
 */
public class ShortcutCheatSheet extends TableView {

    private final TableColumn keyCombinationColumn;
    private final TableColumn descriptionColumn;
    private final XmlNode shortcutsNode;

    public ShortcutCheatSheet() {
        keyCombinationColumn = new TableColumn("Key Combination");
        descriptionColumn = new TableColumn("Description");
        shortcutsNode = getSource();
        super.getColumns().addAll(keyCombinationColumn, descriptionColumn);
        init();
    }

    private void init() {
        keyCombinationColumn.setCellValueFactory(new PropertyValueFactory<Shortcut, String>("keyCombination"));
        keyCombinationColumn.setPrefWidth(160);
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<Shortcut, String>("description"));
        descriptionColumn.setPrefWidth(500);
        List<Shortcut> shortcuts = shortcutsNode.getChildren("shortcut").stream()
                .map(Shortcut::new)
                .sorted()
                .collect(Collectors.toList());
        super.setItems(FXCollections.observableList(shortcuts));
    }

    private static XmlNode getSource() {
        return new DocumentToXmlNodeParser(ShortcutCheatSheet.class.getResourceAsStream("/xml/shortcuts.xml")).parse();
    }

    public static class Shortcut implements Comparable<Shortcut> {

        private final SimpleStringProperty keyCombination;
        private final SimpleStringProperty description;

        private Shortcut(XmlNode shortcutNode) {
            keyCombination = new SimpleStringProperty(createKeyCombination(shortcutNode));
            description = new SimpleStringProperty(shortcutNode.getText());
        }

        public String getKeyCombination() {
            return keyCombination.get();
        }

        public String getDescription() {
            return description.get();
        }

        private static String createKeyCombination(XmlNode shortcutNode) {
            StringBuilder builder = new StringBuilder();
            XmlUtil.getOptionalBoolean(shortcutNode, "ctrl").filter(b -> b).ifPresent(b -> builder.append("+CTRL"));
            XmlUtil.getOptionalBoolean(shortcutNode, "alt").filter(b -> b).ifPresent(b -> builder.append("+ALT"));
            XmlUtil.getOptionalBoolean(shortcutNode, "shift").filter(b -> b).ifPresent(b -> builder.append("+SHIFT"));
            XmlUtil.getOptionalString(shortcutNode, "key").ifPresent(s -> builder.append("+").append(s));
            String result = builder.toString();
            return result.isEmpty() ? result : result.substring(1);
        }

        @Override
        public int compareTo(Shortcut o) {
            return this.getKeyCombination().compareTo(o.getKeyCombination());
        }
    }
}
