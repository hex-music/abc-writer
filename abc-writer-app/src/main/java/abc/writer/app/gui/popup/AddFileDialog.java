package abc.writer.app.gui.popup;

import abc.writer.app.FileType;
import abc.writer.app.gui.popup.AbcDialog.Result;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 *
 * @author hl
 */
public class AddFileDialog extends Dialog<Result> {

    private TextField nameField;
    private ComboBox<FileType> suffixField;

    public AddFileDialog() {
        super.setTitle("Add File");
        super.setHeaderText(null);
        super.setGraphic(getFilePanel());
        final DialogPane dialogPane = getDialogPane();
        dialogPane.setPrefSize(500, 100);
        dialogPane.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
        setResultConverter((param) -> {
            return param.equals(ButtonType.OK)
                    ? AbcDialog.Result.YES
                    : AbcDialog.Result.CANCEL;
        });
        Platform.runLater(() -> {
            nameField.requestFocus();
        });
    }
    
    public String getFilename() {
        return nameField.getText();
    }
    
    public FileType getFileType() {
        return suffixField.getSelectionModel().getSelectedItem();
    }

    private Node getFilePanel() {
        nameField = new TextField();
        suffixField = new ComboBox(FXCollections.observableList(FileType.editableTypes()));
        suffixField.getSelectionModel().select(FileType.ABC);
        GridPane node = new GridPane();
        node.setPadding(new Insets(5));
        node.add(new Label("Name: "), 0, 0);
        node.add(nameField, 1, 0);
        node.add(new Label("File Type:"), 0, 1);
        node.add(suffixField, 1, 1);
        return node;
    }
}
