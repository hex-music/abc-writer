package abc.writer.app.xml;

import java.util.Optional;
import xmlight.XmlNode;

/**
 *
 * @author hl
 */
public class XmlUtil {

    private XmlUtil() {
    }

    public static Optional<String> getOptionalString(XmlNode node, String attribute) {
        if (node.hasAttribute(attribute)) {
            return Optional.of(node.getAttribute(attribute));
        }
        return Optional.empty();
    }

    public static Optional<Boolean> getOptionalBoolean(XmlNode node, String attribute) {
        return getOptionalString(node, attribute).map(a -> true == Boolean.valueOf(a));
    }

    public static Integer getInteger(XmlNode node, String attribute) {
        if (node.hasAttribute(attribute) && node.getAttribute(attribute).matches("\\d+")) {
            return Integer.valueOf(node.getAttribute(attribute));
        }
        return null;
    }
}
