package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.FileType;
import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.gui.popup.AddFileDialog;
import java.io.File;
import java.util.Optional;

/**
 *
 * @author hl
 */
public class AddFileAction extends AbcWriterAction<Optional<File>> {

    private final File parent;

    public AddFileAction(AbcWriter app, File parent) {
        super(app);
        this.parent = parent;
    }

    @Override
    protected Optional<File> performAction() {
        AddFileDialog newFileDialog = new AddFileDialog();
        Optional<AbcDialog.Result> optResult = newFileDialog.showAndWait();
        if (!optResult.isPresent() || !optResult.get().equals(AbcDialog.Result.YES)) {
            return Optional.empty();
        }
        FileType type = newFileDialog.getFileType();
        String name = newFileDialog.getFilename();
        return Optional.of(new CreateFileFromTemplateAction(app, parent, type, name).actionPerformed());
    }

}
