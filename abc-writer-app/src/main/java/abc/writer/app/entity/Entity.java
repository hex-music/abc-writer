package abc.writer.app.entity;

import abc.writer.app.Constants;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author hl
 * @param <T>
 */
public interface Entity<T> {

    String getLabel();

    T getContent();

    public static class Factory {

        private Factory() {
        }

        public static FileInfo fileInfo(String label, String string) {
            if (!string.contains(Constants.INDEX)) {
                return null;
            }
            return FileInfo.of(label, getPreamble(string), tuneList(string));
        }

        public static List<Tune> tuneList(String string) {
            if (string.contains(Constants.INDEX)) {
                return getTunes(Arrays.asList(string.substring(string.indexOf(Constants.INDEX)).trim().split(Constants.INDEX)));
            }
            return Collections.emptyList();
        }

        private static String getPreambleString(String string) {
            return string.substring(0, string.indexOf(Constants.INDEX));
        }

        private static Preamble getPreamble(String string) {
            String preambleString = getPreambleString(string);
            if (preambleString == null || preambleString.trim().isEmpty()) {
                return null;
            }
            return Preamble.from(preambleString);
        }

        private static List<Tune> getTunes(List<String> tuneStrings) {
            return tuneStrings.stream()
                    .filter(s -> hasTune(s))
                    .map(s -> Constants.INDEX + s)
                    .map(Tune::from)
                    .collect(Collectors.toList());
        }

        private static boolean hasTune(String s) {
            return s != null && !s.isEmpty() && (s.contains("\nK:") || s.contains("\nM:"));
        }
    }
}
