package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.entity.Tune;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.Editor;
import java.io.File;

/**
 *
 * @author hl
 */
public class GoToTuneAction extends AbcWriterAction<Void> {

    private final File file;
    private final Tune tune;

    public GoToTuneAction(AbcWriter app, File file, Tune tune) {
        super(app);
        this.file = file;
        this.tune = tune;
    }

    @Override
    protected Void performAction() {
        AbcEditor abcEditor;
        if (app.isOpened(file)) {
            Editor editor = app.findByFile(file).get();
            if (!(isAbcEditor(editor))) {
                return null;
            }
            abcEditor = (AbcEditor) editor;
            app.setCurrentEditor(abcEditor);
        } else {
            abcEditor = (AbcEditor) new OpenAction(app, file).actionPerformed().orElse(null);
        }
        if (abcEditor != null) {
            abcEditor.goToTune(tune);
        }
        return null;
    }

    private boolean isAbcEditor(Editor editor) {
        return editor instanceof AbcEditor;
    }
}
