package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.entity.Voice;
import abc.writer.app.io.PsFileWriter;
import java.io.File;

/**
 *
 * @author hl
 */
public class ViewVoiceAction extends AbcWriterAction<Void> {

    private final Voice voice;

    public ViewVoiceAction(AbcWriter app, Voice voice) {
        super(app);
        this.voice = voice;
    }

    @Override
    protected Void performAction() {
        File voicePsFile = new PsFileWriter(voice).apply();
        new OpenInSystemAction(app, voicePsFile).performAction();
        return null;
    }

}
