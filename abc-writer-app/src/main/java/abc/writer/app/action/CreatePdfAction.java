package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.Editor;
import java.io.File;

/**
 *
 * @author hl
 */
public class CreatePdfAction extends AbstractPostScriptAction {

    public CreatePdfAction(AbcWriter app) {
        this(app, app.getCurrentEditor());
    }

    public CreatePdfAction(AbcWriter app, Editor editor) {
        super(app, editor, Action.CREATE_PDF);
    }

    @Override
    protected File performAction() {
        return apply();
    }
}
