package abc.writer.app.system;

import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;
import xmlight.DocumentToXmlNodeParser;
import xmlight.XmlNode;

/**
 *
 * @author hl
 */
public class WriterProperty {

    public static File getAwFile() {
        return awFile();
    }

    public static Optional<String> getProperty(String key) {
        XmlNode propertiesNode = awNode().getChild("properties");
        return propertiesNode.getChildren("property").stream()
                .filter(n -> n.hasAttribute("key") && n.getAttribute("key").equals(key))
                .findFirst()
                .map(n -> n.getText());
    }

    public static Optional<String> getComposer() {
        return get("composer");
    }

    public static Optional<String> get(String childName) {
        if (awNode().hasChildNamed(childName) && !awNode().getChild(childName).getText().isEmpty()) {
            return Optional.of(awNode().getChild(childName).getText());
        }
        return Optional.empty();
    }

    private static XmlNode awNode() {
        try {
            return new DocumentToXmlNodeParser(new FileInputStream(awFile())).parse();
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException("Could not parse AW file", ex);
        }
    }

    private static File awFile() {
        if (new File(Constants.AW_PATH).exists()) {
            return new File(Constants.AW_PATH);
        }
        File file = new File(Constants.AW_PATH);
        file.getParentFile().mkdirs();
        byte[] docByteArray = FileUtil.inputStreamToByteArray(WriterProperty.class.getResourceAsStream("/writer/aw.xml"));
        FileUtil.write(docByteArray, file.getAbsolutePath());
        return file;
    }

}
