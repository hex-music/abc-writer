package abc.writer.app.entity;

import abc.writer.app.Constants;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author hl
 */
public class Tune implements Entity<String> {

    private final String content;
    private final String label;
    private final Integer index;

    private Tune(String content, String label, Integer index) {
        this.content = content;
        this.label = label;
        this.index = index;
    }

    public static Tune from(String input) {
        if (!input.contains("\n")) {
            return null;
        }
        Integer index = Integer.valueOf(input.substring(2, input.indexOf("\n")).trim());
        String content = input.substring(input.indexOf("\n") + 1);
        if (!content.contains("\n") || !content.contains("K:")) {
            return null;
        }
        String label = content.substring(2, content.indexOf("\n")).trim();
        return new Tune(content, label, index);
    }

    @Override
    public String getLabel() {
        return label;
    }

    public List<String> getTitles() {
        return getFields('T');
    }

    public List<String> getComposers() {
        return getFields('C');
    }

    public Optional<String> getMeter() {
        return getFields('M').stream().findAny();
    }

    public Optional<String> getKey() {
        return getFields('K').stream().findAny();
    }

    public List<Voice> getVoices() {
        return VoiceFactory.from(this);
    }

    public Integer getNumberOfVoices() {
        return getVoices().size();
//        Integer size = getFields('V').size();
//        return size.equals(0) ? 1 : size;
    }

    private List<String> getFields(char field) {
        return Stream.of(content.split("\n")).filter(line -> line.startsWith(field + ":")).map(line -> line.substring(2).trim()).collect(Collectors.toList());
    }

    /**
     * The content of the tune does not include the index tag.
     * <p>
     * The first part of the content "X:n" is removed;
     *
     * @return
     */
    @Override
    public String getContent() {
        return content;
    }

    public Integer getIndex() {
        return index;
    }

    public String getIndexString() {
        return Constants.INDEX + index;
    }

    /**
     * Returns a string containing the entire abc code for the tune.
     *
     * @return
     */
    public String getAsString() {
        return getIndexString() + "\n" + getContent();
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
