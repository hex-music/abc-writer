package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.popup.CheatSheet;
import abc.writer.app.gui.popup.PostscriptFontsCheatSheet;

/**
 *
 * @author hl
 */
public class ShowPostscriptFontCheatSheetAction extends AbcWriterAction<Void> {

    private static final PostscriptFontsCheatSheet CONTENT = new PostscriptFontsCheatSheet();

    public ShowPostscriptFontCheatSheetAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        CheatSheet.of(app, 520, 750, CONTENT, "Postscript Font Cheat Sheet").show(app.getStage());
        return null;
    }
}
