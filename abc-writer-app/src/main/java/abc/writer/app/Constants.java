package abc.writer.app;

import java.io.File;

/**
 *
 * @author hl
 */
public final class Constants {

    public static final File PUBLIC_DIRECTORY = new File(System.getProperty("user.home") + "/AbcWriter/");
    public static final File PUBLIC_ABC_DIRECTORY = new File(PUBLIC_DIRECTORY.getAbsolutePath() + "/ABC/");
    public static final File PUBLIC_MIDI_DIRECTORY = new File(PUBLIC_DIRECTORY.getAbsolutePath() + "/MIDI/");
    public static final File PUBLIC_PDF_DIRECTORY = new File(PUBLIC_DIRECTORY.getAbsolutePath() + "/PDF/");
    public static final File PUBLIC_PS_DIRECTORY = new File(PUBLIC_DIRECTORY.getAbsolutePath() + "/PS/");
    public static final File PUBLIC_XML_DIRECTORY = new File(PUBLIC_DIRECTORY.getAbsolutePath() + "/XML/");
    public static final File WORKING_DIRECTORY = new File(System.getProperty("user.home") + "/.hex/.aw/");
    public static final File WORKING_DIRECTORY_TEMP = new File(WORKING_DIRECTORY.getAbsolutePath() + "/temp/");
    public static final File WORKING_DIRECTORY_ABC_TEMP = new File(WORKING_DIRECTORY.getAbsolutePath() + "/temp/abc/");
    public static final File WORKING_DIRECTORY_MIDI_TEMP = new File(WORKING_DIRECTORY.getAbsolutePath() + "/temp/midi/");
    public static final File WORKING_DIRECTORY_XML_TEMP = new File(WORKING_DIRECTORY.getAbsolutePath() + "/temp/xml/");
    public static final File WORKING_DIRECTORY_PREVIEW = new File(WORKING_DIRECTORY.getAbsolutePath() + "/preview/");
    public static final File TEMPLATES_DIRECTORY = new File(WORKING_DIRECTORY.getAbsolutePath() + "/templates/");
    public static final String AW_PATH = WORKING_DIRECTORY.getAbsolutePath() + "/aw.xml";
    public static final String LOG_PATH = WORKING_DIRECTORY.getAbsolutePath() + "/log.xml";
    public static final String INDEX = "X:";

    private Constants() {
    }
}
