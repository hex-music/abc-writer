package abc.writer.app.gui.control;

import abc.writer.app.AbcWriter;
import abc.writer.app.action.FindAction;
import abc.writer.app.action.GoToLineAction;
import abc.writer.app.action.ShowFileInfoAction;
import abc.writer.app.action.ShowHelpDialogAction;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.popup.ChordPopup;
import java.util.List;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.IndexRange;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author hl
 */
public class AbcNotesKeyEventHandler implements EventHandler<KeyEvent> {

    private final AbcWriter app;
    private final TextInputControlUtil controlUtil;
    private final AbcEditor editor;
    private final TextInputControl area;

    public AbcNotesKeyEventHandler(AbcWriter app, AbcEditor editor, TextInputControl area) {
        this.app = app;
        this.editor = editor;
        this.area = area;
        this.controlUtil = new TextInputControlUtil(area);
    }

    @Override
    public void handle(KeyEvent event) {
        double scrollTop = getScrollTop();
        Node eventSource = (Node) event.getSource();
        Bounds sourceNodeBounds = eventSource.localToScreen(eventSource.getBoundsInLocal());
        System.out.println("");
        System.out.println("Code: " + event.getCode());
        System.out.println("Char: " + event.getCharacter());
        System.out.println("Text: " + event.getText());
        System.out.println("");
        switch (event.getCode()) {
            case F1:
                new ShowHelpDialogAction(app).actionPerformed();
                event.consume();
                return;
            case ESCAPE:
                editor.clearBottom();
                event.consume();
                return;
            case DIGIT2:
                if (event.isShiftDown()) {
                    insertString("\"");
                    moveCaret(-1);
                    return;
                }
        }
        if (event.isControlDown()) {
            if (event.isShiftDown()) {
                switch (event.getCode()) {
                    case C:
                        toggleComment();
                        event.consume();
                        setScrollTop(scrollTop);
                        return;
                    case T:
                        new ShowFileInfoAction(app, editor.getFileInfo(), sourceNodeBounds).actionPerformed();
                        event.consume();
                        return;
                    case Z:
                        editor.revertToLastSaved();
                        event.consume();
                        return;
                    case UP:
                    case DOWN:
                        copyAndMoveText(event.getCode());
                        event.consume();
                        return;
                    case SPACE:
                        insertString(" || ");
                        return;
                }
            }
            switch (event.getCode()) {
                case G:
                    new GoToLineAction(app, controlUtil.getNumberOfLines())
                            .actionPerformed()
                            .ifPresent(controlUtil::goToLine);
                    event.consume();
                    return;
                case H:
                    new FindAction(app, editor, true).actionPerformed();
                    event.consume();
                    return;
                case O:
                    if (event.isAltDown()) {
                        insertString("ô");
                    }
                    return;
                case X:
                    if (area.getSelectedText().isEmpty()) {
                        area.selectRange(controlUtil.getCurrentLineStartPosition(), controlUtil.getCurrentLineEndPosition() + 1);
                        new ClipboardHelper(area).cut();
                    }
                    setScrollTop(scrollTop);
                    return;
                case Z:
                    area.undo();
                    return;
                case Y:
                    area.redo();
                    return;
                case DIGIT8:
                    insertString("[]");
                    moveCaret(-1);
                    return;
                case HOME:
                    area.home();
                    return;
                case END:
                    area.end();
                    return;
                case SPACE:
                    if (surroundingCharactersEquals("\"")) {
                        suggestChord();
                    } else {
                        insertString(" | ");
                    }
                    return;
            }
        }
        if (event.isAltDown()) {
            if (event.isShiftDown()) {
                switch (event.getCode()) {
                    case A:
                        insertString("à");
                        return;
                    case E:
                        insertString("è");
                        return;
                    case UP:
                    case DOWN:
                        moveText(event.getCode());
                        event.consume();
                        return;
                }
            }
            switch (event.getCode()) {
                case A:
                    insertString("á");
                    return;
                case E:
                    insertString("é");
                    return;
                case K:
                case M:
                case V:
                    insertCode(event.getCode().name());
                    return;
                case L:
                    toggleParanthesis();
                    return;
                case N:
                    insertString("!downbow!");
                    return;
                case U:
                    insertString("!upbow!");
                    return;
                case UP:
                    insertString("^");
                    return;
                case DOWN:
                    insertString("_");
                    return;
            }
        }
    }

    private void moveCaret(int steps) {
        area.positionCaret(area.getCaretPosition() + steps);
    }

    public void insertCode(String text) {
        insertString("[" + text + ":]");
        moveCaret(-1);
    }

    private double getScrollTop() {
        if (area instanceof TextArea) {
            return ((TextArea) area).getScrollTop();
        }
        return 0;
    }

    private void setScrollTop(double scrollTop) {
        if (area instanceof TextArea) {
            ((TextArea) area).setScrollTop(scrollTop);
        }
    }

    private boolean surroundingCharactersEquals(String string) {
        return area.getText(area.getCaretPosition() - 1, area.getCaretPosition()).equals(string)
                && area.getText(area.getCaretPosition(), area.getCaretPosition() + 1).equals(string);
    }

    private void copyAndMoveText(KeyCode code) {
        if (area.getSelectedText().isEmpty()) {
            String lineToCopyAndMove = controlUtil.getCurrentLineText();
            int posInLine = area.getCaretPosition() - controlUtil.getCurrentLineStartPosition();
            if (code.equals(KeyCode.DOWN)) {
                copyAndMoveSingleLineDown(lineToCopyAndMove, posInLine);
                return;
            }
            if (code.equals(KeyCode.UP)) {
                copyAndMoveSingleLineUp(lineToCopyAndMove, posInLine);
            }
        } else {
            // There are stil some errors here:
            IndexRange sel = area.getSelection();
            int pos = area.getCaretPosition();
            int startIndex = area.getSelectedText().startsWith("\n") ? sel.getStart() + 1 : sel.getStart();
            int endIndex = area.getSelectedText().endsWith("\n") ? sel.getEnd() - 1 : sel.getEnd();
            String textToCopyAndMove = area.getText().substring(controlUtil.getLineStartPosition(startIndex), controlUtil.getLineEndPosition(endIndex)).trim();
            String firstPart = area.getText().substring(0, controlUtil.getLineStartPosition(startIndex));
            String secondPart = area.getText().substring(controlUtil.getLineStartPosition(startIndex));
            area.setText(firstPart + textToCopyAndMove + "\n" + secondPart);
            if (code.equals(KeyCode.DOWN)) {
                area.selectRange(startIndex + textToCopyAndMove.length() + 1, endIndex + textToCopyAndMove.length() + 1);
            } else if (code.equals(KeyCode.UP)) {
                area.selectRange(startIndex, endIndex);
            }
        }
    }

    private void moveText(KeyCode code) {
        if (area.getSelectedText().isEmpty()) {
            String lineToMove = controlUtil.getCurrentLineText();
            int posInLine = area.getCaretPosition() - controlUtil.getCurrentLineStartPosition();
            if (code.equals(KeyCode.DOWN)) {
                moveSingleLineDown(lineToMove, posInLine);
                return;
            }
            if (code.equals(KeyCode.UP)) {
                moveSingleLineUp(lineToMove, posInLine);
            }
        } else {
            IndexRange sel = area.getSelection();
            int startIndex = area.getSelectedText().startsWith("\n") ? sel.getStart() + 1 : sel.getStart();
            int endIndex = area.getSelectedText().endsWith("\n") ? sel.getEnd() - 1 : sel.getEnd();
            String textToCopyAndMove = area.getText().substring(controlUtil.getLineStartPosition(startIndex), controlUtil.getLineEndPosition(endIndex)).trim();
            if (code.equals(KeyCode.DOWN)) {
                String firstPart = area.getText().substring(0, controlUtil.getLineStartPosition(startIndex));
                String secondPart = area.getText().substring(controlUtil.getLineStartPosition(endIndex + 1), controlUtil.getLineEndPosition(endIndex + 1));
                String thirdPart = area.getText().substring(controlUtil.getLineEndPosition(endIndex + 1) + 1);
                area.setText(firstPart + secondPart + "\n" + textToCopyAndMove + "\n" + thirdPart);
                area.selectRange(startIndex + secondPart.length() + 1, endIndex + secondPart.length() + 1);
            } else if (code.equals(KeyCode.UP)) {
                String tempPart = area.getText().substring(0, controlUtil.getLineStartPosition(startIndex)).trim();
                String firstPart = tempPart.substring(0, tempPart.lastIndexOf("\n"));
                String secondPart = tempPart.substring(tempPart.lastIndexOf("\n"));
                String thirdPart = area.getText().substring(controlUtil.getLineEndPosition(endIndex));
                area.setText(firstPart + "\n" + textToCopyAndMove + secondPart + thirdPart);
                area.selectRange(sel.getStart() - secondPart.length(), sel.getEnd() - secondPart.length());
            }

        }
    }

    private void copyAndMoveSingleLineDown(String lineToMove, int posInLine) {
        String firstPart = area.getText().substring(0, controlUtil.getCurrentLineEndPosition());
        String secondPart = area.getText().substring(controlUtil.getCurrentLineEndPosition());
        area.setText(firstPart + "\n" + lineToMove + secondPart);
        area.positionCaret(firstPart.length() + posInLine + 1);
    }

    private void copyAndMoveSingleLineUp(String lineToMove, int posInLine) {
        String firstPart = area.getText().substring(0, controlUtil.getCurrentLineStartPosition());
        String secondPart = area.getText().substring(controlUtil.getCurrentLineStartPosition());
        area.setText(firstPart + lineToMove + "\n" + secondPart);
        area.positionCaret(firstPart.length() + posInLine);
    }

    private void moveSingleLineDown(String lineToMove, int posInLine) {
        String firstPart = area.getText().substring(0, controlUtil.getCurrentLineStartPosition() - 1);
        String secondPart = controlUtil.getLineText(controlUtil.getCurrentLineEndPosition() + 1);
        String thirdPart = area.getText().substring(controlUtil.getCurrentLineEndPosition() + secondPart.length() + 1);
        area.setText(firstPart + "\n" + secondPart + "\n" + lineToMove + thirdPart);
        area.positionCaret(firstPart.length() + secondPart.length() + 2 + posInLine);
    }

    private void moveSingleLineUp(String lineToMove, int posInLine) {
        String secondPart = controlUtil.getLineText(controlUtil.getCurrentLineStartPosition() - 1);
        String firstPart = area.getText().substring(0, controlUtil.getCurrentLineStartPosition() - secondPart.length() - 1);
        String thirdPart = area.getText().substring(controlUtil.getCurrentLineEndPosition() + 1);
        area.setText(firstPart + lineToMove + "\n" + secondPart + "\n" + thirdPart);
        area.positionCaret(firstPart.length() + posInLine);
    }

    private void toggleComment() {
        if (area.getSelectedText().isEmpty()) {
            String firstPart = area.getText().substring(0, controlUtil.getCurrentLineStartPosition());
            String secondPart = area.getText().substring(controlUtil.getCurrentLineStartPosition());
            int caretPosition = area.getCaretPosition();
            if (controlUtil.getCurrentLineText().startsWith("% ")) {
                area.setText(firstPart + secondPart.substring(2));
                int newCaretPosition = caretPosition - 2;
                if (newCaretPosition < 0) {
                    newCaretPosition = 0;
                }
                area.positionCaret(newCaretPosition);
            } else {
                area.setText(firstPart + "% " + secondPart);
                area.positionCaret(caretPosition + 2);
            }
        } else {
            List<Integer> lines = controlUtil.getSelectedLineStarts();
            IndexRange selection = area.getSelection();
            if (controlUtil.getLineText(selection.getStart()).startsWith("% ")) {
                lines.stream().sorted((a, b) -> b.compareTo(a)).forEach(this::removeComment);
                area.selectRange(selection.getStart(), selection.getEnd() - lines.size() * 2);
            } else {
                lines.stream().sorted((a, b) -> b.compareTo(a)).forEach(this::addComment);
                area.selectRange(selection.getStart(), selection.getEnd() + lines.size() * 2);
            }
        }
    }

    private void insertString(String string) {
        area.insertText(area.getCaretPosition(), string);
    }

    private void addComment(Integer index) {
        String firstPart = area.getText().substring(0, index);
        String secondPart = area.getText().substring(index);
        area.setText(firstPart + "% " + secondPart);
    }

    private void removeComment(Integer index) {
        String firstPart = area.getText().substring(0, index);
        String secondPart = area.getText().substring(index);
        area.setText(firstPart + secondPart.substring(2));
    }

    private void toggleParanthesis() {
        IndexRange selection = area.getSelection();
        if (selection.getLength() > 0) {
            if (area.getSelectedText().startsWith("(") && area.getSelectedText().endsWith(")")) {
                String removedParenthesis = area.getSelectedText().substring(1, area.getSelectedText().length() - 1);
                area.cut();
                area.insertText(selection.getStart(), removedParenthesis);
            } else {
                area.insertText(selection.getStart(), "(");
                area.insertText(selection.getEnd() + 1, ")");
            }
        }
    }

    private void suggestChord() {
        ChordPopup popup = new ChordPopup(app, editor.getCurrentTune());
        Point2D coordinates = controlUtil.getCoordinates();
        popup.show(area, coordinates.getX(), coordinates.getY() - editor.LINE_HEIGHT);
        popup.setFocus();
        popup.setOnHiding(e -> {
            insertString(popup.getChord());
            moveCaret(1);
        });
    }
}
