package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.system.WriterLogger;

/**
 *
 * @author hl
 */
public class ViewLogFileAction extends AbcWriterAction<Void> {

    public ViewLogFileAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        new OpenInSystemAction(app, WriterLogger.getLogFile()).actionPerformed();
        return null;
    }

}
