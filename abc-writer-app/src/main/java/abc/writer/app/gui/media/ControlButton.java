package abc.writer.app.gui.media;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

/**
 *
 * @author hl
 */
public class ControlButton extends Button {

    private final Control control;

    private ControlButton(Control control, ImageView icon) {
        this.control = control;
        super.setDisabled(control.isInitiallyDisabled());
        super.setGraphic(icon);
        super.setPrefSize(32, 32);
    }

    public static ControlButton from(Control control) {
        ImageView icon = getIcon(control);
        return new ControlButton(control, icon);
    }

    public String getName() {
        return control.name();
    }

    private static ImageView getIcon(Control control) {
        ImageView view = new ImageView("/layout/image/icon/control/control_" + control.name().toLowerCase() + "_blue.png");
        view.getStyleClass().add("midi-control-button");
        return view;
    }

}
