package abc.writer.app.gui.popup;

import abc.writer.app.AbcWriter;
import abc.writer.app.entity.Tune;
import abc.writer.app.xml.XmlUtil;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import xmlight.DocumentToXmlNodeParser;
import xmlight.XmlNode;

/**
 *
 * @author hl
 */
public class ChordPopup extends ContextMenu {

    private final AbcWriter app;
    private final List<KeySignature> signatures;
    private final Key key;
    private final KeySignature keySignature;
    private String chord;

    public ChordPopup(AbcWriter app, Tune tune) {
        this.app = app;
        XmlNode node = new DocumentToXmlNodeParser(getClass().getResourceAsStream("/xml/circle-of-fifths.xml")).parse();
        signatures = node.getChildren("key-signature").stream().map(KeySignature::from).collect(Collectors.toList());
        key = Key.from(tune.getKey().orElse("C"));
        keySignature = find(key);
        chord = "";
        init();
    }

    private List<MenuItem> getMenuItems() {
        MenuItem tItem = new MenuItem(T());
        MenuItem sItem = new MenuItem(S());
        MenuItem dItem = new MenuItem(D());
        MenuItem ddItem = new MenuItem(DD());
        MenuItem sdItem = new MenuItem(SD());
        MenuItem tpItem = new MenuItem(Tp());
        MenuItem tpdItem = new MenuItem(TpD());
        MenuItem spItem = new MenuItem(Sp());
        MenuItem spdItem = new MenuItem(SpD());
        MenuItem dpItem = new MenuItem(Dp());
        MenuItem dpdItem = new MenuItem(DpD());
        List<MenuItem> items = Arrays.asList(tItem, sItem, dItem, ddItem, sdItem, tpItem, tpdItem, spItem, spdItem, dpItem, dpdItem);
        items.forEach(item -> item.setOnAction(e -> setChord(item)));
        return items;
    }

    private void init() {
        getItems().addAll(getMenuItems().stream().filter(mi -> !mi.getText().isEmpty()).collect(Collectors.toList()));
    }

    private void setChord(MenuItem item) {
        this.chord = item.getText();
        hide();
    }

    public void setFocus() {
        Platform.runLater(() -> {
            Node graphics = getItems().get(0).getGraphic();
            if (graphics != null) {
                graphics.requestFocus();
            }
        });
    }

    public String getChord() {
        return chord;
    }

    private KeySignature find(Key key) {
        switch (key.getMode()) {
            case MAJOR:
                return signatures.stream().filter(ks -> ks.getMajor().getKeySignature().equals(key.getKeySignature())).findAny().orElse(null);
            case MINOR:
                return signatures.stream().filter(ks -> ks.getMinor().getKeySignature().equals(key.getKeySignature())).findAny().orElse(null);
            case DORIAN:
                return signatures.stream().filter(ks -> ks.getDorian().getKeySignature().equals(key.getKeySignature())).findAny().orElse(null);
            case LYDIAN:
                return signatures.stream().filter(ks -> ks.getLydian().getKeySignature().equals(key.getKeySignature())).findAny().orElse(null);
            case MIXOLYDIAN:
                return signatures.stream().filter(ks -> ks.getMixolydian().getKeySignature().equals(key.getKeySignature())).findAny().orElse(null);
        }
        return null;
    }

    private KeySignature find(Integer index) {
        return signatures.stream().filter(ks -> ks.getIndex().equals(index)).findFirst().orElse(null);
    }

    private String T() {
        return key.asChord();
    }

    private String D() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? find(keySignature.getIndex() + 4) : find(keySignature.getIndex() + 1);
        return ks == null ? "" : ks.getMajor().asChord();
    }

    private String DD() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? find(keySignature.getIndex() + 5) : find(keySignature.getIndex() + 2);
        return ks == null ? "" : ks.getMajor().asChord();
    }

    private String S() {
        KeySignature ks = find(keySignature.getIndex() - 1);
        return ks == null ? "" : key.getMode().equals(Mode.MINOR) ? ks.getMinor().asChord() : ks.getMajor().asChord();
    }

    // D(S)
    private String SD() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? find(keySignature.getIndex() + 3) : null;
        return ks == null ? "" : ks.getMajor().asChord();
    }

    private String Tp() {
        KeySignature ks = find(keySignature.getIndex());
        return key.getMode().equals(Mode.MINOR) || key.getMode().equals(Mode.DORIAN) ? ks.getMajor().asChord() : ks.getMinor().asChord();
    }

    // D(Tp)
    private String TpD() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? find(keySignature.getIndex() + 1) : find(keySignature.getIndex() + 4);
        return ks == null ? "" : ks.getMajor().asChord();
    }

    private String Sp() {
        KeySignature ks = find(keySignature.getIndex() - 1);
        return ks == null ? "" : key.getMode().equals(Mode.MINOR) || key.getMode().equals(Mode.DORIAN) ? ks.getMajor().asChord() : ks.getMinor().asChord();
    }

    // D(Sp)
    private String SpD() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? null : find(keySignature.getIndex() + 3);
        return ks == null ? "" : ks.getMajor().asChord();
    }

    private String Dp() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? find(keySignature.getIndex() + 4) : find(keySignature.getIndex() + 1);
        return ks == null ? "" : ks.getMinor().asChord();
    }

    private String DpD() {
        KeySignature ks = key.getMode().equals(Mode.MINOR) ? find(keySignature.getIndex() + 8) : find(keySignature.getIndex() + 5);
        return ks == null ? "" : ks.getMajor().asChord();
    }

    private static class Key {

        private static final Pattern KEY_PATTERN = Pattern.compile("([A-G]{1})([b#]?)([a-z]{0,10})");
        private final String keySignature;
        private final Mode mode;

        private Key(String keySignature, Mode mode) {
            this.keySignature = keySignature;
            this.mode = mode;
        }

        public String getKeySignature() {
            return keySignature;
        }

        public Mode getMode() {
            return mode;
        }

        public String asChord() {
            return mode.equals(Mode.MINOR) || mode.equals(Mode.DORIAN) ? keySignature + "m" : keySignature;
        }

        @Override
        public String toString() {
            return getKeySignature() + getMode().asString();
        }

        public static Key of(String keySignature, Mode mode) {
            return new Key(keySignature, mode);
        }

        public static Key from(String input) {
            Matcher matcher = KEY_PATTERN.matcher(input);
            if (matcher.find()) {
                return new Key(matcher.group(1) + matcher.group(2), Mode.from(matcher.group(3)));
            }
            return new Key("C", Mode.MAJOR);
        }

    }

    private enum Mode {
        MAJOR, MINOR, DORIAN, LYDIAN, MIXOLYDIAN;

        public static Mode from(String string) {
            if (string == null || string.isEmpty()) {
                return MAJOR;
            }
            if (string.equals("m") || string.startsWith("min")) {
                return MINOR;
            }
            if (string.startsWith("d")) {
                return DORIAN;
            }
            if (string.startsWith("ly")) {
                return LYDIAN;
            }
            if (string.startsWith("mix")) {
                return MIXOLYDIAN;
            }
            return MAJOR;
        }

        public String asString() {
            return this.equals(MAJOR) ? "" : this.equals(MINOR) ? "m" : name().toLowerCase().substring(0, 3);
        }

    }

    private static class KeySignature {

        private final XmlNode node;

        private KeySignature(XmlNode node) {
            this.node = node;
        }

        public static KeySignature from(XmlNode node) {
            return new KeySignature(node);
        }

        public Integer getIndex() {
            Integer index = XmlUtil.getInteger(node, "number");
            if (node.getAttribute("signature").equals("b")) {
                return 0 - index;
            }
            return index;
        }

        public Key getMajor() {
            return Key.of(node.getAttribute("major"), Mode.MAJOR);
        }

        public Key getMinor() {
            return Key.of(node.getAttribute("minor"), Mode.MINOR);
        }

        public Key getDorian() {
            return Key.of(node.getAttribute("dorian"), Mode.DORIAN);
        }

        public Key getLydian() {
            return Key.of(node.getAttribute("lydian"), Mode.LYDIAN);
        }

        public Key getMixolydian() {
            return Key.of(node.getAttribute("mixolydian"), Mode.MIXOLYDIAN);
        }
    }
}
