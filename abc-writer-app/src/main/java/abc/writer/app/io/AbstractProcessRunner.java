package abc.writer.app.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hl
 * @param <T>
 */
public abstract class AbstractProcessRunner<T> implements Writer<T> {

    private final List<String> info = new ArrayList<>();
    private final List<String> error = new ArrayList<>();

    protected void logProcess(Process process) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                error.add(line);
                Logger.getLogger(PsFileWriter.class.getName()).log(Level.WARNING, line);
            }
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                info.add(line);
                Logger.getLogger(PsFileWriter.class.getName()).log(Level.INFO, line);
            }
        }

    }

    @Override
    public List<String> getProcessInfo() {
        return info;
    }

    @Override
    public List<String> getProcessError() {
        return error;
    }

}
