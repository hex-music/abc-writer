package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.entity.Tune;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author hl
 */
public class CreateNewFileFromTuneListAction extends AbcWriterAction<Optional<AbcEditor>> {

    private final List<Tune> tunes;

    public CreateNewFileFromTuneListAction(AbcWriter app, List<Tune> tunes) {
        super(app);
        this.tunes = tunes;
    }

    @Override
    protected Optional<AbcEditor> performAction() {
        String content = IntStream.rangeClosed(1, tunes.size())
                .mapToObj(i -> Constants.INDEX + i + "\n" + tunes.get(i - 1).getContent())
                .collect(Collectors.joining("\n\n"));
        try {
            File file = File.createTempFile("New File", FileType.ABC.suffix(), Constants.WORKING_DIRECTORY_ABC_TEMP);
            file.getParentFile().mkdirs();
            FileUtil.write(content.getBytes(StandardCharsets.UTF_8), file.getAbsolutePath());
            AbcEditor editor = new AbcEditor(app, file);
//            AbcEditor editor = new NewEditorTabAction(app).performAction();
//            editor.setEditorText(content);
            app.setCurrentEditor(editor);
            editor.markAsNew();
            return Optional.of(editor);
        } catch (IOException ex) {
            throw new AbcWriterException(ex);
        }
    }

}
