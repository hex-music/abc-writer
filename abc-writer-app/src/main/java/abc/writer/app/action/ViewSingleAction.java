package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import java.io.File;

/**
 *
 * @author hl
 */
public class ViewSingleAction extends AbstractPostScriptAction {

    public ViewSingleAction(AbcWriter app) {
        super(app, app.getCurrentEditor(), Action.PREVIEW_SINGLE);
    }

    @Override
    protected File performAction() {
        return apply();
    }

}
