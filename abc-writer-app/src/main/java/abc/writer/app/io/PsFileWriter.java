package abc.writer.app.io;

import abc.writer.app.Constants;
import abc.writer.app.entity.Voice;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author hl
 */
public class PsFileWriter extends AbstractProcessRunner<File> {

    private final File result;
    private final File tempPsFile;
    private final File abcFile;
    private Charset charset;

    public PsFileWriter(Voice voice) {
        this(createAbcFile(voice));
    }

    public PsFileWriter(File abcFile) {
        this(abcFile, createPreviewFile(abcFile));
    }

    public PsFileWriter(File abcFile, File resultFile) {
        this.abcFile = abcFile;
        this.result = resultFile;
        this.tempPsFile = new File(abcFile.getParent() + "/Out.ps");
        this.charset = StandardCharsets.UTF_8;
    }

    public PsFileWriter charset(Charset charset) {
        this.charset = Objects.requireNonNull(charset);
        return this;
    }

    @Override
    public File apply() {
        result.getParentFile().mkdirs();
        try {
            String cmdString = "/usr/bin/abcm2ps " + abcFile.getAbsolutePath() + " -O " + tempPsFile.getAbsolutePath();
            Process process = Runtime.getRuntime().exec(cmdString);
            logProcess(process);
            process.waitFor();
            FileUtil.write(getResultSource(tempPsFile), result.getAbsolutePath());
            cleanUp();
            return result;
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(PsFileWriter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private static File createPreviewFile(File abcFile) {
        String abcFileSuffix = abcFile.getName().substring(0, abcFile.getName().lastIndexOf("."));
        return new File(Constants.WORKING_DIRECTORY_PREVIEW + "/" + abcFileSuffix + ".ps");
    }

    private static File createAbcFile(Voice voice) {
        String name = voice.getTune().getLabel();
        String path = Constants.WORKING_DIRECTORY_PREVIEW + "/" + name + "-" + voice.getLabel() + ".abc";
        File abcFile = new File(path.replaceAll(" ", "_"));
        abcFile.getParentFile().mkdirs();
        FileUtil.write(voice.getAsAbcString().getBytes(StandardCharsets.UTF_8), abcFile.getAbsolutePath());
        return abcFile;
    }

    private byte[] getResultSource(File tempPsFile) throws FileNotFoundException {
        String resultString = FileUtil.inputStreamToString(new FileInputStream(tempPsFile));
        return Stream.of(resultString.split("\n"))
                .map(line -> line.startsWith("%%Title:")
                ? "%%Title:" + result.getName().substring(0, result.getName().lastIndexOf("."))
                : line)
                .collect(Collectors.joining("\n")).getBytes(charset);
    }

    private void cleanUp() {
        if (abcFile != null) {
            abcFile.delete();
        }
        if (tempPsFile != null) {
            tempPsFile.delete();
        }
    }
}
