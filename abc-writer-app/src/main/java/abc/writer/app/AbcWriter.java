package abc.writer.app;

import abc.writer.app.action.CloseWhileUnsavedChangesAction;
import abc.writer.app.action.SaveAllAction;
import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.AbcTabPane;
import abc.writer.app.gui.AbcWriterMenuBar;
import abc.writer.app.gui.Editor;
import abc.writer.app.gui.navigator.FileBrowser;
import abc.writer.app.gui.StatusBar;
import abc.writer.app.gui.TextEditor;
import java.io.File;
import java.util.List;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class AbcWriter extends Application {

    public static final int HEIGHT = 1000;
    public static final int WIDTH = 1200;
    private Stage stage;
    private BorderPane root;
    private BorderPane center;
    private TabPane navigatorPane;
    private MenuBar menuBar;
    private AbcTabPane editorTabPane;
    private Editor currentEditor;

    @Override
    public void start(Stage stage) throws Exception {
        setupStage(stage);
        editorTabPane = new AbcTabPane(this);
        VBox topPane = new VBox();
        menuBar = AbcWriterMenuBar.from(this);
        topPane.getChildren().add(menuBar);
        center = new BorderPane();
        root.setCenter(center);
        center.setCenter(editorTabPane);
        navigatorPane = new TabPane();
        navigatorPane.setBorder(Border.EMPTY);
        navigatorPane.getStyleClass().add("abc-navigator");
        root.setLeft(navigatorPane);
        root.setTop(topPane);
        root.setBottom(StatusBar.instance());
        addToNavigator(new FileBrowser(this), "Files");
    }

    public Stage getStage() {
        return stage;
    }

    public AbcEditor newEditor() {
        AbcEditor newEditor = editorTabPane.newAbcEditor();
        setCurrentEditor(newEditor);
        return newEditor;
    }

    public void setCurrentEditor(Editor editor) {
        this.currentEditor = editor;
        if (editor instanceof AbcEditor) {
            editorTabPane.addEditor((AbcEditor) editor);
        } else {
            editorTabPane.addEditor((TextEditor) editor);
        }
    }

    public void closeCurrentEditor() {
        if (currentEditor != null) {
            editorTabPane.closeEditor(currentEditor);
        }
    }

    public Editor getCurrentEditor() {
        return currentEditor;
    }

    public boolean isOpened(File file) {
        return editorTabPane.isOpened(file);
    }

    public Optional<Editor> findByFile(File file) {
        return editorTabPane.findByFileName(file.getAbsolutePath());
    }

    public List<Editor> getOpenEditors() {
        return editorTabPane.getEditors();
    }

    public void addEditorsPaneListener(ListChangeListener<? super Tab> listener) {
        editorTabPane.addChangeListener(listener);
    }

    public void setRight(Node node) {
        root.setRight(node);
    }

    public void setBottom(Node node) {
        root.setBottom(node);
    }

    public void addToNavigator(Node node, String label) {
        navigatorPane.getTabs().add(new Tab(label, node));
    }

    @Override
    public void stop() throws Exception {
        getStage().fireEvent(new WindowEvent(getStage(), WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    private void close(WindowEvent event) {
        event.consume();
        if (editorTabPane.hasChanges()) {
            boolean withCancelOption = false;
            AbcDialog.Result result = new CloseWhileUnsavedChangesAction(this, withCancelOption).actionPerformed();
            switch (result) {
                case YES:
                    Platform.exit();
                    break;
                case SAVE_AND_CLOSE:
                    new SaveAllAction(this).actionPerformed();
                    Platform.exit();
                    break;
            }
        } else {
            Platform.exit();
        }
    }

    private void setupStage(Stage stage) {
        Scene scene = setupScene();
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (event) -> {
            if (event.isAltDown()) {
                event.consume();
            }
        });
        stage.setOnCloseRequest((WindowEvent e) -> {
            close(e);
        });
        stage.setTitle("Abc Writer");
        stage.setScene(scene);
        stage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::close);
        stage.setMinWidth(WIDTH);
        stage.setMinHeight(HEIGHT);
        stage.getIcons().add(new Image("layout/image/MariaBarbaraBach.png"));
        this.stage = stage;
        stage.show();
    }

    private Scene setupScene() {
        root = new BorderPane();
        root.setId("root-pane");
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/layout/style/style.css");
        return scene;
    }
}
