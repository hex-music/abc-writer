package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hl
 */
public class OpenInSystemAction extends AbcWriterAction<Void> {

    private final File file;

    public OpenInSystemAction(AbcWriter app, File file) {
        super(app);
        this.file = file;
    }

    @Override
    protected Void performAction() {
        new Thread(() -> {
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException ex) {
                Logger.getLogger(OpenInSystemAction.class.getName()).log(Level.SEVERE, "Could not open \"" + file.getAbsolutePath() + "\".", ex);
            }
        }).start();

        return null;
    }

}
