package abc.writer.app.gui.control;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

/**
 *
 * @author hl
 */
public class CloseLabel extends Label {

    private static final String DEFAULT_LABEL = "✕";

    private CloseLabel(String label, String tooltip, CloseListener listener) {
        super(label == null ? DEFAULT_LABEL : label);
        init(listener, tooltip);
    }

    public static CloseLabel from(CloseListener listener) {
        return new CloseLabel(DEFAULT_LABEL, null, listener);
    }

    public static CloseLabel of(CloseListener listener, String tooltip) {
        return new CloseLabel(DEFAULT_LABEL, tooltip, listener);
    }

    private void init(CloseListener listener, String tooltip) {
        getStyleClass().add("abc-close");
        setAlignment(Pos.TOP_RIGHT);
        if (listener != null) {
            setOnMouseClicked((e) -> listener.closed());
        }
        if (tooltip != null) {
            Tooltip.install(this, new Tooltip(tooltip));
        }
    }

    public static class Builder {

        private String label = DEFAULT_LABEL;
        private String tooltipText;
        private CloseListener listener;

        public Builder label(String text) {
            this.label = text;
            return this;
        }

        public Builder tooltip(String text) {
            this.tooltipText = text;
            return this;
        }

        public Builder listener(CloseListener listener) {
            this.listener = listener;
            return this;
        }

        public CloseLabel apply() {
            return new CloseLabel(label, tooltipText, listener);
        }
    }
}
