package abc.writer.app.io;

import abc.writer.app.AbcWriterException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Track;

/**
 *
 * @author hl
 */
public class MidiService implements MetaEventListener {

    public static final int END_OF_TRACK_MESSAGE = 47;
    private static final int DEFAULT_NUMBER_OF_LOOPS = 5;
    private static final long SECOND_IN_MICROS = 1_000_000l;
    private Sequencer sequencer;
    private Boolean loop = Boolean.FALSE;
    private SimpleBooleanProperty runningProperty;

    private MidiService() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            setSequencerListener();
            runningProperty = new SimpleBooleanProperty(false);
        } catch (MidiUnavailableException ex) {
            sequencer = null;
        }
    }

    public static MidiService get() {
        return new MidiService();
    }

    private void setSequencerListener() {
        sequencer.addMetaEventListener(this);
    }

    public void load(File midiFile) {
        try {
            load(new FileInputStream(midiFile));
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException(ex);
        }
    }

    public void load(InputStream inputStream) {
        setSequence(createSequence(inputStream));
    }

    private Sequence createSequence(InputStream stream) {
        try {
            if (!stream.markSupported()) {
                stream = new BufferedInputStream(stream);
            }
            Sequence sequence = MidiSystem.getSequence(stream);
            stream.close();
            return sequence;
        } catch (InvalidMidiDataException | IOException ex) {
            throw new AbcWriterException(ex);
        }
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
        if (this.loop) {
            sequencer.setLoopCount(DEFAULT_NUMBER_OF_LOOPS);
        } else {
            sequencer.setLoopCount(0);
        }
    }

    private void setSequence(Sequence sequence) {
        if (sequencer != null && sequence != null && sequencer.isOpen()) {
            try {
                sequencer.setSequence(sequence);
            } catch (InvalidMidiDataException ex) {
                throw new AbcWriterException(ex);
            }
        }
    }

    public Sequence getSequence() {
        return sequencer.getSequence();
    }

    public Sequencer getSequencer() {
        return sequencer;
    }

    public List<Track> getTracks() {
        return Arrays.asList(getSequence().getTracks());
    }

    @Override
    public void meta(MetaMessage event) {
        if (event.getType() == END_OF_TRACK_MESSAGE) {
            if (sequencer != null && sequencer.isOpen() && loop) {
                sequencer.start();
            }
        }
    }

    public void stop() {
        if (sequencer != null && sequencer.isOpen()) {
            sequencer.stop();
            runningProperty.set(sequencer.isRunning());
            sequencer.setMicrosecondPosition(0);
        }
    }

    public void goTo(long seconds) {
        long pos = seconds * SECOND_IN_MICROS;
        if (pos < 0) {
            pos = 0;
        }
        sequencer.setMicrosecondPosition(pos);
    }

    public void rewind(long seconds) {
        sequencer.stop();
        long microInterval = seconds * SECOND_IN_MICROS;
        long pos = sequencer.getMicrosecondPosition() - microInterval;
        if (pos < 0) {
            pos = 0;
        }
        sequencer.setMicrosecondPosition(pos);
        sequencer.start();
        runningProperty.set(sequencer.isRunning());
    }

    public void fastforward(long seconds) {
        sequencer.stop();
        runningProperty.set(sequencer.isRunning());
        long microInterval = seconds * SECOND_IN_MICROS;
        long pos = sequencer.getMicrosecondPosition() + microInterval;
        if (pos > sequencer.getMicrosecondLength()) {
            pos = sequencer.getMicrosecondLength();
        }
        sequencer.setMicrosecondPosition(pos);
        sequencer.start();
        runningProperty.set(sequencer.isRunning());
    }

    public void pause() {
        if (sequencer != null && sequencer.isOpen()) {
            sequencer.stop();
            runningProperty.set(sequencer.isRunning());
        }
    }

    public void open() {
        if (sequencer != null && !sequencer.isOpen()) {
            try {
                sequencer.open();
            } catch (MidiUnavailableException ex) {
                throw new AbcWriterException(ex);
            }
        }
    }

    public void play() {
        if (sequencer != null && sequencer.isOpen()) {
            sequencer.start();
            runningProperty.set(sequencer.isRunning());
        }
    }

    public void close() {
        if (sequencer != null && sequencer.isOpen()) {
            sequencer.close();
            runningProperty.set(sequencer.isRunning());
        }
    }

    public void togglePlayPaus() {
        if (sequencer != null && sequencer.isOpen()) {
            if (isRunning()) {
                pause();
            } else {
                play();
            }
        }
    }

    public Duration getDuration() {
        if (sequencer != null && sequencer.isOpen()) {
            return Duration.ofNanos(sequencer.getMicrosecondLength() * 1000);
        }
        return Duration.ZERO;
    }

    public Duration getElapsedTime() {
        if (sequencer != null && sequencer.isOpen()) {
            return Duration.ofNanos(sequencer.getMicrosecondPosition() * 1000);
        }
        return Duration.ZERO;
    }

    public SimpleBooleanProperty getRunningProperty() {
        return runningProperty;
    }

//    /**
//     * Returns the current position in micro seconds
//     *
//     * @return
//     */
//    public long getPosition() {
//        return sequencer.getMicrosecondPosition();
//    }
    public void addListener(MetaEventListener listener) {
        sequencer.addMetaEventListener(listener);
    }

    public boolean isRunning() {
        return sequencer.isRunning();
    }
}
