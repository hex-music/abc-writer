package abc.writer.app.gui;

import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.action.CloseWhileUnsavedChangesAction;
import abc.writer.app.action.SaveEditorContentAction;
import abc.writer.app.gui.control.CloseListener;
import abc.writer.app.gui.control.EditorListener;
import abc.writer.app.gui.control.TextInputControlUtil;
import abc.writer.app.io.FileMonitor;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Bounds;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author hl
 */
public class TextEditor extends Tab implements Editor<File> {

    private final AbcWriter app;
    private final BorderPane borderPane;
    private final List<CloseListener> closeListeners = new ArrayList<>();
    private final List<EditorListener> editorListeners = new ArrayList<>();
    private final TextArea textArea;
    private final StatusBar statusBar = StatusBar.instance();
    private final TextInputControlUtil controlUtil;
    private AbcTabPane abcTabPane;
    private File file;
    private String key;
    private State state;
    private String originalText = "";
    private String text = "";

    public TextEditor(AbcWriter app, File file) {
        this(app, file, file.getName());
    }

    public TextEditor(AbcWriter app, File file, String label) {
        this.app = app;
        this.file = file;
        this.key = this.file.getAbsolutePath();
        this.textArea = new TextArea();
        super.setText(label);
        updateState(State.UNCHANGED);
        controlUtil = new TextInputControlUtil(textArea);
        borderPane = new BorderPane();
        super.setContent(borderPane);
        update(this.file);
        if (file != null) {
            try {
                FileMonitor.getInstance().addFileChangeListener(this::update, file, FileMonitor.DEFAULT_LISTENER_INTERVAL);
            } catch (FileNotFoundException e) {
                Logger.getLogger(AbcEditor.class.getName()).log(Level.WARNING, "Could not register listener for file " + file, e);
            }
        }
        init();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getEditorText() {
        return textArea.getText();
    }

    private void init() {
        createMenu();
        textArea.caretPositionProperty().addListener((observable, oldValue, newValue) -> {
            showCaretPosition(newValue);
        });
        textArea.setWrapText(false);
        textArea.getStyleClass().add("abc-text-area");
        textArea.setText(text);
        borderPane.setCenter(textArea);
//        textArea.addEventHandler(KeyEvent.KEY_PRESSED, new NotesKeyEventHandler(app, this, textArea));
        textArea.textProperty().addListener((observable, oldValue, newValue) -> updateText(newValue));
        setOnCloseRequest((e) -> {
            if (isChanged()) {
                AbcDialog.Result result = new CloseWhileUnsavedChangesAction(app).actionPerformed();
                switch (result) {
                    case CANCEL:
                        e.consume();
                        break;
                    case SAVE_AND_CLOSE:
                        new SaveEditorContentAction(app, this).actionPerformed();
                        break;
                    default:
                        state = State.CHANGE_IGNORED;
                        break;
                }
            }
            closeListeners.forEach(CloseListener::closed);
        });
    }

    @Override
    public final void update(File input) {
        if (input != null) {
            file = input;
            if (!file.exists()) {
                updateState(State.DELETED);
                updateEditorStyle();
                FileMonitor.getInstance().removeFileChangeListener(this::update, file);
                return;
            }
            key = file.getAbsolutePath();
            super.setText(file.getName());
            super.setTooltip(new Tooltip(key));
            try {
                int pos = textArea.getCaretPosition();
                double scrollTop = textArea.getScrollTop();
                text = FileUtil.inputStreamToString(new FileInputStream(file));
                updateOriginalTextToCurrent();
                textArea.setText(text);
                textArea.positionCaret(pos);
                textArea.setScrollTop(scrollTop);
            } catch (FileNotFoundException ex) {
                throw new AbcWriterException("Could not read file", ex);
            }
        }
    }

    @Override
    public File get() {
        return file;
    }

    @Override
    public String getLabel() {
        return file == null ? "New File" : file.getName();
    }

    @Override
    public void requestTextAreaFocus() {
        textArea.requestFocus();
    }

    @Override
    public void setAbcTabPane(AbcTabPane tabPane) {
        this.abcTabPane = tabPane;
    }

    @Override
    public void setEditorText(String text) {
        textArea.setText(text);
        updateState(State.NEW);
    }

    @Override
    public boolean isChanged() {
        return state != null && (state.equals(State.DELETED) || state.equals(State.CHANGED) || state.equals(State.NEW));
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public Bounds getEditorBounds() {
        return textArea.localToScreen(textArea.getBoundsInLocal());
    }

    private void updateText(String text) {
        this.text = text;
        updateState(originalText.equals(text) ? State.UNCHANGED : State.CHANGED);
    }

    private void updateOriginalTextToCurrent() {
        if (!originalText.equals(text)) {
            originalText = text;
        }
    }

    private void updateEditorStyle() {
        if (state.equals(State.DELETED)) {
            textArea.getStyleClass().addAll("deleted-content");
        } else {
            textArea.getStyleClass().removeAll("deleted-content");
        }
    }

    public void addCloseListener(CloseListener listener) {
        this.closeListeners.add(listener);
    }

    public void addEditorListener(EditorListener listener) {
        this.editorListeners.add(listener);
    }

    @Override
    public void setSaved() {
        updateOriginalTextToCurrent();
        updateState(State.UNCHANGED);
        updateEditorStyle();
    }

    @Override
    public void setUnSaved() {
        updateOriginalTextToCurrent();
        updateState(State.NEW);
        updateEditorStyle();
    }

    @Override
    public void setIgnoreChanges() {
        updateState(State.CHANGE_IGNORED);
    }

    private void showCaretPosition(Number number) {
        statusBar.rightMessage(getCurrentCaretPosition());
    }

    String getCurrentCaretPosition() {
        int positionOnLIne = controlUtil.getPositionOnCurrentLine() + 1;
        return controlUtil.getCurrentLineIndex() + ":" + positionOnLIne;
    }

    private void updateState(State newState) {
        if (state != null && state.equals(State.NEW) && !newState.equals(State.CHANGE_IGNORED)) {
            return;
        }
        state = newState;
        if (state.equals(State.CHANGED)) {
            getStyleClass().add("changed-tab");
        } else {
            getStyleClass().removeAll("changed-tab");
        }
        if (state.equals(State.NEW)) {
            getStyleClass().add("new-tab");
        } else {
            getStyleClass().removeAll("new-tab");
        }
        if (state.equals(State.DELETED)) {
            getStyleClass().add("deleted-tab");
            setTooltip(new Tooltip("This file has been externally deleted\nYou can restore it by saving it."));
        } else {
            if (file == null) {
                setTooltip(null);
            } else {
                setTooltip(new Tooltip(key));
            }
            getStyleClass().removeAll("deleted-tab");
        }
        editorListeners.forEach(listener -> {
            listener.stateChanged(this.state);
        });
    }

    public void close() {
        abcTabPane.closeEditor(this);
    }

    private void createMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem closeItem = new MenuItem("Close");
        closeItem.setOnAction((e) -> close());
        MenuItem closeAllItem = new MenuItem("Close all");
        closeAllItem.setOnAction((e) -> abcTabPane.closeAll());
        MenuItem closeOtherItem = new MenuItem("Close other");
        closeOtherItem.setOnAction((e) -> abcTabPane.closeOther(this));

        menu.getItems().addAll(closeItem, closeAllItem, closeOtherItem);
        setContextMenu(menu);
    }

}
