package abc.writer.app.io;

import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.FileInfo;
import abc.writer.app.entity.Tune;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hl
 */
public class XmlFileWriter extends AbstractProcessRunner<File> {

    private static final String ABC_XML_PYTHON = System.getProperty("user.home") + "/.hex/.aw/.abc2xml/abc2xml.py";
    private final File abcFile;
    private File xmlTempDirectory;
    private FileInfo info;
    private File result;

    public XmlFileWriter(File abcFile) {
        this.abcFile = abcFile;
    }

    @Override
    public File apply() {
        try {
            xmlTempDirectory = new File(Constants.WORKING_DIRECTORY_XML_TEMP.getAbsolutePath() + "/" + getXmlDir().replaceAll(" ", "_"));
            xmlTempDirectory.mkdirs();
            result = new File(Constants.PUBLIC_XML_DIRECTORY + "/" + getXmlDir().replaceAll("_", " ") + "/");
            result.mkdirs();
            info = Entity.Factory.fileInfo("temp-file", FileUtil.inputStreamToString(new FileInputStream(abcFile)));
            info.getContent().forEach(this::createTuneFile);
            xmlTempDirectory.delete();
            return result;
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException(ex);
        }
    }

    private void createTuneFile(Tune tune) {
        try {
            Constants.WORKING_DIRECTORY_ABC_TEMP.mkdirs();
            File tempTuneFile = File.createTempFile(tune.getLabel().replaceAll(" ", "_"), FileType.ABC.suffix(), Constants.WORKING_DIRECTORY_ABC_TEMP);
            String preamble = info.getPreamble().map(p -> p.getContent()).orElse("");
            String tuneFileContent = preamble + tune.getAsString();
            FileUtil.write(tuneFileContent.getBytes(StandardCharsets.UTF_8), tempTuneFile.getAbsolutePath());
            String cmdString = ABC_XML_PYTHON + " " + tempTuneFile.getAbsolutePath() + " -o " + xmlTempDirectory.getAbsolutePath() + "/";
            Process process = Runtime.getRuntime().exec(cmdString);
            logProcess(process);
            process.waitFor();
            File xmlFile = new File(result.getAbsolutePath() + "/" + getXmlName(tune));
            File tempXmlFile = new File(xmlTempDirectory.getAbsolutePath() + "/" + tempTuneFile.getName().substring(0, tempTuneFile.getName().lastIndexOf(".")) + FileType.XML.suffix());
            byte[] tempData = FileUtil.inputStreamToByteArray(new FileInputStream(tempXmlFile));
            FileUtil.write(tempData, xmlFile.getAbsolutePath());
            tempTuneFile.delete();
            tempXmlFile.delete();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(XmlFileWriter.class.getName()).log(Level.SEVERE, "Could not create XML file of Tune \"" + tune.getLabel() + "\"", ex);
        }
    }

    private String getXmlDir() {
        return abcFile.getName().substring(0, abcFile.getName().lastIndexOf(".")) + "/";
    }

    private String getXmlName(Tune tune) {
        return tune.getLabel() + FileType.XML.suffix();
    }

}
