package abc.writer.app.gui.popup;

import abc.writer.app.io.FileUtil;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author hl
 */
public class MidiChannelsCheatSheet extends TableView {

    private static final Pattern INDEX_PATTERN = Pattern.compile("\\s{4}(\\d+).*");
    private final TableColumn indexColumn;
    private final TableColumn labelColumn;
    private final String midiChanelsSource;

    public MidiChannelsCheatSheet() {
        indexColumn = new TableColumn("Index");
        labelColumn = new TableColumn("Channel Name");
        midiChanelsSource = getSource();
        super.getColumns().addAll(indexColumn, labelColumn);
        init();
    }

    private void init() {
        indexColumn.setCellValueFactory(new PropertyValueFactory<MidiChannel, Integer>("index"));
        labelColumn.setCellValueFactory(new PropertyValueFactory<MidiChannel, String>("label"));

        List<MidiChannel> midiChannels = Stream.of(midiChanelsSource.split("\n"))
                .filter(line -> line.startsWith("    "))
                .map(MidiChannel::new)
                .sorted()
                .collect(Collectors.toList());
        super.setItems(FXCollections.observableList(midiChannels));
    }

    private static String getSource() {
        return FileUtil.inputStreamToString(MidiChannelsCheatSheet.class.getResourceAsStream("/text/midi-channels.txt"));
    }

    public static class MidiChannel implements Comparable<MidiChannel> {

        private final SimpleIntegerProperty index;
        private final SimpleStringProperty label;

        private MidiChannel(String row) {
            index = new SimpleIntegerProperty(createIndex(row));
            label = new SimpleStringProperty(createLabel(row));
        }

        private static String createLabel(String row) {
            return row.trim().substring(row.trim().indexOf(" ")).trim();
        }

        public Integer getIndex() {
            return index.get();
        }

        public String getLabel() {
            return label.get();
        }

        private static Integer createIndex(String row) {
            Matcher matcher = INDEX_PATTERN.matcher(row);
            if (matcher.find()) {
                return Integer.valueOf(matcher.group(1)) - 1;
            }
            return null;
        }

        @Override
        public int compareTo(MidiChannel o) {
            return this.getIndex().compareTo(o.getIndex());
        }
    }
}
