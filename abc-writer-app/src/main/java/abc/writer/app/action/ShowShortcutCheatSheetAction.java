package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.popup.CheatSheet;
import abc.writer.app.gui.popup.ShortcutCheatSheet;

/**
 *
 * @author hl
 */
public class ShowShortcutCheatSheetAction extends AbcWriterAction<Void> {

    private static final ShortcutCheatSheet CONTENT = new ShortcutCheatSheet();

    public ShowShortcutCheatSheetAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        CheatSheet.of(app, 680, 750, CONTENT, "Shortcut Cheat Sheet").show(app.getStage());
        return null;
    }
}
