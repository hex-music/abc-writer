package abc.writer.app.gui;

import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.AbcWriter;
import abc.writer.app.action.CloseWhileUnsavedChangesAction;
import abc.writer.app.action.SaveEditorContentAction;
import abc.writer.app.system.WriterLogger;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ListChangeListener;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.util.Duration;

/**
 *
 * @author hl
 */
public class AbcTabPane extends TabPane {

    private final AbcWriter app;

    public AbcTabPane(AbcWriter app) {
        this.app = app;
        init();
    }

    public boolean hasChanges() {
        return getEditors().stream().filter(e -> e.isChanged()).findAny().isPresent();
    }

    public void addEditor(Editor editor) {
        if (editor == null) {
            return;
        }
        if (!contains(editor)) {
            WriterLogger.setLog(WriterLogger.LAST_OPENED_FILE, editor.getKey());
            addEditors(editor);
            editor.setAbcTabPane(this);
        }
        setSelectedEditor(editor);
    }

    private void addEditors(Editor... editors) {
        getTabs().addAll(Stream.of(editors).filter(e -> e instanceof AbcEditor || e instanceof TextEditor)
                .map(e -> e instanceof AbcEditor ? (AbcEditor) e : (TextEditor) e)
                .collect(Collectors.toList()));
    }

    public AbcEditor newAbcEditor() {
        return new AbcEditor(app, this);
    }

    public void addChangeListener(ListChangeListener<? super Tab> listener) {
        getTabs().addListener(listener);
    }

    void closeOther(Editor remainingEditor) {
        List<Editor> editorsToClose = getEditors().stream().filter(e -> !e.equals(remainingEditor)).collect(Collectors.toList());
        closeEditors(editorsToClose);
    }

    public boolean isOpened(File file) {
        return findByFileName(file.getAbsolutePath()).isPresent();
    }

    public Optional<Editor> findByFileName(String fileName) {
        return getEditors().stream().filter(editor -> editor.getKey().equals(fileName)).findAny();
    }

    private void closeEditors(List<Editor> editorsToClose) {
        if (editorsToClose.stream().anyMatch(e -> e.isChanged())) {
            AbcDialog.Result result = new CloseWhileUnsavedChangesAction(app, true).actionPerformed();
            switch (result) {
                case CANCEL:
                    editorsToClose = editorsToClose.stream().filter(e -> !e.isChanged()).collect(Collectors.toList());
                    break;
                case SAVE_AND_CLOSE:
                    editorsToClose.stream().filter(e -> e.isChanged()).forEach(e -> {
                        new SaveEditorContentAction(app, e).actionPerformed();
                    });
                    break;
                default:
                    break;
            }
        }
        getTabs().removeAll(editorsToClose);
    }

    public void closeEditor(Editor editorToClose) {
        closeEditors(Collections.singletonList(editorToClose));
    }

    void closeAll() {
        closeEditors(getEditors());
    }

    private boolean contains(Editor editor) {
        if (editor == null) {
            return false;
        }
        return getEditors().stream()
                .filter(e -> e != null && e.getKey().equals(editor.getKey()))
                .findAny()
                .isPresent();
    }

    public List<Editor> getEditors() {
        return getTabs().stream()
                .filter(t -> t instanceof Editor)
                .map(e -> e instanceof AbcEditor ? (AbcEditor) e : (TextEditor) e)
                .collect(Collectors.toList());
    }
//
//    public List<AbcEditor> getAbcEditors() {
//        return getTabs().stream()
//                .filter(t -> t instanceof AbcEditor)
//                .map((tab) -> (AbcEditor) tab)
//                .collect(Collectors.toList());
//    }
//
//    public List<TextEditor> getTextEditors() {
//        return getTabs().stream()
//                .filter(t -> t instanceof TextEditor)
//                .map((tab) -> (TextEditor) tab)
//                .collect(Collectors.toList());
//    }

    private void setSelectedEditor(Editor editor) {
        getEditors().stream()
                .filter(e -> e.getKey().equals(editor.getKey()))
                .findFirst()
                .ifPresent(this::setSelected);
    }

    private void setSelected(Editor editor) {
        if (editor instanceof AbcEditor) {
            this.getSelectionModel().select((AbcEditor) editor);
        } else if (editor instanceof TextEditor) {
            this.getSelectionModel().select((TextEditor) editor);
        }
        editor.requestTextAreaFocus();
    }

    private void init() {
        super.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue == null) {
                        StatusBar.instance().clearRight();
                    } else if (newValue instanceof AbcEditor) {
                        AbcEditor editor = (AbcEditor) newValue;
                        app.setCurrentEditor(editor);
                        new Timeline(new KeyFrame(Duration.millis(50), e -> {
                            StatusBar.instance().rightMessage(editor.getCurrentCaretPosition());
                            editor.showCurrentTune();
                            editor.requestTextAreaFocus();
                        })).play();
                    } else if (newValue instanceof TextEditor) {
                        TextEditor editor = (TextEditor) newValue;
                        app.setCurrentEditor(editor);
                    }
                });
    }
}
