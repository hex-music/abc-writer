package abc.writer.app.gui.popup;

import abc.writer.app.gui.popup.AbcDialog.Result;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;

/**
 *
 * @author hl
 */
public class SplitOptionsDialog extends AbcDialog<Result> {

    public static final String INCLUDE_PREAMBLE = "include-preamble";
    public static final String USE_DEFAULT_DIRECTORY = "use-default-directory";
    private final Map<String, String> options;
    private final String defaultDirectory;

    public SplitOptionsDialog(String defaultDirectory) {
        options = new HashMap<>();
        this.defaultDirectory = defaultDirectory;
        super.setTitle("Split Tunes Options");
        super.setHeaderText(null);
        super.setGraphic(getOptionsPanel());
        final DialogPane dialogPane = getDialogPane();
        dialogPane.setPrefSize(500, 100);
        dialogPane.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
        setResultConverter((param) -> {
            return param.equals(ButtonType.OK)
                    ? AbcDialog.Result.YES
                    : AbcDialog.Result.CANCEL;
        });
    }

    public Map<String, String> getOptions() {
        return options;
    }

    private GridPane getOptionsPanel() {
        options.put(INCLUDE_PREAMBLE, Boolean.FALSE.toString());
        options.put(USE_DEFAULT_DIRECTORY, Boolean.TRUE.toString());
        GridPane pane = new GridPane();
        pane.setHgap(10);

        Label useDefaultDirectoryLabel = new Label("Use Default Directory");
        String defaultDirectoryTooltip = "Use the default directory: \n\t" + defaultDirectory;
        Tooltip.install(useDefaultDirectoryLabel, new Tooltip(defaultDirectoryTooltip));
        pane.add(useDefaultDirectoryLabel, 0, 0);
        CheckBox useDefaultDirectoryCheckbox = new CheckBox();
        useDefaultDirectoryCheckbox.setSelected(true);
        Tooltip.install(useDefaultDirectoryCheckbox, new Tooltip(defaultDirectoryTooltip));
        useDefaultDirectoryCheckbox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Boolean use = !newValue;
            options.put(USE_DEFAULT_DIRECTORY, newValue.toString());
        });
        pane.add(useDefaultDirectoryCheckbox, 1, 0);

        Label includePreambleLabel = new Label("Include Preamble");
        String preambleTooltip = "Include the preamble of the file, if it exists, in each separate tune file.";
        Tooltip.install(includePreambleLabel, new Tooltip(preambleTooltip));
        pane.add(includePreambleLabel, 0, 1);
        CheckBox includePreambleCheckBox = new CheckBox();
        Tooltip.install(includePreambleCheckBox, new Tooltip(preambleTooltip));
        includePreambleCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            options.put(INCLUDE_PREAMBLE, newValue.toString());
        });

        pane.add(includePreambleCheckBox, 1, 1);
        return pane;
    }

}
