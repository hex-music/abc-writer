package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.io.XmlFileWriter;
import java.io.File;

/**
 *
 * @author hl
 */
public class CreateMusicXmlFilesAction extends AbcWriterAction<File> {

    private final File file;

    public CreateMusicXmlFilesAction(AbcWriter app, File file) {
        super(app);
        this.file = file;
    }

    @Override
    protected File performAction() {
        return new XmlFileWriter(file).apply();
    }
}
