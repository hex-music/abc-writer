package abc.writer.app.gui;

import java.io.File;
import javafx.geometry.Bounds;

/**
 *
 * @author hl
 * @param <T>
 */
public interface Editor<T> {

    T get();

    String getLabel();

    String getKey();

    String getEditorText();

    void setEditorText(String text);

    boolean isChanged();

    State getState();

    void close();

    void setSaved();

    void setUnSaved();

    void setIgnoreChanges();

    Bounds getEditorBounds();

    void requestTextAreaFocus();

    void setAbcTabPane(AbcTabPane tabPane);

    void update(File file);

    public enum State {
        UNCHANGED, CHANGED, NEW, DELETED, CHANGE_IGNORED;
    }
}
