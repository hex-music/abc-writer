package abc.writer.app.gui.popup;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;

/**
 *
 * @author hl
 */
public class SaveOptionsDialog extends AbcDialog<AbcDialog.Result> {

    public SaveOptionsDialog() {
        this(true);
    }

    public SaveOptionsDialog(boolean withCancelOption) {
        super.setTitle("Unsaved Changes");
        super.setHeaderText(null);
        super.setContentText("You have unsaved changes.\nDo you want to ignore them and close the editor?");
        final DialogPane dialogPane = getDialogPane();
        dialogPane.setPrefSize(500, 100);
        ButtonType saveAndCloseButton = new ButtonType("Save and close", ButtonBar.ButtonData.OTHER);
        ButtonType closeButton = new ButtonType("Close", ButtonBar.ButtonData.OTHER);
        dialogPane.getButtonTypes().addAll(saveAndCloseButton, closeButton);
        if (withCancelOption) {
            ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.OTHER);
            dialogPane.getButtonTypes().add(1, cancelButton);
        }
        setResultConverter((param) -> {
            return param.equals(closeButton)
                    ? AbcDialog.Result.YES
                    : param.equals(saveAndCloseButton)
                    ? AbcDialog.Result.SAVE_AND_CLOSE
                    : AbcDialog.Result.CANCEL;
        });
    }
}
