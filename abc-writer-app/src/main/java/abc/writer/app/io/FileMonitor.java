package abc.writer.app.io;

import abc.writer.app.gui.control.FileChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

/**
 *
 * From:
 * http://www.java2s.com/Tutorial/Java/0180__File/MonitoringaFileforchanges.htm
 *
 *
 * @author hl
 */
public class FileMonitor {

    public static final Long DEFAULT_LISTENER_INTERVAL = 1_000l;

    private static final FileMonitor INSTANCE = new FileMonitor();
    private final Timer timer;
    private final Map<String, FileMonitorTask> timerEntries;

    /**
     * Gets the file monitor INSTANCE.
     *
     * @return file monitor INSTANCE
     */
    public static FileMonitor getInstance() {
        return INSTANCE;
    }

    /**
     * Constructor.
     */
    private FileMonitor() {
        timer = new Timer(true);
        timerEntries = new HashMap<>();
    }

    /**
     * Adds a monitored file with a {@link FileChangeListener}.
     *
     * @param listener listener to notify when the file changed.
     * @param fileName name of the file to monitor.
     * @param period polling period in milliseconds.
     * @throws java.io.FileNotFoundException
     */
    public void addFileChangeListener(FileChangeListener listener, String fileName, long period) throws FileNotFoundException {
        addFileChangeListener(listener, new File(fileName), period);
    }

    /**
     * Adds a monitored file with a FileChangeListener.
     *
     * @param listener listener to notify when the file changed.
     * @param file
     * @param period polling period in milliseconds.
     * @throws java.io.FileNotFoundException
     */
    public void addFileChangeListener(FileChangeListener listener, File file, long period) throws FileNotFoundException {
        removeFileChangeListener(listener, file);
        FileMonitorTask task = new FileMonitorTask(listener, file);
        timerEntries.put(file.toString() + listener.hashCode(), task);
        timer.schedule(task, period, period);
    }

    /**
     * Remove the listener from the notification list.
     *
     * @param listener the listener to be removed.
     * @param fileName
     */
    public void removeFileChangeListener(FileChangeListener listener, String fileName) {
        removeFileChangeListener(listener, new File(fileName));
    }

    /**
     * Remove the listener from the notification list.
     *
     * @param listener the listener to be removed.
     * @param file
     */
    public void removeFileChangeListener(FileChangeListener listener, File file) {
        FileMonitorTask task = timerEntries.remove(file.toString() + listener.hashCode());
        if (task != null) {
            task.cancel();
        }
    }

    /**
     * Fires notification that a file changed.
     *
     * @param listener file change listener
     * @param file the file that changed
     */
    protected void fireFileChangeEvent(FileChangeListener listener, File file) {
        listener.fileChanged(file);
    }

    /**
     * File monitoring task.
     */
    class FileMonitorTask extends TimerTask {

        FileChangeListener listener;
        File monitoredFile;
        long timestamp;

        public FileMonitorTask(FileChangeListener listener, File file) throws FileNotFoundException {
            this.listener = listener;
            this.timestamp = 0;
            monitoredFile = file;
            if (!monitoredFile.exists()) { // but is it on CLASSPATH?
                URL fileURL = listener.getClass().getClassLoader().getResource(file.toString());
                if (fileURL != null) {
                    monitoredFile = new File(fileURL.getFile());
                } else {
                    throw new FileNotFoundException("File Not Found: " + file);
                }
            }
            this.timestamp = monitoredFile.lastModified();
        }

        @Override
        public void run() {
            long lastModified = monitoredFile.lastModified();
            if (lastModified != this.timestamp) {
                this.timestamp = lastModified;
                new Timeline(new KeyFrame(Duration.millis(50), (e) -> {
                    fireFileChangeEvent(this.listener, monitoredFile);
                })).play();
            }
        }
    }
}
