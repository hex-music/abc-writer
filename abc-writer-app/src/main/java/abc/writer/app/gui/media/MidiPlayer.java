package abc.writer.app.gui.media;

import abc.writer.app.io.MidiService;
import java.io.File;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;

/**
 *
 * @author hl
 */
public class MidiPlayer {

    private static final double WIDTH = 400, HEIGHT = 400;
    private static final long DEFAULT_INTERVAL = 3l;
    private static final String ELAPSED_TIME_START = "0:00";
    private MidiService midiService;
    private final Scene scene;
    private final Stage stage;
    private final BorderPane root;
    private final Label titleLabel;

    private final HBox controlPanel;
    private final ControlButton rewindButton;
    private final ControlButton fastforwardButton;
    private final ControlButton pauseButton;
    private final ControlButton playButton;
    private final ControlButton stopButton;
    private final ControlButton repeatButton;
    private final ControlButton powerButton;

    private boolean isRepeating = false;
    private final Label durationLabel;
    private final Label elapsedTimeLabel;
    private final Slider slider;
    private final Timeline timeline;
    private DragStartState dragStartState = DragStartState.STOPPED;

    private static final MidiPlayer INSTANCE = new MidiPlayer();

    private MidiPlayer() {
        this.midiService = MidiService.get();
        this.stage = new Stage();
        this.root = new BorderPane();
        this.scene = new Scene(root, WIDTH, HEIGHT);
        this.titleLabel = new Label();
        this.controlPanel = new HBox();
        this.slider = new Slider();
        this.durationLabel = new Label();
        this.elapsedTimeLabel = new Label();
        this.timeline = new Timeline(new KeyFrame(Duration.seconds(1), (e) -> {
            updateElapsedTime(midiService.getElapsedTime().getSeconds());
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        stage.setTitle("ABC Midi Player");
        stage.setScene(scene);
        stage.getIcons().add(new Image("layout/image/MariaBarbaraBach.png"));
        stage.setX(1200);
        stage.setY(100);
        scene.getStylesheets().add("/layout/style/midi-player.css");
        controlPanel.setAlignment(Pos.CENTER);
        controlPanel.getStyleClass().add("abc-midi-control-panel");
        titleLabel.getStyleClass().add("abc-midi-title");
        titleLabel.setPrefHeight(32);
        titleLabel.setPrefWidth(WIDTH);
        titleLabel.setAlignment(Pos.CENTER);
        root.setTop(titleLabel);
        root.setCenter(new VBox(5));
        addToCenter(controlPanel);
        slider.setLabelFormatter(getConverter());
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setOnMousePressed((e) -> {
            dragStartState = isPlaying() ? DragStartState.PLAYING : DragStartState.PAUSED;
            pause(false);
        });
        slider.setOnMouseDragged((e) -> {
            updateElapsedTime(new Double(slider.getValue()).longValue());
        });
        slider.setOnMouseReleased((e) -> {
            goTo(slider.getValue());
            if (dragStartState.equals(DragStartState.PLAYING)) {
                play();
            }
        });
        VBox sliderBox = new VBox(slider);
        sliderBox.setPadding(new Insets(3, 20, 3, 20));
        addToCenter(sliderBox);
        rewindButton = controlButton(Control.REWIND);
        fastforwardButton = controlButton(Control.FASTFORWARD);
        pauseButton = controlButton(Control.PAUSE);
        playButton = controlButton(Control.PLAY);
        stopButton = controlButton(Control.STOP);
        repeatButton = controlButton(Control.REPEAT);
        powerButton = controlButton(Control.POWER);

        HBox timeBox = new HBox(elapsedTimeLabel, new Label(" / "), durationLabel);
        timeBox.setAlignment(Pos.CENTER_RIGHT);

        elapsedTimeLabel.textProperty().addListener((observable, oldValue, newValue) -> {
            slider.setValue(toSeconds(newValue));
        });
        midiService.getRunningProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                timeline.play();
            } else {
                timeline.stop();
            }
        });
        root.setBottom(timeBox);
        init();
    }

    private void addToCenter(Node node) {
        ((VBox) root.getCenter()).getChildren().add(node);
    }

    private void init() {
        controlPanel.getChildren().addAll(rewindButton, fastforwardButton, playButton, pauseButton, stopButton, repeatButton, powerButton);
        rewindButton.setOnAction(e -> rewind());
        fastforwardButton.setOnAction(e -> fastforward());
        pauseButton.setOnAction(e -> pause());
        playButton.setOnAction(e -> play());
        stopButton.setOnAction(e -> stop());
        repeatButton.setOnAction(e -> toggleRepeat());
        powerButton.setOnAction(e -> quit());

        stage.setOnCloseRequest(e -> quit());
        midiService.addListener((meta) -> {
            if (meta.getType() == MidiService.END_OF_TRACK_MESSAGE) {
                stop();
            }
        });
    }

    public static MidiPlayer instance() {
        return INSTANCE;
    }

    public void open() {
        if (midiService == null) {
            midiService = MidiService.get();
        }
        midiService.open();
    }

    public void show() {
        stage.show();
        stage.requestFocus();
    }

    public void showAndWait() {
        stage.showAndWait();
    }

    public void hide() {
        stage.hide();
    }

    public void close() {
        stage.close();
    }

    public void load(File midiFile) {
        load(midiFile, midiFile.getName());
    }

    public void load(File midiFile, String title) {
        titleLabel.setText(title);
        stop();
        midiService.load(midiFile);
        durationLabel.setText(formatDuration(midiService.getDuration().getSeconds()));
        elapsedTimeLabel.setText(ELAPSED_TIME_START);
        updateSliderDisplay(midiService.getDuration().getSeconds());
        Platform.runLater(() -> playButton.requestFocus());
    }

    private String formatDuration(long seconds) {
        if (seconds > 60 * 60) {
            return String.format("%d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 60));
        }
        return String.format("%d:%02d", (seconds % 3600) / 60, (seconds % 60));
    }

    private void goTo(Number seconds) {
        boolean running = isPlaying();
        if (running) {
            pause(false);
        }
        midiService.goTo(seconds.longValue());
        if (running) {
            play(false);
        }
    }

    public void play() {
        play(true);
    }

    public void play(boolean setPauseButtonFocus) {
        playButton.setDisable(true);
        rewindButton.setDisable(false);
        fastforwardButton.setDisable(false);
        pauseButton.setDisable(false);
        if (setPauseButtonFocus) {
            pauseButton.requestFocus();
        }
        stopButton.setDisable(false);
        midiService.play();
    }

    public void pause() {
        pause(true);
    }

    public void pause(boolean setPlayButtonFocus) {
        pauseButton.setDisable(true);
        rewindButton.setDisable(true);
        fastforwardButton.setDisable(true);
        playButton.setDisable(false);
        if (setPlayButtonFocus) {
            playButton.requestFocus();
        }
        stopButton.setDisable(false);
        midiService.pause();
    }

    public void stop() {
        dragStartState = DragStartState.STOPPED;
        stopButton.setDisable(true);
        rewindButton.setDisable(true);
        fastforwardButton.setDisable(true);
        playButton.setDisable(false);
        pauseButton.setDisable(true);
        midiService.stop();
        Platform.runLater(() -> {
            elapsedTimeLabel.setText(ELAPSED_TIME_START);
        });
        playButton.requestFocus();
    }

    public void rewind() {
        pauseButton.setDisable(false);
        playButton.setDisable(true);
        midiService.rewind(DEFAULT_INTERVAL);
    }

    public void fastforward() {
        pauseButton.setDisable(false);
        playButton.setDisable(true);
        midiService.fastforward(DEFAULT_INTERVAL);
    }

    public void toggleRepeat() {
        isRepeating = !isRepeating;
        midiService.setLoop(isRepeating);
        String imageUrl = isRepeating ? "/layout/image/icon/control/control_repeat.png" : "/layout/image/icon/control/control_repeat_blue.png";
        repeatButton.setGraphic(new ImageView(imageUrl));
    }

    public void quit() {
        stop();
        midiService.close();
        close();
    }

    public boolean isPlaying() {
        return midiService.isRunning();
    }

    private StringConverter<Double> getConverter() {
        return new StringConverter<Double>() {
            @Override
            public String toString(Double duration) {
                return formatDuration(duration.longValue());
            }

            @Override
            public Double fromString(String string) {
                return 0D;
            }
        };
    }

    private void updateElapsedTime(long elapsedTime) {
        elapsedTimeLabel.setText(formatDuration(elapsedTime));
    }

    private int toSeconds(String timeString) {
        int result = 0;
        String[] parts = timeString.split(":");
        int seconds = parts.length - 1;
        int minutes = parts.length - 2;
        int hours = parts.length - 3;
        if (hours >= 0) {
            result += Integer.valueOf(parts[hours]) * 60 * 60;
        }
        if (minutes >= 0) {
            result += Integer.valueOf(parts[minutes]) * 60;
        }
        if (seconds >= 0) {
            result += Integer.valueOf(parts[seconds]);
        }
        return result;
    }

    private void updateSliderDisplay(long max) {
        int unit = 60;
        int count = 3;
        if (max >= 60 * 15) {
            count = 1;
        } else if (max >= 60 * 6) {
            count = 3;
        } else if (max <= 60 * 2) {
            unit = 15;
            count = 2;
        }
        slider.setMin(0);
        slider.setMax(max);
        slider.setValue(0);
        slider.setMajorTickUnit(unit);
        slider.setMinorTickCount(count);
        slider.setBlockIncrement(1);
    }

    private ControlButton controlButton(Control control) {
        return ControlButton.from(control);
    }

    public enum DragStartState {
        PLAYING, PAUSED, STOPPED;
    }
}
