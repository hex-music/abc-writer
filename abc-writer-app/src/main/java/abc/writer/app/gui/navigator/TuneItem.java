package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.action.GoToTuneAction;
import abc.writer.app.action.OpenMidiPlayerAction;
import abc.writer.app.action.SplitTuneIntoVoicesAction;
import abc.writer.app.action.ViewTunesAction;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.Tune;
import java.io.File;
import java.util.stream.Collectors;
import javafx.event.Event;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 *
 * @author hl
 */
public class TuneItem extends TreeItem<Entity> implements FileBrowserItem {

    private final AbcWriter app;
    private final File parent;
    private final Tune tune;

    public TuneItem(AbcWriter app, File parent, Tune tune) {
        super(tune, getImageView());
        this.app = app;
        this.parent = parent;
        this.tune = tune;
        init();
    }

    private void init() {
        if (tune != null) {
            getChildren().addAll(tune.getVoices().stream()
                    .map(voice -> new VoiceItem(app, parent, tune, voice))
                    .collect(Collectors.toList()));
        }
    }

    @Override
    public String getText() {
        return tune.getLabel();
    }

    @Override
    public Tooltip getTooltip() {
        StringBuilder builder = new StringBuilder("ID: ").append(tune.getIndex()).append("\n");
        tune.getTitles().forEach(s -> builder.append(s).append("\n"));
        tune.getComposers().forEach(s -> builder.append(s).append("\n"));
        tune.getMeter().ifPresent(s -> builder.append("Meter: ").append(s).append("\n"));
        tune.getKey().ifPresent(s -> builder.append("Key: ").append(s).append("\n"));
        builder.append("Number of voices: ").append(tune.getNumberOfVoices());
        return new Tooltip(builder.toString().trim());
    }

    private static ImageView getImageView() {
        ImageView result = new ImageView("layout/image/icon/music.png");
        result.setFitHeight(18);
        result.setFitWidth(18);
        return result;
    }

    @Override
    public void performDefaultAction() {
        new GoToTuneAction(app, parent, tune).actionPerformed();
    }

    @Override
    public void performDeleteAction() {
    }

    @Override
    public ContextMenu getContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem gotToTuneItem = new MenuItem("Go to tune");
        gotToTuneItem.setOnAction((e) -> performDefaultAction());
        MenuItem viewTuneItem = new MenuItem("View tune");
        viewTuneItem.setOnAction((e) -> new ViewTunesAction(app, tune).actionPerformed());
        MenuItem splitVoicesItem = new MenuItem("Split into voices");
        splitVoicesItem.setOnAction(e -> new SplitTuneIntoVoicesAction(app, parent, tune).actionPerformed());
        MenuItem openAsMidi = new MenuItem("Play Tune");
        openAsMidi.setOnAction(this::playMidi);

        menu.getItems().addAll(gotToTuneItem, viewTuneItem, openAsMidi, splitVoicesItem);
        return menu;
    }

    private void playMidi(Event event) {
        new OpenMidiPlayerAction(app, tune.getAsString(), tune.getLabel()).actionPerformed();
    }
}
