package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.help.HelpWindow;
import javafx.application.Platform;

/**
 *
 * @author hl
 */
public class ShowHelpDialogAction extends AbcWriterAction<Void> {

    public ShowHelpDialogAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        Platform.runLater(() -> {
            new HelpWindow().show();
        });
        return null;
    }

}
