package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.entity.FileInfo;
import abc.writer.app.gui.AbcEditor;
import javafx.geometry.Bounds;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;

/**
 *
 * @author hl
 */
public class ShowFileInfoAction extends AbcWriterAction<Void> {

    private final FileInfo fileInfo;
    private final Bounds bounds;
    private static final double PREFERRED_WIDTH = 365;

    public ShowFileInfoAction(AbcWriter app) {
        this(app, getFileInfo(app), app.getCurrentEditor() == null ? null : app.getCurrentEditor().getEditorBounds());
    }

    public ShowFileInfoAction(AbcWriter app, FileInfo fileInfo, Bounds bounds) {
        super(app);
        this.fileInfo = fileInfo;
        this.bounds = bounds;
    }

    @Override
    protected Void performAction() {
        if (fileInfo == null) {
            return null;
        }
        Popup popup = getPopup();
        popup.show(app.getStage());
        return null;
    }

    private Popup getPopup() {
        Popup popup = new Popup();
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label headerLabel = new Label(getHeaderString());
        headerLabel.getStyleClass().add("abc-popup-header");
        VBox box = new VBox(headerLabel);
        box.setPrefWidth(PREFERRED_WIDTH);
        box.getStyleClass().add("abc-popup-list");
        if (fileInfo != null) {
            fileInfo.getContent().stream()
                    .map(tune -> tune.getLabel())
                    .map(title -> createTitleLabel(title, box))
                    .forEach(box.getChildren()::add);
        }
        popup.getContent().add(box);
        popup.setX(bounds.getMaxX() - PREFERRED_WIDTH);
        popup.setY(bounds.getMinY() + 2);
        return popup;
    }

    private String getHeaderString() {
        if (fileInfo == null) {
            return "File contains no tunes.";
        }
        String startString = "File contains " + fileInfo.getNumberOfTunes() + " ";
        String tunesString = fileInfo.getNumberOfTunes() == 1 ? "tune" : "tunes";
        String preambleString = fileInfo.getPreamble().isPresent() ? " and a preamble." : ".";
        String headerString = startString + tunesString + preambleString;
        return headerString;
    }

    private Label createTitleLabel(String title, VBox box) {
        Label label = new Label(title);
        label.setPrefWidth(PREFERRED_WIDTH);
        label.getStyleClass().add(box.getChildren().size() % 2 == 0 ? "abc-even-label" : "abc-odd-label");
        return label;
    }

    private static FileInfo getFileInfo(AbcWriter app) {
        return app.getCurrentEditor() == null || !(app.getCurrentEditor() instanceof AbcEditor) ? null : ((AbcEditor) app.getCurrentEditor()).getFileInfo();
    }
}
