package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.io.FileMonitor;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.TextEditor;
import abc.writer.app.io.FileUtil;
import abc.writer.app.system.WriterLogger;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;

/**
 *
 * @author hl
 */
public class SaveFileAsAction extends AbcWriterAction<Optional<File>> {

    public SaveFileAsAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Optional<File> performAction() {
        if (app.getCurrentEditor() == null) {
            return Optional.empty();
        }
        File result = fileChooser().showSaveDialog(app.getStage());
        if (result != null) {
            result = assertCorrectSuffix(result, getFileType());
            File originalFile = getOriginalFile();
            FileMonitor.getInstance().removeFileChangeListener(app.getCurrentEditor()::update, originalFile);
            String text = app.getCurrentEditor().getEditorText();
            String path = result.getAbsolutePath();
            FileUtil.write(text.getBytes(StandardCharsets.UTF_8), path);
            app.getCurrentEditor().update(result);
            app.getCurrentEditor().setSaved();
            try {
                FileMonitor.getInstance().addFileChangeListener(app.getCurrentEditor()::update, result, FileMonitor.DEFAULT_LISTENER_INTERVAL);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SaveFileAsAction.class.getName()).log(Level.WARNING, "Could not register new file listener for " + result.getAbsolutePath(), ex);
            }
            setLeftStatus("Saved as " + result.getAbsolutePath(), 4);
            WriterLogger.setLog(WriterLogger.LAST_SAVED_IN_DIR, result.getParent());
            return Optional.of(result);
        }
        return Optional.empty();
    }

    private FileType getFileType() {
        if (isAbcEditor()) {
            return FileType.ABC;
        }
        return null;
    }

    private File getOriginalFile() {
        if (isAbcEditor()) {
            return ((AbcEditor) app.getCurrentEditor()).get().file();
        }
        if (isTextEditor()) {
            return ((TextEditor) app.getCurrentEditor()).get();
        }
        throw new AbcWriterException("Editor type is not recognised: " + app.getCurrentEditor().getClass().getName());
    }

    private File assertCorrectSuffix(File file, FileType fileType) {
        if (fileType == null || file.getName().endsWith(fileType.suffix())) {
            return file;
        } else {
            return new File(file.getAbsolutePath() + fileType.suffix());
        }
    }

    private FileChooser fileChooser() {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(getExtensionFilters());
        if (isAbcEditor()) {
            chooser.setSelectedExtensionFilter(FileType.ABC.getExtensionFilter());
        }
        File initialDirectory = WriterLogger.getLog(WriterLogger.LAST_SAVED_IN_DIR)
                .map(i -> i.getValue())
                .map(File::new)
                .orElse(getDefaultInitialDirectory());
        initialDirectory.mkdirs();
        chooser.setInitialDirectory(initialDirectory);
        chooser.setInitialFileName(app.getCurrentEditor().getLabel());
        chooser.setTitle("Select new name for file");
        return chooser;
    }

    private File getDefaultInitialDirectory() {
        if (isAbcEditor()) {
            return Constants.PUBLIC_ABC_DIRECTORY;
        }
        return new File(System.getProperty("user.home"));
    }

    private boolean isTextEditor() {
        return app.getCurrentEditor() instanceof TextEditor;
    }

    private boolean isAbcEditor() {
        return app.getCurrentEditor() instanceof AbcEditor;
    }
}
