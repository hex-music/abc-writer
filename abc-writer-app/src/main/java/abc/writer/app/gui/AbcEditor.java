package abc.writer.app.gui;

import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.io.FileMonitor;
import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.AbcWriterException;
import abc.writer.app.FileType;
import abc.writer.app.action.CloseWhileUnsavedChangesAction;
import abc.writer.app.action.CreateFileFromTemplateAction;
import abc.writer.app.action.CreatePdfAction;
import abc.writer.app.action.CreatePostScriptAction;
import abc.writer.app.action.FindAction;
import abc.writer.app.action.OpenMidiPlayerAction;
import abc.writer.app.action.SaveEditorContentAction;
import abc.writer.app.action.ViewTunesAction;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.FileInfo;
import abc.writer.app.entity.Tune;
import abc.writer.app.gui.AbcEditor.AbcFile;
import abc.writer.app.gui.control.CloseListener;
import abc.writer.app.gui.control.EditorListener;
import abc.writer.app.gui.control.AbcNotesKeyEventHandler;
import abc.writer.app.gui.control.TextInputControlUtil;
import abc.writer.app.gui.control.TextListener;
import abc.writer.app.gui.popup.ChordPopup;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.stream.Stream;
import javafx.geometry.Bounds;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import java.util.ConcurrentModificationException;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

/**
 *
 * @author hl
 */
public class AbcEditor extends Tab implements Editor<AbcFile> {

    public static final int LINE_HEIGHT = 20;
    private final BorderPane borderPane;
    private final AbcWriter app;
    private final List<CloseListener> closeListeners = new ArrayList<>();
    private final List<EditorListener> editorListeners = new ArrayList<>();
    private final List<TextListener> editorLabelListener = new ArrayList<>();
    private File file;
    private String abcText = "";
    private String originalAbcText = "";
    private State state;
    private final TextArea abcArea;
    private String key;
    private AbcTabPane abcTabPane;
    private final StatusBar statusBar = StatusBar.instance();
    private final TextInputControlUtil controlUtil;
    private final AbcNotesKeyEventHandler keyEventHandler;

    private AbcEditor(AbcWriter app, File file, AbcTabPane abcTabPane) {
        this.app = app;
        updateState(State.UNCHANGED);
        this.abcTabPane = abcTabPane;
        this.abcArea = new TextArea();
        this.controlUtil = new TextInputControlUtil(abcArea);
        this.keyEventHandler = new AbcNotesKeyEventHandler(app, this, abcArea);
        this.borderPane = new BorderPane();
        super.setContent(borderPane);
        update(file);
        if (file != null) {
            try {
                FileMonitor.getInstance().addFileChangeListener(this::update, file, FileMonitor.DEFAULT_LISTENER_INTERVAL);
            } catch (FileNotFoundException e) {
                Logger.getLogger(AbcEditor.class.getName()).log(Level.WARNING, "Could not register listener for file " + file, e);
            }
        }
        init();
    }

    public AbcEditor(AbcWriter app, AbcTabPane abcTabPane) {
        this(app, null, abcTabPane);
        updateState(State.NEW);
    }

    public AbcEditor(AbcWriter app, File file) {
        this(app, file, null);
        this.state = State.UNCHANGED;
    }

    @Override
    public void requestTextAreaFocus() {
        abcArea.requestFocus();
    }

    @Override
    public void setEditorText(String text) {
        abcArea.setText(text);
        // TODO: This might be wrong, check.
        // this.state = State.NEW;
        updateState(State.NEW);
    }

    public void setBottom(Node node) {
        borderPane.setBottom(node);
    }

    public void clearBottom() {
        borderPane.setBottom(null);
    }

    public SearchPanel openSearchPanel() {
        if (!hasSearchPanel()) {
            setBottom(new SearchPanel(this));
        }
        return (SearchPanel) borderPane.getBottom();
    }

    private boolean hasSearchPanel() {
        return borderPane.getBottom() != null && borderPane.getBottom() instanceof SearchPanel;
    }

    @Override
    public final void update(File input) {
        if (input != null) {
            file = input;
            if (!file.exists()) {
                updateState(State.DELETED);
                updateEditorStyle();
                FileMonitor.getInstance().removeFileChangeListener(this::update, file);
                return;
            }
            key = file.getAbsolutePath();
            super.setText(file.getName());
            super.setTooltip(new Tooltip(key));
            try {
                int pos = abcArea.getCaretPosition();
                double scrollTop = abcArea.getScrollTop();
                abcText = FileUtil.inputStreamToString(new FileInputStream(file));
                updateOriginalTextToCurrent();
                abcArea.setText(abcText);
                abcArea.positionCaret(pos);
                abcArea.setScrollTop(scrollTop);
            } catch (FileNotFoundException ex) {
                throw new AbcWriterException("Could not read file", ex);
            }
        } else {
            setText(getNewTabText());
            setTooltip(null);
            this.file = new CreateFileFromTemplateAction(app, Constants.WORKING_DIRECTORY_ABC_TEMP, FileType.ABC, getLabel()).actionPerformed();
            this.file.deleteOnExit();
            this.key = this.file.getAbsolutePath();
            try {
                abcText = FileUtil.inputStreamToString(new FileInputStream(file));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(AbcEditor.class.getName()).log(Level.SEVERE, null, ex);
                abcText = "";
            }
            abcArea.setText(abcText);
            updateOriginalTextToCurrent();
//            state = State.NEW;
            // TODO: Check if this works.
            updateState(State.NEW);
        }
    }

    @Override
    public String getLabel() {
        return super.getText();
    }

    public void addCloseListener(CloseListener listener) {
        this.closeListeners.add(listener);
    }

    public void addEditorListener(EditorListener listener) {
        this.editorListeners.add(listener);
    }

    public void addLabelListener(TextListener listener) {
        this.editorLabelListener.add(listener);
    }

    public void goToTune(Tune tune) {
        abcArea.requestFocus();
        String pre = abcArea.getText().split(tune.getIndexString())[0];
        int pos = 0;
        if (pre.length() > 0) {
            pos = controlUtil.getLineStartPosition(pre.length());
        }
        abcArea.positionCaret(pos);
        Integer lineIndex = controlUtil.getCurrentLineIndex();
        abcArea.setScrollTop(lineIndex * LINE_HEIGHT);
    }

    private String getNewTabText() {
        Integer nextTabNumber;
        Integer lastFileNumber = Stream.of(Constants.PUBLIC_ABC_DIRECTORY.listFiles())
                .filter(this::filterNewFiles)
                .map(this::createInt)
                .max((a, b) -> a.compareTo(b)).orElse(0);
        Integer lastTabNumber = getAbcTabPane().getTabs().stream()
                .filter(t -> filterOpenTabs(t))
                .map(this::createInt)
                .max((a, b) -> a.compareTo(b)).orElse(0);
        nextTabNumber = lastFileNumber.equals(lastTabNumber) || lastFileNumber > lastTabNumber
                ? lastFileNumber + 1
                : lastTabNumber + 1;
        return "New File " + nextTabNumber + FileType.ABC.suffix();
    }

    private Integer createInt(File file) {
        return createInt(file.getName());
    }

    private Integer createInt(Tab tab) {
        return createInt(tab.getText());
    }

    private Integer createInt(String string) {
        return Integer.valueOf(string.replaceAll("\\D", "").trim());
    }

    private boolean filterOpenTabs(Tab tab) {
        return tab != null && tab.getText().startsWith("New File") && !tab.getText().replaceAll("\\D", "").isEmpty();
    }

    private boolean filterNewFiles(File f) {
        boolean isNewFile = f.getName().startsWith("New File")
                && f.getName().endsWith(FileType.ABC.suffix())
                && !f.getName().replaceAll("\\D", "").isEmpty();
        return isNewFile;
    }

    @Override
    public Bounds getEditorBounds() {
        return abcArea.localToScreen(abcArea.getBoundsInLocal());
    }

    private AbcTabPane getAbcTabPane() {
        return abcTabPane;
    }

    @Override
    public void setAbcTabPane(AbcTabPane pane) {
        this.abcTabPane = pane;
    }

    TextArea getAbcArea() {
        return abcArea;
    }

    String getCurrentCaretPosition() {
        int positionOnLIne = controlUtil.getPositionOnCurrentLine() + 1;
        return controlUtil.getCurrentLineIndex() + ":" + positionOnLIne;
    }

    public Tune getCurrentTune() {
        int pos = controlUtil.getCurrentLineEndPosition();
        if (!abcArea.getText().substring(0, pos).contains(Constants.INDEX)) {
            return null;
        }
        int start = abcArea.getText().substring(0, pos).lastIndexOf(Constants.INDEX);
        Matcher tuneTitleMatcher = TextInputControlUtil.TUNE_TITLE_FIELD_MATCH.matcher(abcArea.getText().substring(start));
        if (tuneTitleMatcher.find()) {
            if (abcArea.getText().substring(start).contains("\n" + Constants.INDEX)) {
                int end = abcArea.getText().substring(start).indexOf("\n" + Constants.INDEX) + start;
                return Tune.from(abcArea.getText().substring(start, end));
            } else {
                return Tune.from(abcArea.getText().substring(start));
            }
        }
        return null;
    }

    private void showCaretPosition(Number number) {
        statusBar.rightMessage(getCurrentCaretPosition());
    }

    void showCurrentTune() {
        if (getCurrentTune() == null) {
            statusBar.centerMessage("");
        } else {
            statusBar.centerMessage(getCurrentTune().getLabel());
        }
    }

    private void init() {
        createTabMenu();
        createAreaMenu();
        abcArea.caretPositionProperty().addListener((observable, oldValue, newValue) -> {
            showCaretPosition(newValue);
            showCurrentTune();
        });
        abcArea.setWrapText(false);
        abcArea.getStyleClass().add("abc-text-area");
        abcArea.setText(abcText);
        borderPane.setCenter(abcArea);
        abcArea.addEventHandler(KeyEvent.KEY_PRESSED, keyEventHandler);
        abcArea.textProperty().addListener((observable, oldValue, newValue) -> updateText(newValue));
        textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                editorLabelListener.forEach(listener -> listener.update(newValue));
            } catch (ConcurrentModificationException e) {
//                Logger.getLogger(AbcEditor.class.getName()).log(Level.WARNING, "Try to fix this?",  e);
            }
        });
        setOnCloseRequest((e) -> {
            if (isChanged()) {
                AbcDialog.Result result = new CloseWhileUnsavedChangesAction(app).actionPerformed();
                switch (result) {
                    case CANCEL:
                        e.consume();
                        break;
                    case SAVE_AND_CLOSE:
                        new SaveEditorContentAction(app, this).actionPerformed();
                        break;
                    default:
                        state = State.CHANGE_IGNORED;
                        break;
                }
            }
            closeListeners.forEach(CloseListener::closed);
        });
    }

    public FileInfo getFileInfo() {
        String label = file == null ? getText() : file.getAbsolutePath();
        return Entity.Factory.fileInfo(label, abcArea.getText());
    }

    private void updateText(String text) {
        this.abcText = text;
        updateState(originalAbcText.equals(abcText) ? State.UNCHANGED : State.CHANGED);
    }

    private void updateOriginalTextToCurrent() {
        if (!originalAbcText.equals(abcText)) {
            originalAbcText = abcText;
        }
    }

    private void updateEditorStyle() {
        if (state.equals(State.DELETED)) {
            abcArea.getStyleClass().addAll("deleted-content");
        } else {
            abcArea.getStyleClass().removeAll("deleted-content");
        }
    }

    private void updateState(State newState) {
        if (state != null && state.equals(State.NEW) && !newState.equals(State.CHANGE_IGNORED)) {
            return;
        }
        state = newState;
        if (state.equals(State.CHANGED)) {
            getStyleClass().add("changed-tab");
        } else {
            getStyleClass().removeAll("changed-tab");
        }
        if (state.equals(State.NEW)) {
            getStyleClass().add("new-tab");
        } else {
            getStyleClass().removeAll("new-tab");
        }
        if (state.equals(State.DELETED)) {
            getStyleClass().add("deleted-tab");
            setTooltip(new Tooltip("This file has been externally deleted\nYou can restore it by saving it."));
        } else {
            if (file == null) {
                setTooltip(null);
            } else {
                setTooltip(new Tooltip(key));
            }
            getStyleClass().removeAll("deleted-tab");
        }
        editorListeners.forEach(listener -> {
            listener.stateChanged(this.state);
        });
    }

    @Override
    public boolean isChanged() {
        return state != null && (state.equals(State.DELETED) || state.equals(State.CHANGED) || state.equals(State.NEW));
    }

    @Override
    public State getState() {
        return state;
    }

    public void revertToLastSaved() {
        abcText = originalAbcText;
        abcArea.setText(abcText);
        updateState(State.UNCHANGED);
        updateEditorStyle();
    }

    @Override
    public void setSaved() {
        updateOriginalTextToCurrent();
        updateState(State.UNCHANGED);
        updateEditorStyle();
    }

    @Override
    public void setUnSaved() {
        updateOriginalTextToCurrent();
        updateState(State.NEW);
        updateEditorStyle();
    }

    @Override
    public void setIgnoreChanges() {
        updateState(State.CHANGE_IGNORED);
    }

    public void markAsNew() {
        updateState(State.NEW);
    }

    @Override
    public AbcFile get() {
        return AbcFile.of(file, abcText);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getEditorText() {
        return abcArea.getText();
    }

    @Override
    public void close() {
        abcTabPane.closeEditor(this);
    }

    private void createTabMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem closeItem = new MenuItem("Close");
        closeItem.setOnAction((e) -> close());
        MenuItem closeAllItem = new MenuItem("Close all");
        closeAllItem.setOnAction((e) -> abcTabPane.closeAll());
        MenuItem closeOtherItem = new MenuItem("Close other");
        closeOtherItem.setOnAction((e) -> abcTabPane.closeOther(this));

        MenuItem exportAsPdfItem = new MenuItem("Export as PDF");
        exportAsPdfItem.setOnAction(e -> new CreatePdfAction(app, this).actionPerformed());
        MenuItem exportAsPsItem = new MenuItem("Export as PostScript");
        exportAsPsItem.setOnAction(e -> new CreatePostScriptAction(app, this).actionPerformed());

        menu.getItems().addAll(closeItem, closeAllItem, closeOtherItem, new SeparatorMenuItem(), exportAsPdfItem, exportAsPsItem);
        setContextMenu(menu);
    }

    private void createAreaMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem showFocusedTuneItem = new MenuItem("Show focused tune");
        showFocusedTuneItem.setAccelerator(new KeyCodeCombination(KeyCode.B, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        showFocusedTuneItem.setOnAction(e -> new ViewTunesAction(app, getCurrentTune()).actionPerformed());

        MenuItem showAllTunesItem = new MenuItem("Show all tunes");
        showAllTunesItem.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        showAllTunesItem.setOnAction(e -> new ViewTunesAction(app, getEditorContentAsFile()).actionPerformed());

        MenuItem playFocusedTuneItem = new MenuItem("Play focused tune");
        playFocusedTuneItem.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        playFocusedTuneItem.setOnAction(e -> {
            if (getCurrentTune() != null) {
                new OpenMidiPlayerAction(app, getCurrentTune()).actionPerformed();
            }
        });

        Menu insertMenu = new Menu("Insert");
        MenuItem keyChangeItem = new MenuItem("Key Change");
        keyChangeItem.setAccelerator(new KeyCodeCombination(KeyCode.K, KeyCombination.ALT_DOWN));
        keyChangeItem.setOnAction(e -> keyEventHandler.insertCode("K"));
        MenuItem meterChangeItem = new MenuItem("Meter Change");
        meterChangeItem.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.ALT_DOWN));
        meterChangeItem.setOnAction(e -> keyEventHandler.insertCode("M"));
        MenuItem voiceChangeItem = new MenuItem("Voice Change");
        voiceChangeItem.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.ALT_DOWN));
        voiceChangeItem.setOnAction(e -> keyEventHandler.insertCode("V"));
        insertMenu.getItems().addAll(keyChangeItem, meterChangeItem, voiceChangeItem);

        MenuItem searchItem = new MenuItem("Search");
        searchItem.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN));
        searchItem.setOnAction(e -> new FindAction(app, this).actionPerformed());

        MenuItem searchAndReplaceItem = new MenuItem("Search and replace");
        searchAndReplaceItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN));
        searchAndReplaceItem.setOnAction(e -> new FindAction(app, this, true).actionPerformed());

        menu.getItems().addAll(showFocusedTuneItem, showAllTunesItem, playFocusedTuneItem, new SeparatorMenuItem(),
                searchItem, searchAndReplaceItem, insertMenu
        );
        abcArea.setContextMenu(menu);
    }

    private File getEditorContentAsFile() {
        String name = getLabel().replaceAll(" ", "_");
        File tempFile = new File(Constants.WORKING_DIRECTORY_ABC_TEMP + "/" + name);
        tempFile.getParentFile().mkdirs();
        tempFile.deleteOnExit();
        FileUtil.write(abcArea.getText().getBytes(StandardCharsets.UTF_8), tempFile.getAbsolutePath());
        return tempFile;
    }

    public static class AbcFile {

        private final File file;
        private final String text;

        private AbcFile(File file, String text) {
            this.file = file;
            this.text = text;
        }

        public File file() {
            return file;
        }

        public String text() {
            return text;
        }

        public static AbcFile of(File file, String text) {
            return new AbcFile(file, text);
        }

    }
}
