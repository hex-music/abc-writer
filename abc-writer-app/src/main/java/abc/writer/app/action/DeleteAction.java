package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author hl
 */
public class DeleteAction extends AbcWriterAction<Void> {

    private final File file;

    public DeleteAction(AbcWriter app, File file) {
        super(app);
        this.file = file;
    }

    @Override
    protected Void performAction() {
        if (file.isDirectory() && file.listFiles().length > 0) {
            new Alert(Alert.AlertType.WARNING, "Folder is not empty", ButtonType.OK).showAndWait();
            return null;
        }
        StringBuilder message = new StringBuilder("Do you want to delete the ");
        if (file.isDirectory()) {
            message.append("folder: \n");
        } else {
            message.append("file: \n");
        }
        message.append(file.getName()).append("?");
        new Alert(Alert.AlertType.CONFIRMATION, message.toString(), ButtonType.CANCEL, ButtonType.OK)
                .showAndWait()
                .filter(t -> t.equals(ButtonType.OK))
                .ifPresent(t -> {
                    file.delete();
                });
        return null;
    }
}
