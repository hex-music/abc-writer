package abc.writer.app.gui.popup;

import abc.writer.app.io.FileUtil;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;

/**
 *
 * @author hl
 */
public class PostscriptFontsCheatSheet extends TableView {

    private final TableColumn fontNameColumn;
    private final TableColumn fontSampleColumn;
    private final String fontSource;
    private static final String SAMPLE_TEXT = "Flygande bäckasiner söka hwila på mjuka tuvor";

    public PostscriptFontsCheatSheet() {
        fontNameColumn = new TableColumn("Font Name");
        fontSampleColumn = new TableColumn("Font Sample");
        fontSource = getSource();
        super.getColumns().addAll(fontNameColumn, fontSampleColumn);
        init();
    }

    private void init() {
        fontNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("fontName"));
        fontSampleColumn.setCellValueFactory(new PropertyValueFactory<String, String>("fontSample"));
        fontSampleColumn.setCellFactory(col -> new SampleCell());
        List<PostscriptFont> fonts = Stream.of(fontSource.split("\n"))
                .map(PostscriptFont::new)
                .sorted()
                .collect(Collectors.toList());
        super.setItems(FXCollections.observableList(fonts));
    }

    public static class SampleCell extends TableCell<String, String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty || item == null) {
                setText(null);
                setGraphic(null);
            } else {
                setText(null);
                setGraphic(new ImageView("/layout/image/font/" + item));
            }

        }

    }

    public static class PostscriptFont implements Comparable<PostscriptFont> {

        private final SimpleStringProperty fontName;
        private final SimpleStringProperty fontSample;

        public PostscriptFont(String label) {
            this.fontName = new SimpleStringProperty(label);
            this.fontSample = new SimpleStringProperty(label + ".png");
        }

        public String getFontName() {
            return fontName.get();
        }

        public String getFontSample() {
            return fontSample.get();
        }

        @Override
        public int compareTo(PostscriptFont o) {
            return this.getFontName().compareTo(o.getFontName());
        }
    }

    private static String getSource() {
        return FileUtil.inputStreamToString(PostscriptFontsCheatSheet.class.getResourceAsStream("/text/postscript-fonts.txt"));
    }
}
