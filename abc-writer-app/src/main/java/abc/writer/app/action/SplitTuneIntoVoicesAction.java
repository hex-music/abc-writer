package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.FileType;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.Tune;
import abc.writer.app.entity.Voice;
import abc.writer.app.entity.VoiceFactory;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author hl
 */
public class SplitTuneIntoVoicesAction extends AbcWriterAction<List<File>> {

    private final File file;
    private final Tune tune;

    public SplitTuneIntoVoicesAction(AbcWriter app, File file, Tune tune) {
        super(app);
        this.file = file;
        this.tune = tune;
    }

    @Override
    protected List<File> performAction() {
        File resultFolder = createResultFolder();
        String fileBaseName = createFileBaseName(resultFolder);
        String contentStart = getContentPreamble();
        return VoiceFactory.from(tune).stream()
                .map(voice -> createVoiceFile(voice, contentStart, fileBaseName))
                .collect(Collectors.toList());
    }

    private File createVoiceFile(Voice voice, String preamble, String fileBaseName) {
        String voiceFileName = voice.getLabel() + FileType.ABC.suffix();
        File voiceFile = new File(fileBaseName + voiceFileName);
        if (voiceFile.exists()) {
            FileUtil.getIncrementallyNamedFile(voiceFile);
        }
        String content = preamble + voice.getAsAbcString();
        FileUtil.write(content.getBytes(StandardCharsets.UTF_8), voiceFile.getAbsolutePath());
        return voiceFile;
    }

    private String getContentPreamble() {
        try {
            return Entity.Factory
                    .fileInfo(file.getAbsolutePath(), FileUtil.inputStreamToString(new FileInputStream(file)))
                    .getPreamble()
                    .map(pa -> pa.getContent())
                    .orElse("");
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException(ex);
        }
    }

    private String createFileBaseName(File resultFolder) {
        return resultFolder + "/" + tune.getLabel() + " - ";
    }

    private File createResultFolder() {
        String folderName = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf("."));
        File resultFolder = new File(folderName);
        if (resultFolder.exists()) {
            resultFolder = FileUtil.getIncrementallyNamedFile(resultFolder);
        }
        resultFolder.mkdirs();
        return resultFolder;
    }

}
