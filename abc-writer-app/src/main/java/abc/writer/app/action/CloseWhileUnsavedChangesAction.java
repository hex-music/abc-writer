package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.gui.popup.SaveOptionsDialog;

/**
 * é
 *
 * @author hl
 */
public class CloseWhileUnsavedChangesAction extends AbcWriterAction<AbcDialog.Result> {

    private final boolean withCancelOption;

    public CloseWhileUnsavedChangesAction(AbcWriter app) {
        this(app, true);
    }

    public CloseWhileUnsavedChangesAction(AbcWriter app, boolean withCancelOption) {
        super(app);
        this.withCancelOption = withCancelOption;
    }

    @Override
    protected AbcDialog.Result performAction() {
        AbcDialog.Result result = new SaveOptionsDialog(withCancelOption).showAndWait().orElse(AbcDialog.Result.CANCEL);
        return result;
    }
}
