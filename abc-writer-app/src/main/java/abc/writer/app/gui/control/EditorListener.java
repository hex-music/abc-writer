package abc.writer.app.gui.control;

import abc.writer.app.gui.AbcEditor;

/**
 *
 * @author hl
 */
@FunctionalInterface
public interface EditorListener {

    void stateChanged(AbcEditor.State newState);
}
