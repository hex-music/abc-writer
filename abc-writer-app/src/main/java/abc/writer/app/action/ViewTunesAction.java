package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.Tune;
import abc.writer.app.io.FileUtil;
import abc.writer.app.io.PsFileWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author hl
 */
public class ViewTunesAction extends AbcWriterAction<Void> {

    private final List<Tune> tunes;

    public ViewTunesAction(AbcWriter app, File file) {
        this(app, extractTunesFromFile(file));
    }

    private static List<Tune> extractTunesFromFile(File file) {
        try {
            return Entity.Factory.tuneList(FileUtil.inputStreamToString(new FileInputStream(file)));
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException(ex);
        }
    }

    public ViewTunesAction(AbcWriter app, Tune tune) {
        this(app, Collections.singletonList(tune));
    }

    public ViewTunesAction(AbcWriter app, List<Tune> tunes) {
        super(app);
        this.tunes = tunes;
    }

    @Override
    protected Void performAction() {
        if (tunes.isEmpty()) {
            return null;
        }
        String abcFilePath = Constants.WORKING_DIRECTORY_ABC_TEMP + "/tunes-" + LocalDate.now() + ".abc";
        File tempFile = new File(abcFilePath.replaceAll(" ", "_"));
        tempFile.getParentFile().mkdirs();
        tempFile.deleteOnExit();
        String collection = tunes.stream().filter(tune -> tune != null).map(tune -> tune.getAsString()).collect(Collectors.joining("\n\n"));
        if (collection.isEmpty()) {
            return null;
        }
        FileUtil.write(collection.getBytes(StandardCharsets.UTF_8), tempFile.getAbsolutePath());
        new Thread(() -> {
            try {
                Desktop.getDesktop().open(new PsFileWriter(tempFile).apply());
            } catch (IOException e) {

            }
        }).start();
        return null;
    }
//
//    private File createResultFile(String abcPart) {
//        String psPart = abcPart.substring(0, abcPart.lastIndexOf(".")) + ".ps";
//        String psFilePath = Constants.PUBLIC_PS_DIRECTORY + psPart;
//        File result = new File(psFilePath);
//        result.getParentFile().mkdirs();
//        return result;
//    }
}
