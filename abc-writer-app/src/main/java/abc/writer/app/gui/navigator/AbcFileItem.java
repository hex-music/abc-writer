package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.action.CreateMusicXmlFilesAction;
import abc.writer.app.action.OpenAction;
import abc.writer.app.action.ViewTunesAction;
import abc.writer.app.entity.Entity;
import abc.writer.app.entity.FileEntity;
import abc.writer.app.entity.FileInfo;
import abc.writer.app.entity.Tune;
import abc.writer.app.io.FileUtil;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;

/**
 *
 * @author hl
 */
public class AbcFileItem extends FileItem {

    private final FileInfo info;

    public AbcFileItem(AbcWriter app, FileEntity fileEntity) {
        super(app, fileEntity);
        try {
            info = Entity.Factory.fileInfo(fileEntity.getLabel(), FileUtil.inputStreamToString(new FileInputStream(fileEntity.getContent())));
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException(ex);
        }
        init();
    }

    @Override
    public void performDefaultAction() {
        new OpenAction(app, file).actionPerformed();
    }

    @Override
    public Tooltip getTooltip() {
        StringBuilder builder = new StringBuilder(file.getName()).append("\n\n");
        if (info == null) {
            builder.append("Empty ").append(fileType.getLabel()).append(" file\n");
        } else {
            builder.append(fileType.getLabel()).append("\n");
            builder.append("Number of tunes: ").append(info.getNumberOfTunes()).append("\n\n");
            info.getContent().forEach(t -> builder.append("\t").append(t.getLabel()).append("\n"));
        }
        builder.append("\nDirectory: ").append(file.getParent()).append("/\n");
        builder.append("Mediatype: ").append(fileType.getMediatype()).append("\n");
        builder.append("Size: ").append(FileUtil.getFileSize(file));
        return new Tooltip(builder.toString());
    }

    @Override
    public ContextMenu getContextMenu() {
        ContextMenu menu = super.getContextMenu();
        MenuItem viewContentItem = new MenuItem("View tunes");
        viewContentItem.setOnAction((e) -> new ViewTunesAction(app, file).actionPerformed());
        MenuItem exportAsXmlItem = new MenuItem("Export tunes as MusicXML");
        exportAsXmlItem.setOnAction(e -> new CreateMusicXmlFilesAction(app, file).actionPerformed());

        menu.getItems().addAll(new SeparatorMenuItem(), viewContentItem, new SeparatorMenuItem(), exportAsXmlItem);
        return menu;
    }

    private void init() {
        try {
            List<Tune> tuneList = Entity.Factory.tuneList(FileUtil.inputStreamToString(new FileInputStream(getFile())));
            getChildren().addAll(tuneList.stream().map(this::getTuneItem).collect(Collectors.toList()));
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException(ex);
        }
    }

    private TuneItem getTuneItem(Tune tune) {
        return new TuneItem(app, file, tune);
    }

}
