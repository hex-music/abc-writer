package abc.writer.app.gui.navigator;

import javafx.event.Event;
import javafx.scene.control.TreeCell;
import javafx.scene.input.KeyCode;

/**
 *
 * @author hl
 * @param <T>
 */
public class AbcTreeCell<T> extends TreeCell<T> {

    public AbcTreeCell() {
//        init();
    }

    private void init() {
        setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                if (!isEmpty() && getItem() instanceof FileItem || getItem() instanceof TuneItem) {
                    ((FileBrowserItem) getItem()).performDefaultAction();
                    e.consume();
                }
            }
        });
        setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                if (getItem() instanceof FileItem || getItem() instanceof TuneItem) {
                    ((FileBrowserItem) getItem()).performDefaultAction();
                    e.consume();
                }
            }
        });
        if (getItem() instanceof FileBrowserItem) {
            FileBrowserItem newValue = (FileBrowserItem) getItem();
            setContextMenu(((FileBrowserItem) newValue).getContextMenu());

        }
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
            setTooltip(null);
        } else {
            if (getTreeItem() instanceof FileBrowserItem) {
                FileBrowserItem fileBrowserItem = (FileBrowserItem) getTreeItem();
                setText(fileBrowserItem.getText());
                setGraphic(fileBrowserItem.getGraphic());
                setContextMenu(fileBrowserItem.getContextMenu());
                setTooltip(fileBrowserItem.getTooltip());
                setOnMouseClicked(e -> {
                    if (e.getClickCount() == 2) {
                        performItemDefaultAction(fileBrowserItem, e);
                    }
                });
            } else {
                setText(getItem().toString());
                setGraphic(null);
                setTooltip(null);
            }
        }
    }

    private void performItemDefaultAction(FileBrowserItem fileBrowserItem, Event e) {
        if (!isEmpty() && (fileBrowserItem instanceof FileItem || fileBrowserItem instanceof TuneItem || fileBrowserItem instanceof VoiceItem)) {
            fileBrowserItem.performDefaultAction();
            e.consume();
        }
    }

}
