package abc.writer.app.help;

import abc.writer.app.io.FileUtil;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author hl
 */
public class HelpBrowser extends Region {

    final WebView browser = new WebView();
    final WebEngine webEngine = browser.getEngine();
    private final HelpPageMap pages = HelpPageMap.instance();

    public HelpBrowser() {
        getStyleClass().add("abc-help-browser");
        browser.setPrefSize(preferredWidth(), preferredHeight());
        super.getChildren().add(browser);
        openUrl(HelpPageMap.START);
    }

    public final void openUrl(String key) {
        webEngine.load(pages.getUrl(key));
    }

    private double preferredHeight() {
        return HelpWindow.HEIGHT - 40;
    }

    private double preferredWidth() {
        return HelpWindow.WIDTH - 200;
    }
}
