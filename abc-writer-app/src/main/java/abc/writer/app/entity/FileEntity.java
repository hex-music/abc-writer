package abc.writer.app.entity;

import java.io.File;
import java.util.Optional;

/**
 *
 * @author hl
 */
public class FileEntity implements Entity<File> {

    private final File file;

    private FileEntity(File file) {
        this.file = file;
    }

    public static FileEntity from(File file) {
        return new FileEntity(file);
    }

    public boolean isDirectory() {
        return file.isDirectory();
    }

    public Optional<String> suffix() {
        if (file.isDirectory()) {
            return Optional.empty();
        }
        if (file.getName().contains(".")) {
            return Optional.of(file.getName().substring(file.getName().lastIndexOf(".") + 1));
        }
        return Optional.empty();
    }

    @Override
    public String toString() {
        return getLabel();
    }

    @Override
    public String getLabel() {
        return file.getName();
    }

    @Override
    public File getContent() {
        return file;
    }

}
