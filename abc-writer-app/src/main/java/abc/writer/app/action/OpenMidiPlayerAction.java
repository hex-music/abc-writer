package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.entity.Tune;
import abc.writer.app.gui.media.MidiPlayer;
import abc.writer.app.io.MidiFileWriter;
import java.io.File;

/**
 *
 * @author hl
 */
public class OpenMidiPlayerAction extends AbcWriterAction<Void> {

    private static final MidiPlayer PLAYER = MidiPlayer.instance();
    private final String data;
    private final String title;

    public OpenMidiPlayerAction(AbcWriter app, Tune tune) {
        this(app, tune.getAsString(), tune.getLabel());
    }

    public OpenMidiPlayerAction(AbcWriter app, String data, String title) {
        super(app);
        this.data = data;
        this.title = title;
    }

    public OpenMidiPlayerAction(AbcWriter app, String data) {
        this(app, data, "Midi File");
    }

    @Override
    protected Void performAction() {
        File midiFile = new MidiFileWriter(data).apply();
        PLAYER.open();
        PLAYER.load(midiFile, title);
        PLAYER.show();
        return null;
    }

}
