package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.popup.CheatSheet;
import abc.writer.app.gui.popup.MidiChannelsCheatSheet;

/**
 *
 * @author hl
 */
public class ShowMidiChannelCheatSheetAction extends AbcWriterAction<Void> {

    private static final MidiChannelsCheatSheet CONTENT = new MidiChannelsCheatSheet();

    public ShowMidiChannelCheatSheetAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        CheatSheet.of(app, 250, 750, CONTENT, "Midi Channel Cheat Sheet").show(app.getStage());
        return null;
    }
}
