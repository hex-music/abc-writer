package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.FileType;
import abc.writer.app.entity.FileEntity;
import java.io.File;

/**
 *
 * @author hl
 */
public class FileItemFactory {

    public FileItemFactory() {
    }

    public static FileItem fileItem(AbcWriter app, File file) {
        FileEntity entity = FileEntity.from(file);
        if (FileType.of(file).equals(FileType.ABC)) {
            return new AbcFileItem(app, entity);
        }
        return new FileItem(app, entity);
    }
}
