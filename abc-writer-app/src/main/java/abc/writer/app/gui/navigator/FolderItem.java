package abc.writer.app.gui.navigator;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.action.OpenFolderInSystemAction;
import abc.writer.app.entity.FileEntity;
import abc.writer.app.FileType;
import abc.writer.app.action.AddFileAction;
import abc.writer.app.action.AddFolderAction;
import abc.writer.app.action.DeleteAction;
import abc.writer.app.action.OpenAction;
import abc.writer.app.entity.Entity;
import abc.writer.app.io.FileMonitor;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author hl
 */
public class FolderItem extends TreeItem<Entity> implements FileBrowserItem {

    private static final long INTERVAL = 1_000l;
    private final File folder;
    private final AbcWriter app;
    private boolean isChanged = false;
    private Timeline monitor;

    public FolderItem(AbcWriter app, FileEntity fileEntity) {
        super(fileEntity, getImageView());
        this.folder = fileEntity.getContent();
        this.app = app;
        init();
    }

    @Override
    public void performDeleteAction() {
        new DeleteAction(app, folder).actionPerformed();
    }

    @Override
    public ContextMenu getContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem openInSystemItem = new MenuItem("Open in system");
        openInSystemItem.setOnAction(e -> new OpenFolderInSystemAction(app, folder).actionPerformed());
        MenuItem newFolderItem = new MenuItem("New Folder");
        newFolderItem.setOnAction(e -> new AddFolderAction(app, folder).actionPerformed());
        MenuItem newFileItem = new MenuItem("New File");
        newFileItem.setOnAction(e -> new AddFileAction(app, folder).actionPerformed().ifPresent(this::addFile));
        MenuItem deleteFolderItem = new MenuItem("Delete folder");
        deleteFolderItem.setOnAction(e -> performDeleteAction());
        menu.getItems().addAll(openInSystemItem, newFolderItem, newFileItem, deleteFolderItem);
        return menu;
    }

    @Override
    public String getText() {
        return folder.getName();
    }

    @Override
    public Tooltip getTooltip() {
        return new Tooltip("Folder");
    }

    @Override
    public void performDefaultAction() {
        setExpanded(true);
    }

    private void init() {
        if (folder.getParentFile().equals(Constants.PUBLIC_DIRECTORY) && folder.getName().equals("ABC")) {
            setExpanded(true);
        }
        addFolderMonitor();
        List<TreeItem<Entity>> folders = Stream.of(folder.listFiles())
                .filter(f -> f.isDirectory() && f.getParentFile().equals(folder) && !f.isHidden())
                .sorted((a, b) -> a.getName().compareTo(b.getName()))
                .map(this::createFolderItem)
                .collect(Collectors.toList());
        getChildren().addAll(folders);
        List<TreeItem<Entity>> files = Stream.of(folder.listFiles())
                .filter(f -> f.isFile() && f.getParentFile().equals(folder) && !f.isHidden() && isSupportedType(f))
                .sorted((a, b) -> a.getName().compareTo(b.getName()))
                .map(this::createFileItem)
                .collect(Collectors.toList());
        getChildren().addAll(files);
    }

    private FolderItem createFolderItem(File folder) {
        try {
            FileMonitor.getInstance().addFileChangeListener(this::checkChanges, folder, INTERVAL);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FolderItem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new FolderItem(app, FileEntity.from(folder));
    }

    private FileItem createFileItem(File file) {
        try {
            FileMonitor.getInstance().addFileChangeListener(this::checkChanges, file, INTERVAL);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FolderItem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return FileItemFactory.fileItem(app, file);
    }

    private boolean isSupportedType(File file) {
        return FileType.isSupported(file.getAbsolutePath());
    }

    private void addFile(File addFile) {
        try {
            FileMonitor.getInstance().addFileChangeListener(this::checkChanges, addFile, 10_000l);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FolderItem.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (addFile.isDirectory()) {
            getChildren().add(createFolderItem(addFile));
        } else {
            getChildren().add(createFileItem(addFile));
        }
        getChildren().sort((a, b) -> FileUtil.sortFiles((File) a.getValue().getContent(), (File) b.getValue().getContent()));
        if (FileType.editableTypes().contains(FileType.of(addFile))) {
            new OpenAction(app, addFile).actionPerformed();
        }
    }

    private boolean filterFiles(File f) {
        return f.getParentFile().equals(folder) && !f.isHidden() && f.isDirectory() || isSupportedType(f);
    }

    private static ImageView getImageView() {
        ImageView result = new ImageView("layout/image/icon/folder.png");
        result.setFitHeight(18);
        result.setFitWidth(18);
        return result;
    }

    private void updateFolder() {
        if (folder == null || !folder.exists()) {
            monitor.stop();
            removeFolder();
            return;
        }
        List<File> filesInSystemFolder = Stream.of(folder.listFiles())
                .filter(this::filterFiles)
                .collect(Collectors.toList());
        List<String> fileNames = getChildren().stream().map(treeItem -> treeItem.getValue().getLabel()).collect(Collectors.toList());
        filesInSystemFolder.stream().filter(f -> !fileNames.contains(f.getName())).forEach(this::addFile);
    }

    private void checkChanges(File file) {
        if (!file.exists()) {
            isChanged = true;
            removeFile(file);
        }
        checkChanges();
    }

    private void removeFolder() {
        removeFile(folder);
    }

    private void removeFile(File file) {
        getChildren().removeAll(getChildren().stream().filter(ti -> ti.getValue().getLabel().equals(file.getName())).collect(Collectors.toList()));
    }

    private void checkChanges() {
        if (!isChanged) {
            List<String> childrenLabels = getChildren().stream().map(ti -> ti.getValue().getLabel()).collect(Collectors.toList());
            List<String> fileNames = Stream.of(folder.listFiles()).filter(this::filterFiles).map(f -> f.getName()).collect(Collectors.toList());
            isChanged = !Objects.equals(childrenLabels, fileNames);
        }
        if (isChanged) {
            updateFolder();
            isChanged = false;
        }
    }

    private void addFolderMonitor() {
        monitor = new Timeline(new KeyFrame(Duration.millis(INTERVAL), (e) -> updateFolder()));
        monitor.setCycleCount(Timeline.INDEFINITE);
        monitor.play();
    }
}
