package abc.writer.app;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.stage.FileChooser;

/**
 *
 * @author hl
 */
public enum FileType {

    ABC("Abc Music", "text/vnd.abc") {
        @Override
        public boolean hasTemplate() {
            return true;
        }
    },
    FMT("Abc Format Template", "text/vnd.abc.fmt") {
        @Override
        public boolean hasTemplate() {
            return true;
        }
    },
    MID("Midi Audio", "audio/midi"),
    PDF("Portable Document Format", "application/pdf"),
    PS("PostScript", "application/postscript"),
    SWT("Story Writer", "text/vnd.hex.nu.story-writer.swt") {
        @Override
        public boolean hasTemplate() {
            return true;
        }

    },
    TXT("Plain text", "text/plain"),
    WAV("Waveform Audio", "audio/vnd.wave"),
    XML("Music Xml", "application/xml"),
    EMPTY("Empty File", "n/a") {
        @Override
        public String suffix() {
            return "";
        }

        @Override
        public FileChooser.ExtensionFilter getExtensionFilter() {
            return null;
        }
    };
    private final String label;
    private final String mediatype;

    private FileType(String label, String mediatype) {
        this.label = label;
        this.mediatype = mediatype;
    }

    public boolean hasTemplate() {
        return false;
    }

    public String getLabel() {
        return label;
    }

    public String getMediatype() {
        return mediatype;
    }

    public String suffix() {
        return "." + name().toLowerCase();
    }

    public static List<FileType> editableTypes() {
        return Arrays.asList(ABC, FMT, SWT, TXT, EMPTY);
    }

    public static FileType of(File file) {
        if (file.isDirectory()) {
            return null;
        }
        return of(file.getName());
    }

    public static FileType of(String string) {
        if (string == null || string.isEmpty() || !string.contains(".")) {
            return EMPTY;
        }
        if (string.contains(".")) {
            return valueOf(string.substring(string.lastIndexOf(".") + 1).toUpperCase());
        }
        return valueOf(string.toUpperCase());
    }

    public static boolean isSupported(String string) {
        if (!string.contains(".")) {
            return true;
        }
        return suffices().stream().anyMatch(suf -> string.substring(string.lastIndexOf(".")).toLowerCase().equals(suf));
    }

    public boolean isEditable() {
        return editableTypes().contains(this);
    }

    public static List<String> suffices() {
        return Stream.of(values()).map(filetype -> filetype.suffix()).collect(Collectors.toList());
    }

    public FileChooser.ExtensionFilter getExtensionFilter() {
        return new FileChooser.ExtensionFilter(getLabel() + " file (" + name().toLowerCase() + ")", "*" + suffix());
    }

    public static List<FileChooser.ExtensionFilter> getExtensionFilters() {
        return Stream.of(values()).filter(ft -> !ft.equals(EMPTY)).map(ft -> ft.getExtensionFilter()).collect(Collectors.toList());
    }

}
