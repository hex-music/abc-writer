package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.Editor;
import java.io.File;
import java.util.Optional;

/**
 *
 * @author hl
 */
public class SaveAction extends AbcWriterAction<Optional<File>> {

    public SaveAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Optional<File> performAction() {
        if (app.getCurrentEditor() == null) {
            return Optional.empty();
        }
        if (app.getCurrentEditor().getState().equals(Editor.State.NEW)) {
            app.getCurrentEditor().setIgnoreChanges();
            Optional<File> newFile = new SaveFileAsAction(app).actionPerformed();
            if (!newFile.isPresent()) {
                app.getCurrentEditor().setUnSaved();
            }
            return newFile;
        }
        Editor editor = app.getCurrentEditor();
        Optional<File> result = new SaveEditorContentAction(app, editor).actionPerformed();
        result.ifPresent(f -> setLeftStatus("Saved file: " + f.getAbsolutePath(), 4));
        return result;
    }
}
