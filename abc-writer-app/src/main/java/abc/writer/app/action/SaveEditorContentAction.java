package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.Editor;
import abc.writer.app.gui.TextEditor;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import javafx.stage.FileChooser;

/**
 *
 * @author hl
 */
public class SaveEditorContentAction extends AbcWriterAction<Optional<File>> {

    private final Editor editor;

    public SaveEditorContentAction(AbcWriter app, Editor editor) {
        super(app);
        this.editor = editor;
    }

    @Override
    protected Optional<File> performAction() {
        if (!editor.isChanged()) {
            return Optional.empty();
        }
        File file = getFile();
        if (file == null) {
            return Optional.empty();
        }
        FileUtil.write(editor.getEditorText().getBytes(StandardCharsets.UTF_8), file.getAbsolutePath());
        if (isNewFile(file)) {
            editor.update(file);
        }
        editor.setSaved();
        return Optional.of(file);
    }

    private File getFile() {
        if (isAbcEditor()) {
            AbcEditor abcEditor = (AbcEditor) editor;
            AbcEditor.AbcFile abcFile = abcEditor.get();
            return abcFile.file() == null ? selectFile(abcEditor.getText(), true) : abcFile.file();
        } else if (isTextEditor()) {
            TextEditor textEditor = (TextEditor) editor;
            return textEditor.get() == null ? selectFile("New File", false) : textEditor.get();
        }
        return null;
    }

    private boolean isNewFile(File file) {
        return !file.getAbsolutePath().equals(editor.getKey());
    }

    private File selectFile(String filename, boolean isAbcFile) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(getExtensionFilters());
        if (isAbcFile) {
            chooser.setInitialDirectory(Constants.PUBLIC_ABC_DIRECTORY);
            if (!filename.endsWith(FileType.ABC.suffix())) {
                filename += FileType.ABC.suffix();
            }
        } else {
            chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }
        chooser.setInitialFileName(filename);
        return chooser.showSaveDialog(app.getStage());
    }

    private boolean isAbcEditor() {
        return editor instanceof AbcEditor;
    }

    private boolean isTextEditor() {
        return editor instanceof TextEditor;
    }
}
