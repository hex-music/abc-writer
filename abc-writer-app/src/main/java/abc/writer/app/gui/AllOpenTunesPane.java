package abc.writer.app.gui;

import abc.writer.app.AbcWriter;
import abc.writer.app.action.CreateNewFileFromTuneListAction;
import abc.writer.app.entity.FileInfo;
import abc.writer.app.entity.Tune;
import abc.writer.app.gui.control.CloseLabel;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author hl
 */
public class AllOpenTunesPane extends BorderPane {

    private static final int TUNE_BOX_WIDTH = 240;
    private final AbcWriter app;
    private final VBox tunesBox;
    private final ScrollPane scrollPane;

    public AllOpenTunesPane(AbcWriter app) {
        this.app = app;
        this.tunesBox = new VBox(15);
        this.scrollPane = new ScrollPane(this.tunesBox);
        init();
    }

    private void init() {
        scrollPane.setFitToHeight(true);
        tunesBox.setPadding(new Insets(10));
        setTop(getHeader());
        setBottom(getFooter());
        setCenter(this.scrollPane);
        app.addEditorsPaneListener(this::tabsChanged);
        setupContent();
    }

    private void tabsChanged(ListChangeListener.Change<? extends Tab> change) {
        while (change.next()) {
            removeTabs(change.getRemoved());
            addTabs(change.getAddedSubList());
        }
    }

    private void addTabs(List<? extends Tab> addedTabs) {
        addedTabs.stream()
                .filter(t -> t instanceof AbcEditor)
                .map(tab -> (AbcEditor) tab)
                .filter(editor -> editor.getFileInfo() != null && !getOpenAbcEditorsFileInfo().contains(editor.getFileInfo()))
                .forEach(this::addFileBox);
    }

    private void removeTabs(List<? extends Tab> removedTabs) {
        removedTabs.stream()
                .filter(t -> t instanceof AbcEditor)
                .map(tab -> ((AbcEditor) tab).getFileInfo())
                .forEach(this::removeFileBox);
    }

    private List<FileInfo> getOpenAbcEditorsFileInfo() {
        return app.getOpenEditors().stream()
                .filter(e -> e instanceof AbcEditor)
                .map(e -> ((AbcEditor) e).getFileInfo())
                .collect(Collectors.toList());
    }

    private List<Tune> getSelectedTunes() {
        return getFileBoxes().stream()
                .map(fb -> fb.getSelectedTunes())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private void setupContent() {
        app.getOpenEditors().stream()
                .filter(e -> e instanceof AbcEditor)
                .map(e -> (AbcEditor) e)
                .forEach(this::addFileBox);
    }

    private void addFileBox(AbcEditor editor) {
        editor.addLabelListener(this::labelUpdated);
        tunesBox.getChildren().add(new FileBox(app, editor.getFileInfo()));
    }

    private void labelUpdated(String newLabel) {
        update();
    }

    private void update() {
        List<String> openTabLabels = app.getOpenEditors().stream()
                .map(editor -> editor.getLabel())
                .collect(Collectors.toList());
        tunesBox.getChildren().removeAll(getFileBoxes().stream()
                .filter(box -> !openTabLabels.contains(box.getLabel()))
                .collect(Collectors.toList()));
        List<String> boxes = getFileBoxes().stream()
                .map(box -> box.getLabel()).collect(Collectors.toList());
        app.getOpenEditors().stream()
                .filter(editor -> !boxes.contains(editor.getLabel()))
                .map(editor -> (AbcEditor) editor)
                .forEach(this::addFileBox);
    }

    private void removeFileBox(FileInfo fileInfo) {
        tunesBox.getChildren().removeAll(getFileBoxes().stream()
                .filter(fb -> compareFileBox(fb, fileInfo.getLabel()))
                .collect(Collectors.toList()));
    }

    private static boolean compareFileBox(FileBox fb, String fileInfoLabel) {
        return fb.getFileInfo().getLabel().equals(fileInfoLabel);
    }

    private ObservableList<FileBox> getFileBoxes() {
        return FXCollections.observableList(tunesBox.getChildren().stream()
                .filter(n -> n instanceof FileBox)
                .map(n -> (FileBox) n)
                .collect(Collectors.toList()));
    }

    private Node getHeader() {
        HBox headerBox = new HBox(5);
        Label headerLabel = new Label("All open tunes");
        headerLabel.setPrefSize(TUNE_BOX_WIDTH, 20);
        headerLabel.getStyleClass().addAll("all-tunes-label");
        headerLabel.setAlignment(Pos.CENTER);
        headerBox.getChildren().addAll(headerLabel, CloseLabel.from(() -> app.setRight(null)));
        return headerBox;
    }

    private Node getFooter() {
        HBox footerBox = new HBox(5);
        Button newTabButton = new Button("New file");
        Tooltip.install(newTabButton, new Tooltip("Create new file from selected tunes"));
        newTabButton.setOnAction((e) -> new CreateNewFileFromTuneListAction(app, getSelectedTunes()).actionPerformed());
        footerBox.getChildren().addAll(newTabButton);
        return footerBox;
    }

    private static class FileBox extends VBox {

        private List<TuneBox> tuneBoxes;
        private final AbcWriter app;
        private final FileInfo fileInfo;
        private Button fileButton;

        public FileBox(AbcWriter app, FileInfo fileInfo) {
            super(5);
            this.app = app;
            this.fileInfo = fileInfo;
            init();
        }

        public String getLabel() {
            return fileButton.getText();
        }

        public FileInfo getFileInfo() {
            return fileInfo;
        }

        public List<Tune> getSelectedTunes() {
            return tuneBoxes.stream()
                    .filter(tb -> tb.isSelected())
                    .map(tb -> tb.getTune())
                    .collect(Collectors.toList());
        }

        private void init() {
            getStyleClass().add("abc-file-box");
            fileButton = getFileNameButton(fileInfo.getLabel());
            tuneBoxes = fileInfo.getContent()
                    .stream()
                    .map(tune -> new TuneBox(app, tune))
                    .collect(Collectors.toList());
            fileButton.setOnAction((e) -> {
                tuneBoxes.forEach(box -> box.setSelected(true));
            });
            fileButton.setContextMenu(createMenu());
            getChildren().add(fileButton);
            getChildren().addAll(tuneBoxes);
        }

        private Button getFileNameButton(String path) {
            String label = path.substring(path.lastIndexOf("/") + 1);
            Button fileButton = new Button(label);
            Tooltip.install(fileButton, new Tooltip("File: " + path + "\n\nClick to select all tunes."));
            fileButton.setPrefSize(TUNE_BOX_WIDTH, 20);
            fileButton.getStyleClass().addAll("file-name-button");
            return fileButton;
        }

        private ContextMenu createMenu() {
            ContextMenu menu = new ContextMenu();
            return menu;
        }

    }

    private static class TuneBox extends CheckBox {

        private final AbcWriter app;
        private final Tune tune;

        public TuneBox(AbcWriter app, Tune tune) {
            super(tune.getLabel());
            this.app = app;
            this.tune = tune;
        }

        public Tune getTune() {
            return tune;
        }
    }
}
