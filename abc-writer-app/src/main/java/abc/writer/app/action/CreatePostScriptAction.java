package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.Editor;
import java.io.File;

/**
 *
 * @author hl
 */
public class CreatePostScriptAction extends AbstractPostScriptAction {

    public CreatePostScriptAction(AbcWriter app, Editor editor) {
        super(app, editor, Action.CREATE_PS);
    }

    public CreatePostScriptAction(AbcWriter app) {
        this(app, app.getCurrentEditor());
    }

    @Override
    protected File performAction() {
        return apply();
    }
}
