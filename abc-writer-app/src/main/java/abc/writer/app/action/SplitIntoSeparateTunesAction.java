package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.FileType;
import abc.writer.app.entity.FileInfo;
import abc.writer.app.gui.popup.AbcDialog;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.popup.SplitOptionsDialog;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;
import javafx.stage.DirectoryChooser;

/**
 *
 * @author hl
 */
public class SplitIntoSeparateTunesAction extends AbcWriterAction<File> {

    private final FileInfo fileInfo;

    public SplitIntoSeparateTunesAction(AbcWriter app) {
        this(app, getFileInfo(app));
    }

    public SplitIntoSeparateTunesAction(AbcWriter app, FileInfo fileInfo) {
        super(app);
        this.fileInfo = fileInfo;
    }

    @Override
    protected File performAction() {
        if (fileInfo == null) {
            return null;
        }
        Options options = getOptions();
        if (options == null) {
            return null;
        }
        fileInfo.getContent().forEach(t -> {
            String path = options.outputDirectory.getAbsolutePath() + "/" + zeroFileIndex(t.getIndex()) + "-" + t.getLabel() + FileType.ABC.suffix();
            StringBuilder builder = new StringBuilder();
            if (options.includePreamble) {
                fileInfo.getPreamble().ifPresent(p -> builder.append(p.getContent()).append("\n\n"));
            }
            builder.append(t.getAsString());
            FileUtil.write(builder.toString().getBytes(StandardCharsets.UTF_8), path);
        });
        return options.outputDirectory;
    }

    private Options getOptions() {
        File sourceFile = new File(fileInfo.getLabel());
        File outputDirectory = new File(sourceFile.getParent() + "/" + sourceFile.getName().substring(0, sourceFile.getName().indexOf(".")) + "/");
        boolean outputDirectoryCreated = outputDirectory.mkdirs();
        SplitOptionsDialog dialog = new SplitOptionsDialog(outputDirectory.getAbsolutePath() + "/");
        AbcDialog.Result result = dialog.showAndWait().orElse(AbcDialog.Result.CANCEL);
        if (result.equals(AbcDialog.Result.CANCEL)) {
            return null;
        }
        if (Boolean.valueOf(dialog.getOptions().get(SplitOptionsDialog.USE_DEFAULT_DIRECTORY))) {
            return new Options(Boolean.valueOf(dialog.getOptions().get(SplitOptionsDialog.INCLUDE_PREAMBLE)), outputDirectory);
        }
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setInitialDirectory(outputDirectory);
        File dialogResult = chooser.showDialog(app.getStage());
        if (dialogResult == null) {
            return null;
        }
        if (outputDirectoryCreated && canDelete(outputDirectory, dialogResult)) {
            outputDirectory.delete();
        }
        return new Options(false, dialogResult);
    }

    private boolean canDelete(File outputDirectory, File dialogResult) {
        return !Stream.of(outputDirectory.list()).findAny().isPresent()
                && !dialogResult.equals(outputDirectory);
    }

    private String zeroFileIndex(int index) {
        String result = "" + index;
        int numberOfTunes = fileInfo.getContent().size();
        int numberOfInts = 1;
        if (numberOfTunes > 999) {
            numberOfInts = 4;
        } else if (numberOfTunes > 99) {
            numberOfInts = 3;
        } else if (numberOfTunes > 9) {
            numberOfInts = 2;
        }
        while (result.length() < numberOfInts) {
            result = 0 + result;
        }
        return result;
    }

    private static FileInfo getFileInfo(AbcWriter app) {
        return app.getCurrentEditor() == null || !(app.getCurrentEditor() instanceof AbcEditor) ? null : ((AbcEditor) app.getCurrentEditor()).getFileInfo();
    }

    private static class Options {

        final boolean includePreamble;
        final File outputDirectory;

        public Options(boolean includePreamble, File outputDirectory) {
            this.includePreamble = includePreamble;
            this.outputDirectory = outputDirectory;
        }
    }
}
