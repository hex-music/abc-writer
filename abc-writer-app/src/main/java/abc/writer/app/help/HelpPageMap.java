package abc.writer.app.help;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hl
 */
public class HelpPageMap {

    public static final String START = "Start",
            SHORTCUTS = "Shortcuts",
            ABC_2_XML = "Abc2xml",
            XML_2_ABC = "Xml2abc";
    private static final HelpPageMap INSTANCE = new HelpPageMap();

    private final Map<String, String> pagesMap = new HashMap<>();

    private HelpPageMap() {
        init();
    }

    public static HelpPageMap instance() {
        return INSTANCE;
    }

    private void init() {
        pagesMap.put(START, "http://localhost:88/abc-writer/index.xhtml");
        pagesMap.put(SHORTCUTS, "http://localhost:88/abc-writer/shortcuts.xhtml");
        pagesMap.put(ABC_2_XML, "http://localhost:88/abc-writer/abc2xml.html");
        pagesMap.put(XML_2_ABC, "http://localhost:88/abc-writer/xml2abc.html");
    }

    String getUrl(String key) {
        return pagesMap.get(key);
    }

}
