package abc.writer.app.entity;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author hl
 */
public class Voice implements Entity<String> {

    private static final Pattern HEADER_FIELD_PATTERN = Pattern.compile("([A-UX-Z]:)|([%\\\\].)");
    private static final Pattern NAME_PATTERN = Pattern.compile("V:[a-zA-Z0-9]{1,5} name=\"([-a-zåäöA-ZÅÄÖ0-9\\s]+)\".*");
    private final String content;
    private final String label;
    private final String id;
    private final Tune tune;

    private Voice(Tune tune, String content, String label, String id) {
        this.tune = tune;
        this.content = content;
        this.label = label;
        this.id = id;
    }

    public static Voice of(Tune tune, String tuneContent) {
        if (!tuneContent.contains("\n")) {
            return null;
        }
        String id = null;
        String label = "Voice";
        if (tuneContent.contains("V:")) {
            String firstLine = tuneContent.substring(0, tuneContent.indexOf("\n")).trim();
            id = firstLine.substring(2).trim();
            if (id.contains(" ")) {
                id = id.substring(0, id.indexOf(" "));
            }
            label += " " + id;
            Matcher matcher = NAME_PATTERN.matcher(firstLine);
            if (matcher.find()) {
                label = matcher.group(1);
            }
        }
        String content = tuneContent;
        return new Voice(tune, content, label, id);
    }

    public Tune getTune() {
        return tune;
    }

    public Optional<String> getId() {
        return Optional.ofNullable(id);
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getContent() {
        return content;
    }

    public String getAsAbcString() {
        return getStringForStartOfTune(tune) + content;
    }

    private String getStringForStartOfTune(Tune tune) {
        String abcContent = tune.getAsString();
        int end = 0;
        for (String line : abcContent.split("\n")) {
            if (line.length() < 2 || HEADER_FIELD_PATTERN.matcher(line.substring(0, 2)).matches()) {
                end += line.length() + 1;
            } else {
                break;
            }
        }
        return abcContent.substring(abcContent.indexOf("X:"), end);
    }

}
