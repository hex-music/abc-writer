package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.AbcEditor;
import abc.writer.app.gui.SearchPanel;

/**
 *
 * @author hl
 */
public class FindAction extends AbcWriterAction<Void> {

    private final AbcEditor editor;
    private final boolean replace;

    public FindAction(AbcWriter app, AbcEditor editor) {
        this(app, editor, false);
    }

    public FindAction(AbcWriter app, AbcEditor editor, boolean replace) {
        super(app);
        this.editor = editor;
        this.replace = replace;
    }

    @Override
    protected Void performAction() {
        SearchPanel searchPanel = editor.openSearchPanel();
        searchPanel.setFocus();
        if (replace) {
            searchPanel.showReplacePanel();
        } else {
            searchPanel.hideReplacePanel();
        }
        return null;
    }

}
