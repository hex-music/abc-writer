package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import javafx.application.Platform;

/**
 * Created 2017-okt-27
 *
 * @author hl
 */
public class ExitAction extends AbcWriterAction<Void> {

    public ExitAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Void performAction() {
        Platform.exit();
        return null;
    }

}
