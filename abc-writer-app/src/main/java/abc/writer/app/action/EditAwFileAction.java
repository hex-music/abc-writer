package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.gui.TextEditor;
import abc.writer.app.io.FileUtil;
import java.io.File;

/**
 *
 * @author hl
 */
public class EditAwFileAction extends AbcWriterAction<Void> {

    private final File file;

    public EditAwFileAction(AbcWriter app, File file) {
        super(app);
        this.file = file;
    }

    @Override
    protected Void performAction() {
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            byte[] awContent = FileUtil.inputStreamToByteArray(TextEditor.class.getResourceAsStream("/writer/aw.xml"));
            FileUtil.write(awContent, file.getAbsolutePath());
        }
        TextEditor awEditor = new TextEditor(app, file, "Abc Writer - Properties");
        app.setCurrentEditor(awEditor);
        return null;

    }

}
