package abc.writer.app.gui.control;

/**
 *
 * @author hl
 */
@FunctionalInterface
public interface CloseListener {

    abstract void closed();
}
