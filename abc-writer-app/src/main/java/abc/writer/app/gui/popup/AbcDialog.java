package abc.writer.app.gui.popup;

import javafx.scene.control.Dialog;

/**
 *
 * @author hl
 * @param <T>
 */
public abstract class AbcDialog<T> extends Dialog<T> {

    public static enum Result {
        SAVE, SAVE_AND_CLOSE, YES, NO, CANCEL;
    }
}
