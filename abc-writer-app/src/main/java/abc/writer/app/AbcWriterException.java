package abc.writer.app;

/**
 *
 * @author hl
 */
public class AbcWriterException extends RuntimeException {

    public AbcWriterException(String message) {
        super(message);
    }

    public AbcWriterException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbcWriterException(Throwable cause) {
        super(cause);
    }

}
