package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.AbcWriterException;
import abc.writer.app.system.WriterProperty;
import java.io.File;
import java.io.IOException;
import javafx.application.Platform;

/**
 *
 * @author hl
 */
public class OpenFolderInSystemAction extends AbcWriterAction<Void> {

    private final File folder;

    public OpenFolderInSystemAction(AbcWriter app, File folder) {
        super(app);
        this.folder = folder;
    }

    @Override
    protected Void performAction() {
        Platform.runLater(() -> {
            try {
                String fileManager = WriterProperty.getProperty("file-manager").orElse("nautilus");
                Process process = new ProcessBuilder("sh", "-c", fileManager + " " + folder.getAbsolutePath()).start();
            } catch (IOException ex) {
                throw new AbcWriterException(ex);
            }
        });
        return null;
    }

}
