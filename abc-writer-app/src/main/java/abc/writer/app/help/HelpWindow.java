package abc.writer.app.help;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author hl
 */
public class HelpWindow {

    static final double WIDTH = 825, HEIGHT = 750;
    private final HelpBrowser browser;
    private final Scene scene;
    private final Stage stage;

    public HelpWindow() {
        browser = new HelpBrowser();
        BorderPane root = new BorderPane();
        root.setPrefSize(WIDTH, HEIGHT);
        stage = new Stage();
        stage.setTitle("ABC Writer - Help");
        scene = new Scene(root, WIDTH, HEIGHT);
        stage.setScene(scene);
        stage.show();
        root.setCenter(browser);
    }

    public void show() {
        stage.show();
    }

    public void showAndWait() {
        stage.showAndWait();
    }

    public void hide() {
        stage.hide();
    }

}
