package abc.writer.app.gui;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.util.Duration;

/**
 *
 * @author hl
 */
public class StatusBar extends BorderPane {

    public static final int SIDE_WIDTH = 450;
    private final Label left;
    private final Label right;
    private final Label center;
    private static final StatusBar INSTANCE = new StatusBar();

    private StatusBar() {
        setPrefHeight(20);
        left = new Label("");
        left.getStyleClass().add("abc-status-bar left");
        left.setPadding(insets());
        left.setPrefWidth(SIDE_WIDTH);
        left.setMinWidth(SIDE_WIDTH);
        left.setAlignment(Pos.CENTER_LEFT);
        center = new Label("");
        center.getStyleClass().add("abc-status-bar center");
        center.setPadding(insets());
        center.setAlignment(Pos.CENTER);
        right = new Label("");
        right.getStyleClass().add("abc-status-bar right");
        right.setPadding(insets());
        right.setPrefWidth(SIDE_WIDTH);
        right.setMinWidth(SIDE_WIDTH);
        right.setAlignment(Pos.CENTER_RIGHT);
        setLeft(left);
        setCenter(center);
        setRight(right);
    }

    public static StatusBar instance() {
        return INSTANCE;
    }

    public void leftMessage(Node message) {
        setLeft(message);
    }

    public void leftMessage(Node message, double timeout) {
        leftMessage(message);
        new Timeline(new KeyFrame(Duration.seconds(timeout), this::clearLeft)).play();
    }

    public void leftMessage(String message) {
        left.setText(message);
        leftMessage(left);
    }

    public void leftMessage(String message, double timeout) {
        left.setText(message);
        leftMessage(left, timeout);
    }

    private void clearLeft(ActionEvent e) {
        clearLeft();
    }

    public void clearLeft() {
        left.setText("");
    }

    public void centerNode(Node message) {
        setCenter(message);
    }

    public void centerNode(Node message, double timeout) {
        centerNode(message);
        new Timeline(new KeyFrame(Duration.seconds(timeout), this::clearCenter)).play();
    }

    public void centerMessage(String message) {
        center.setText(message);
        centerNode(center);
    }

    public void centerMessage(String message, double timeout) {
        center.setText(message);
        centerNode(center, timeout);
    }

    private void clearCenter(ActionEvent e) {
        clearCenter();
    }

    public void clearCenter() {
        center.setText("");
    }

    public void rightMessage(Node message) {
        setRight(message);
    }

    public void rightMessage(Node message, double timeout) {
        rightMessage(message);
        new Timeline(new KeyFrame(Duration.seconds(timeout), this::clearRight)).play();
    }

    public void rightMessage(String message) {
        right.setText(message);
        rightMessage(right);
    }

    public void rightMessage(String message, double timeout) {
        right.setText(message);
        rightMessage(right, timeout);
    }

    private void clearRight(ActionEvent e) {
        clearRight();
    }

    public void clearRight() {
        right.setText("");
    }

    private Insets insets() {
        return new Insets(2, 10, 2, 10);
    }
}
