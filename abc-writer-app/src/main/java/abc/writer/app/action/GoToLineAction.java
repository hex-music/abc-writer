package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import java.util.Optional;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author hl
 */
public class GoToLineAction extends AbcWriterAction<Optional<Integer>> {

    private final Integer numberOfLines;

    public GoToLineAction(AbcWriter app, Integer numberOfLines) {
        super(app);
        this.numberOfLines = numberOfLines;
    }

    @Override
    protected Optional<Integer> performAction() {
        TextInputDialog dialog = new TextInputDialog("0");
        dialog.setTitle("Go to line...");
        dialog.setHeaderText(null);
        dialog.setContentText("Go to line: ");
        return dialog.showAndWait()
                .map(s -> s.replaceAll("\\D", ""))
                .filter(s -> !s.isEmpty())
                .map(s -> {
                    Integer i = Integer.valueOf(s);
                    return i >= numberOfLines ? numberOfLines : i;
                });
    }

}
