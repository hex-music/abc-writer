package abc.writer.app.system;

import abc.writer.app.AbcWriterException;
import abc.writer.app.Constants;
import abc.writer.app.io.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import xmlight.DocumentToXmlNodeParser;
import xmlight.NodeFactory;
import xmlight.XmlNode;

/**
 *
 * @author hl
 */
public class WriterLogger {

    public static final String LAST_SAVED_IN_DIR = "last-saved-in-dir";
    public static final String LAST_OPENED_FROM_DIR = "last-opened-from-dir";
    public static final String LAST_OPENED_FILE = "last-opened-file";

    public static File getLogFile() {
        return logFile();
    }

    public static void setLog(String key, String value) {
        XmlNode writerLog = writerLogNode();
        XmlNode logNode = NodeFactory.createNode("log", value);
        logNode.addAttribute("key", key);
        logNode.addAttribute("timestamp", LocalDateTime.now().toString());
        writerLog.addChild(0, logNode);
        update(writerLog);
    }

    public static Optional<Item> getLog(String key) {
        return writerLogNode().getChildren("log").stream()
                .filter(n -> n.hasAttribute("key") && n.getAttribute("key").equals(key))
                .findFirst()
                .map(Item::new);
    }

    public static List<Item> getDistinctList(String key) {
        return writerLogNode().getChildren("log").stream()
                .filter(n -> n.hasAttribute("key") && n.getAttribute("key").equals(key))
                .map(Item::new)
                .filter(distinctByKey(b -> b.getValue()))
                //                .map(n -> n.getText())
                //                .distinct()
                .collect(Collectors.toList());
    }

    private static void update(XmlNode writerLogNode) {
        StringBuilder logBuilder = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<writer-log version=\"1.0\">\n");
        writerLogNode.getChildren("log").forEach(log -> logBuilder.append("    ").append(log.toString()).append("\n"));
        logBuilder.append("</writer-log>\n");
        FileUtil.write(logBuilder.toString().getBytes(StandardCharsets.UTF_8), Constants.LOG_PATH);
    }

    private static XmlNode writerLogNode() {
        try {
            return new DocumentToXmlNodeParser(new FileInputStream(logFile())).parse();
        } catch (FileNotFoundException ex) {
            throw new AbcWriterException("Could not parse Log file", ex);
        }
    }

    private static File logFile() {
        if (new File(Constants.LOG_PATH).exists()) {
            return new File(Constants.LOG_PATH);
        }
        File file = new File(Constants.LOG_PATH);
        file.getParentFile().mkdirs();
        byte[] docByteArray = FileUtil.inputStreamToByteArray(WriterLogger.class.getResourceAsStream("/writer/log.xml"));
        FileUtil.write(docByteArray, file.getAbsolutePath());
        return file;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static class Item {

        private final XmlNode node;

        public Item(XmlNode node) {
            this.node = node;
        }

        public String getKey() {
            return node.getAttribute("key");
        }

        public String getValue() {
            return node.getText();
        }

        public LocalDateTime getTimestamp() {
            return LocalDateTime.parse(node.getAttribute("timestamp"));
        }
    }

}
