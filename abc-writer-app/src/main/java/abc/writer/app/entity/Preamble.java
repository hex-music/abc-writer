package abc.writer.app.entity;

/**
 *
 * @author hl
 */
public class Preamble implements Entity<String> {

    private final String content;

    private Preamble(String content) {
        this.content = content;
    }

    public static Preamble from(String content) {
        return new Preamble(content);
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getLabel() {
        return "Preamble";
    }

}
