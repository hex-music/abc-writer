package abc.writer.app.action;

import abc.writer.app.AbcWriter;
import abc.writer.app.Constants;
import abc.writer.app.FileType;
import abc.writer.app.gui.Editor;
import abc.writer.app.system.WriterLogger;
import java.io.File;
import java.util.List;
import java.util.Optional;
import javafx.stage.FileChooser;

/**
 *
 * @author hl
 */
public class SelectFileToOpenAction extends AbcWriterAction<Optional<Editor>> {

    public SelectFileToOpenAction(AbcWriter app) {
        super(app);
    }

    @Override
    protected Optional<Editor> performAction() {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(getExtensionFilters());
        chooser.setSelectedExtensionFilter(FileType.ABC.getExtensionFilter());
        File initialDirectory = WriterLogger.getLog(WriterLogger.LAST_OPENED_FROM_DIR)
                .map(i -> i.getValue())
                .map(File::new)
                .orElse(Constants.PUBLIC_ABC_DIRECTORY);
        initialDirectory.mkdirs();
        chooser.setInitialDirectory(initialDirectory);
        chooser.setTitle("Select ABC Music file to open");
        File file = chooser.showOpenDialog(app.getStage());
        if (file != null) {
            WriterLogger.setLog(WriterLogger.LAST_OPENED_FROM_DIR, file.getParent());
        }
        return new OpenAction(app, file).actionPerformed();
    }

    @Override
    protected List<FileChooser.ExtensionFilter> getExtensionFilters() {
        return FileType.getExtensionFilters();
    }
}
